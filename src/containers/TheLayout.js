import React from 'react'
import { useHistory } from 'react-router-dom'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'

const TheLayout = (props) => {
  const history = useHistory();

  let Page;
  const authLayouts = ['/login','/register','/forgot-password','/reset-password']
  if(authLayouts.indexOf(history.location.pathname) > -1){
    Page = <div className="c-app c-default-layout">
      <div className="c-wrapper">
        <div className="c-body">
          <TheContent/>
        </div>
      </div>
    </div>
  }
  else {
    Page = <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <TheContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  }
  
  return (
    <>
      {Page}
    </>
  )
}

export default TheLayout
