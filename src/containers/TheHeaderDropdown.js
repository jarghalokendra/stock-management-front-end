import React, { useState, useEffect } from "react";
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { Avatar, Spin } from "antd";
import { LogoutOutlined, UserOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import http from "../config";
import { setAuth, setAuthenticate } from "../redux/actions/index";

const TheHeaderDropdown = (props) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);

  let isMount = false;
  useEffect(() => {
    return () => {
      isMount = true;
    };
  }, []);

  const handleLogout = async () => {
    try {
      setLoading(true);
      await http.post("/auth/logout");
      props.setAuth({ auth: {} });
      props.setAuthenticate(false);
      localStorage.removeItem("isAuthenticate");
      localStorage.removeItem("access_token");
      history.push("/login");
      if (!isMount) setLoading(false);
    } catch (err) {
      if (!isMount) setLoading(false);
    }
  };

  return (
    <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
      <Spin spinning={loading}>
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <span>
            <h5 style={{ color: "#6b7b8c", paddingRight: 5 }}>My Account</h5>
          </span>
          <div className="c-avatar" style={{ display: "flex" }}>
            <Avatar
              size={33}
              icon={<UserOutlined style={{ verticalAlign: "middle" }} />}
            />
          </div>
        </CDropdownToggle>
      </Spin>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>Account</strong>
        </CDropdownItem>
        <CDropdownItem onClick={() => history.push("/users")}>
          <CIcon name="cil-user-follow" className="mfe-2" />
          Users
        </CDropdownItem>
        <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>Settings</strong>
        </CDropdownItem>
        <CDropdownItem onClick={() => history.push("/account")}>
          <CIcon name="cil-user" className="mfe-2" />
          Profile
        </CDropdownItem>
        <CDropdownItem onClick={handleLogout}>
          <LogoutOutlined /> &nbsp;Log out
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { setAuthenticate, setAuth })(
  TheHeaderDropdown
);
