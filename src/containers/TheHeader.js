import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CBreadcrumbRouter,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

// routes config
import routes from "../routes";

import { TheHeaderDropdown } from "./index";
import { setSidebarShow } from "../redux/actions/index";

const TheHeader = (props) => {
  const dispatch = useDispatch();
  let isMount = false;
  let npath = "/canvas/normal";
  let gpath = "/canvas/gallery";
  let sidebarShow = useSelector((state) => state.sidebarShow);

  useEffect(() => {
    let path = props.location.pathname;
    if (path.indexOf(npath) > -1 || path.indexOf(gpath) > -1) {
      sidebarShow = true;
      toggleSidebar();
    } else if (!sidebarShow) {
      sidebarShow = "responsive";
      toggleSidebarMobile();
    }
    return () => {
      isMount = true;
    };
  }, [props.location]);

  const toggleSidebar = () => {
    const val = [true, "responsive"].includes(sidebarShow)
      ? false
      : "responsive";
    if (!isMount) dispatch(setSidebarShow({ sidebarShow: val }));
  };

  const toggleSidebarMobile = () => {
    const val = [false, "responsive"].includes(sidebarShow)
      ? true
      : "responsive";
    if (!isMount) dispatch(setSidebarShow({ sidebarShow: val }));
  };

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
        {/* <CIcon name="logo" height="48" alt="Logo" /> */}
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CBreadcrumbRouter
          className="border-0 c-subheader-nav m-0 px-0 px-md-3"
          routes={routes}
        />
      </CHeaderNav>

      <CHeaderNav className="px-3">
        <TheHeaderDropdown />
      </CHeaderNav>
    </CHeader>
  );
};

export default withRouter(TheHeader);
