import React, { Suspense, useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Redirect,
  Route,
  Switch,
  useLocation,
  useHistory,
} from "react-router-dom";
import { CContainer, CFade } from "@coreui/react";

// routes config
import routes from "../routes";
import http from "../config";

import { setAuthenticate } from "../redux/actions/index";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const TheContent = (props) => {
  const [loading, setLoading] = useState(false);
  const [apicheck, setApicheck] = useState(false);
  const location = useLocation();
  const history = useHistory();
  const route = ["/login", "/forgot-password", "/register", "/reset-password"];
  let isMount = false;
  useEffect(() => {
    if (!apicheck && route.indexOf(history.location.pathname) === -1)
      checkApi();
    return () => {
      isMount = true;
    };
  }, [location]);

  const checkApi = async () => {
    try {
      setLoading(true);
      setApicheck(true);
      let r = await http.get("/auth/check-authentication");
      const authenticated = r.data.authenticated;
      props.setAuthenticate(authenticated);
      localStorage.setItem("isAuthenticate", authenticated);
      if (authenticated && route.indexOf(history.location.pathname) > -1)
        history.push("/dashboard");
      if (!isMount) {
        setLoading(false);
        setApicheck(false);
      }
    } catch (err) {
      if (err.response.status === 401) {
        props.setAuthenticate(false);
        localStorage.setItem("isAuthenticate", false);
        history.push("/login");
      }

      if (!isMount) {
        setLoading(false);
        setApicheck(false);
      }
    }
  };
  return (
    <main className="c-main">
      <CContainer fluid>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route, idx) => {
              return (
                route.component && (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={(props) => (
                      <CFade>
                        <route.component {...props} />
                      </CFade>
                    )}
                  />
                )
              );
            })}
            <Redirect from="/" to="/login" />
          </Switch>
        </Suspense>
      </CContainer>
    </main>
  );
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { setAuthenticate })(
  React.memo(TheContent)
);
