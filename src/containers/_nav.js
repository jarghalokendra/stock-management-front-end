export default [
  {
    _tag: "CSidebarNavItem",
    name: "Dashboard",
    to: "/dashboard",
    icon: "cil-speedometer",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Stock",
    to: "/inventory",
    icon: "cil-wallet",
  },

  // CORE MODULES //
  {
    _tag: "CSidebarNavTitle",
    _children: ["Modules"],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "Manage Core Modules",
    children: [
      {
        _tag: "CSidebarNavItem",
        name: "Categories",
        to: "/categories",
        icon: "cil-tags",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Prices",
        to: "/prices",
        icon: "cil-dollar",
      },
    ],
  },

  // PURCHASE MODULES
  {
    _tag: "CSidebarNavDropdown",
    name: "Manage Purchases",
    children: [
      {
        _tag: "CSidebarNavItem",
        name: "Products",
        to: "/products",
        icon: "cil-apps",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Parties",
        to: "/vendors",
        icon: "cil-people",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Purchase Transactions",
        to: "/purchase-transactions",
        icon: "cil-bank",
      },
    ],
  },

  // SALE MODULES
  {
    _tag: "CSidebarNavDropdown",
    name: "Manage Sales",
    children: [
      {
        _tag: "CSidebarNavItem",
        name: "Sales",
        to: "/sales",
        icon: "cil-basket",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Customers",
        to: "/customers",
        icon: "cil-people",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Sale Transactions",
        to: "/sale-transactions",
        icon: "cil-bank",
      },
    ],
  },

  // core settings
  {
    _tag: "CSidebarNavTitle",
    _children: ["Core Settings"],
  },
  {
    _tag: "CSidebarNavDropdown",
    name: "Manage Settings",
    children: [
      {
        _tag: "CSidebarNavItem",
        name: "Users",
        to: "/users",
        icon: "cil-user-plus",
      },
      {
        _tag: "CSidebarNavItem",
        name: "Profile",
        to: "/users/account",
        icon: "cil-settings",
      },
    ],
  },

  {
    _tag: "CSidebarNavDivider",
    className: "m-2",
  },
];
