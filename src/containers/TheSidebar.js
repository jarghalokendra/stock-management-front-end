import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Image } from "antd";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from "@coreui/react";

import http from "../config";

// sidebar nav config
import navigation from "./_nav";
import { setSidebarShow } from "../redux/actions/index";

const TheSidebar = () => {
  const dispatch = useDispatch();
  const show = useSelector((state) => state.sidebarShow);

  let navs = [];
  const [menus, setMenus] = useState([]);

  useEffect(() => {
    modules();
  }, []);

  const modules = async () => {
    try {
      let res = await http.get(`/commons/modules`);
      setMenus(res.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  if (menus) {
    navigation.forEach((nav) => {
      if (nav._tag === "CSidebarNavDropdown") {
        nav._children = [];
        menus.forEach((menu) => {
          if (menu.parent && nav._tag === "CSidebarNavDropdown") {
            nav.children.forEach((n) => {
              if (menu.module_name === n.name) {
                nav._children.push(n);
              }
            });
          }
        });
      }
    });
    navigation.forEach((nav) => {
      if (nav.name) {
        if (nav._tag === "CSidebarNavDropdown" && nav._children.length) {
          navs.push(nav);
        }
        menus.forEach((menu) => {
          if (menu.module_name === nav.name) {
            navs.push(nav);
          }
        });
      } else {
        navs.push(nav);
      }
    });
  }

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch(setSidebarShow({ sidebarShow: val }))}
    >
      <CSidebarBrand
        style={{ textDecoration: "none", backgroundColor: "#CED2D8" }}
        className="d-md-down-none"
        to="/"
      >
        <Image
          src={require("../images/saurya-icon1.png")}
          height={50}
          width={80}
          preview={false}
        />
      </CSidebarBrand>
      <CSidebarNav>
        <CCreateElement
          items={navs}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none" />
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
