import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Spin } from 'antd';

import './scss/style.scss';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
// const Login = React.lazy(() => import('./views/pages/login/Login'));
// const ForgotPassword = React.lazy(() => import('./views/pages/forgot-password/forgot-password'));
// const ResetPassword = React.lazy(() => import('./views/pages/reset-password/reset-password'));
// const Register = React.lazy(() => import('./views/pages/register/Register'));
// const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
// const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));
// <Route path="/login" name="Login Page" render={props => <Login {...props}/>} />
// <Route path="/forgot-password" name="Forgot Password" render={props => <ForgotPassword {...props}/>} />
// <Route path="/reset-password" name="Reset Password" render={props => <ResetPassword {...props}/>} />
// <Route path="/register" name="Register Page" render={props => <Register {...props}/>} />
// <Route path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
// <Route path="/500" name="Page 500" render={props => <Page500 {...props}/>} />

class App extends Component {

  render() {
    return (
      <Router>
          <React.Suspense fallback={loading}>
            <Switch>
              <Route path="/" name="Login" render={props => <TheLayout {...props}/>} />
            </Switch>
          </React.Suspense>
      </Router>
    );
  }
}

export default App;
