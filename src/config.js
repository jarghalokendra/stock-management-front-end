import axios from "axios";
import { message } from "antd";

const instance = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_VERSION}`,
  headers: {
    "Cache-Control": "no-cache",
    Pragma: "no-cache",
    Expires: "0",
  },
});

instance.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem("access_token");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
/**
 * response handlar
 * Mostly intended to handle 401 error
 */

message.loading({ content: "Loading...", key: "updated", duration: 2 });
instance.interceptors.response.use(
  function (response) {
    if (response.status === 200) {
      if (typeof response.data.message !== "undefined")
        message.success({
          content: response.data.message,
          key: "updated1",
          duration: 2,
        });
    }
    return response;
  },
  function (error) {
    if (error.response.status === 403) {
      message.error({
        content: "Unauthorised access level",
        key: "updated2",
        duration: 3,
      });
    } else if (error.response.status === 401) {
      localStorage.removeItem("access_token");
      localStorage.removeItem("isAuthenticate");
      let url = error.response.config.url.split("/");
      if (url[url.length - 1] !== "check") {
        message.error({
          content: "Oops..! Session has expired!",
          key: "updated3",
          duration: 3,
        });
        //window.location.pathname = '/login';
      }
    } else if (error.response.status === 404) {
      message.error({
        content: "The requested api is not found in server. 404 error!",
        key: "updated4",
        duration: 3,
      });
    } else if (error.response.status === 500) {
      message.error({
        content: "500 internal error!",
        key: "updated4",
        duration: 3,
      });
    }
    return Promise.reject(error);
  }
);
export default instance;
