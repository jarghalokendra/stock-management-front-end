import React from "react";
import { connect } from "react-redux";
import { CCard, CCol, CRow } from "@coreui/react";
import {
  Button,
  Drawer,
  Form,
  Input,
  Alert,
  message,
  Row,
  Col,
  Spin,
} from "antd";
import http from "../../config";
import { setInitGrid } from "../../redux/actions/index";

class Category extends React.Component {
  formRef = React.createRef();

  state = {
    visible: false,
    loading: false,
    validationerror: "closed",
    errors: [],
    categories: [],
  };

  componentDidMount() {
    const { isEdit } = this.props;
    if (isEdit) this.handleEdit();
  }

  handleEdit = () => {
    this.setState({ loading: true });
    const { edit } = this.props;
    setTimeout(() => {
      this.formRef.current.setFieldsValue(edit);
      this.setState({ loading: false });
    }, 1000);
  };

  showDrawer = (e) => {
    e.preventDefault();
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      validationerror: "closed",
      errors: [],
    });
    this.props.handleClose();
    this.formRef.current.resetFields();
  };

  onFinish = async (values) => {
    try {
      this.setState({ loading: true });
      const { isEdit } = this.props;
      let url = isEdit ? `/categories/${this.props.edit.id}` : "/categories";
      if (!isEdit) {
        await http.post(url, values);
      } else {
        await http.put(url, values);
      }
      this.formRef.current.resetFields();
      this.props.handleClose();
      this.props.setInitGrid({ init: true });
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        this.setState({
          errors: ers,
          validationerror: error,
        });
      }
      message.error({ content: error, duration: 10 });
      this.setState({ loading: false });
    }
  };

  render() {
    return (
      <CRow>
        <CCol xl={12} md={12}>
          <CCard>
            <Drawer
              title={`${
                this.props.isEdit ? "Edit Category" : "Add New Category"
              }`}
              width="720"
              closable={true}
              onClose={this.onClose}
              visible={this.props.visible}
            >
              <Row>
                <Col span={14} offset={4}>
                  {this.state.validationerror !== "closed" && (
                    <Alert
                      message="Validation errors"
                      description={this.state.validationerror}
                      type="error"
                      showIcon
                      closable
                      style={{ marginBottom: "10px" }}
                    />
                  )}
                  {this.state.errors.length > 0 &&
                    this.state.errors.map((item, index) => {
                      return (
                        <Alert
                          message={item.error}
                          type="error"
                          showIcon
                          key={index}
                          closable
                        />
                      );
                    })}
                </Col>
              </Row>
              <Spin spinning={this.state.loading}>
                <Form
                  ref={this.formRef}
                  labelCol={{ span: 4 }}
                  wrapperCol={{ span: 14 }}
                  layout="horizontal"
                  onFinish={this.onFinish}
                >
                  <Form.Item
                    label="Name"
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Please enter categorie name!",
                      },
                    ]}
                  >
                    <Input placeholder="Name" />
                  </Form.Item>

                  <Form.Item
                    label="Description"
                    name="description"
                    rules={[
                      { required: true, message: "Please enter description" },
                    ]}
                  >
                    <Input.TextArea placeholder="Enter description" />
                  </Form.Item>

                  <Form.Item label="Icon" name="icon">
                    <Input.TextArea placeholder="Enter Icon.." />
                  </Form.Item>

                  <Form.Item label=" " colon={false}>
                    <Button type="primary" htmlType="submit" ghost>
                      Save
                    </Button>
                  </Form.Item>
                </Form>
              </Spin>
            </Drawer>
          </CCard>
        </CCol>
      </CRow>
    );
  }
}
const mapStateToProps = (state) => ({
  gridData: state.gridData,
});
export default connect(mapStateToProps, { setInitGrid })(Category);
