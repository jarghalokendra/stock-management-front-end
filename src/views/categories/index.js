import React, { useState } from "react";
import { CCard, CCol, CRow } from "@coreui/react";

//import http from '../../config'
import Price from "./create";
import GridTable from "../../components/grid/index";

// import GridTable from '../grid/index'
const Categories = () => {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [edit, setEdit] = useState({});

  const handleCreate = (e) => {
    e.preventDefault();
    setVisible(true);
    setIsEdit(false);
  };

  const handeEditAction = (e, row) => {
    e.preventDefault();
    setVisible(true);
    setIsEdit(true);
    setEdit(row);
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      sorter: true,
      editable: true,
    },
    {
      title: "Description",
      dataIndex: "description",
      sorter: true,
      editable: true,
    },
    // {
    //   title: "Icon",
    //   dataIndex: "icon",
    //   sorter: true,
    //   editable: true,
    // },
    {
      title: "Created At",
      dataIndex: "created_at",
      sorter: true,
    },
  ];
  const acolumns = {
    apiurl: "categories",
    content_type: "categories",
    title: "Categories",
    reduxKey: "categories",
    settings: {
      show: true,
      createTitle: "Create Category",
      createLink: "",
    },
    actions: {
      show: true,
      edit: {
        show: true,
        name: "Edit",
        type: "popup",
        callFunction: "handleEdit", //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit}
      },
      delete: {
        show: true,
        name: "Delete",
        type: "popup",
      },
      screen: {
        show: false,
        name: "Set to Screen",
        type: "set-to-screen", //it should be only two types set-to-screen or set-content
      },
    },
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <GridTable
            columns={columns}
            acolumns={acolumns}
            handleCreate={(e) => handleCreate(e)}
            handleEdit={(e, row) => handeEditAction(e, row)}
          />
        </CCard>
        {visible && (
          <Price
            visible={visible}
            handleClose={() => setVisible(false)}
            isEdit={isEdit}
            edit={edit}
          />
        )}
      </CCol>
    </CRow>
  );
};

export default Categories;
