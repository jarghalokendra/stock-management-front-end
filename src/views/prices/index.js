import React, { useState } from "react";
import { CCard, CCol, CRow } from "@coreui/react";
import Price from "./create";
import GridTable from "../../components/grid/index";

const Prices = () => {
  const [visible, setVisible] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [edit, setEdit] = useState({});

  const handleCreate = (e) => {
    e.preventDefault();
    setVisible(true);
    setIsEdit(false);
  };

  const handeEditAction = (e, row) => {
    e.preventDefault();
    setVisible(true);
    setIsEdit(true);
    setEdit(row);
  };

  const columns = [
    {
      title: "Product Name",
      dataIndex: "product_name",
      sorter: true,
      editable: true,
    },
    {
      title: "Cost Price",
      dataIndex: "cost_price",
      sorter: false,
      editable: true,
    },
    {
      title: "Wholesale SP",
      dataIndex: "whole_sp",
      sorter: false,
      editable: true,
    },
    {
      title: "Retail SP",
      dataIndex: "retail_sp",
      sorter: false,
      editable: true,
    },
    {
      title: "Created At",
      dataIndex: "created_at",
      sorter: true,
    },
  ];
  const acolumns = {
    apiurl: "prices",
    content_type: "prices",
    title: "Prices",
    reduxKey: "prices",
    settings: {
      show: true,
      createTitle: "Create Prices",
      createLink: "",
    },
    actions: {
      show: true,
      edit: {
        show: true,
        name: "Edit",
        type: "popup", //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit},
        callFunction: "handleEdit",
      },
      delete: {
        show: true,
        name: "Delete",
        type: "popup",
      },
    },
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <GridTable
            columns={columns}
            acolumns={acolumns}
            handleCreate={(e) => handleCreate(e)}
            handleEdit={(e, row) => handeEditAction(e, row)}
          />
        </CCard>
        {visible && (
          <Price
            visible={visible}
            handleClose={() => setVisible(false)}
            isEdit={isEdit}
            edit={edit}
          />
        )}
      </CCol>
    </CRow>
  );
};

export default Prices;
