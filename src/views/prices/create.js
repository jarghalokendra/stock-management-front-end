import React from "react";
import { CCard, CCol, CRow } from "@coreui/react";
import { Drawer } from "antd";
import PriceForm from "./form";

class Price extends React.Component {
  state = {
    visible: false,
    loading: false,
    validationerror: "closed",
    errors: [],
  };

  onClose = () => {
    this.setState({
      validationerror: "closed",
      errors: [],
    });
    this.props.handleClose();
  };

  render() {
    const { edit, isEdit } = this.props;
    return (
      <CRow>
        <CCol xl={12} md={12}>
          <CCard>
            <Drawer
              title={`${this.props.isEdit ? "Edit Price" : "Add New Price"}`}
              width="720"
              closable={true}
              onClose={this.onClose}
              visible={this.props.visible}
            >
              <PriceForm
                handleFormClose={this.onClose}
                isEdit={isEdit}
                edit={edit}
              />
            </Drawer>
          </CCard>
        </CCol>
      </CRow>
    );
  }
}

export default Price;
