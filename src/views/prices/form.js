import React from "react";
import { connect } from "react-redux";
import {
  Button,
  Form,
  Input,
  Alert,
  message,
  Row,
  Col,
  Spin,
  InputNumber,
} from "antd";
import http from "../../config";
import { setInitGrid } from "../../redux/actions/index";

class PriceForm extends React.Component {
  formRef = React.createRef();

  state = {
    visible: false,
    loading: false,
    validationerror: "closed",
    errors: [],
  };

  componentDidMount() {
    const { isEdit } = this.props;
    console.log(this.props);
    if (isEdit) this.handleEdit();
  }

  handleEdit = () => {
    this.setState({ loading: true });
    const { edit } = this.props;
    setTimeout(() => {
      this.formRef.current.setFieldsValue(edit);
      this.setState({ loading: false });
    }, 1000);
  };

  showDrawer = (e) => {
    e.preventDefault();
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      validationerror: "closed",
      errors: [],
    });
    this.props.handleClose();
    this.formRef.current.resetFields();
  };

  onFinish = async (values) => {
    try {
      this.setState({ loading: true });
      const { isEdit } = this.props;
      let url = isEdit ? `/prices/${this.props.edit.id}` : "/prices";

      if (!isEdit) {
        await http.post(url, values);
      } else {
        await http.put(url, values);
      }
      this.formRef.current.resetFields();
      this.props.handleFormClose();
      this.setState({ loading: false });
      this.props.setInitGrid({ init: true });
    } catch (err) {
      console.log(err);
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        this.setState({
          errors: ers,
          validationerror: error,
        });
      }
      message.error({ content: error, duration: 10 });
      this.setState({ loading: false });
    }
  };

  render() {
    return (
      <>
        <Row>
          <Col span={14} offset={4}>
            {this.state.validationerror !== "closed" && (
              <Alert
                message="Validation errors"
                description={this.state.validationerror}
                type="error"
                showIcon
                closable
                style={{ marginBottom: "10px" }}
              />
            )}
            {this.state.errors.length > 0 &&
              this.state.errors.map((item, index) => {
                return (
                  <Alert
                    message={item.error}
                    type="error"
                    showIcon
                    key={index}
                    closable
                  />
                );
              })}
          </Col>
        </Row>
        <Spin spinning={this.state.loading}>
          <Form
            ref={this.formRef}
            labelCol={{ span: 4 }}
            wrapperCol={{ span: 14 }}
            layout="horizontal"
            onFinish={this.onFinish}
          >
            <Form.Item
              label="Product Name"
              name="product_name"
              rules={[
                { required: true, message: "Please enter product name!" },
              ]}
             
            >
              <Input placeholder="Product Name"  style={{width:"200px"}} />
            </Form.Item>

            <Form.Item
              label="Cost Price"
              name="cost_price"
              rules={[{ required: true, message: "Please enter cost price!" }]}
            >
              <InputNumber />
            </Form.Item>

            <Form.Item
              label="Wholesale SP"
              name="whole_sp"
              rules={[
                {
                  required: true,
                  message: "Please enter wholesale selling price!",
                },
              ]}
            >
              <InputNumber />
            </Form.Item>

            <Form.Item
              label="Retail SP"
              name="retail_sp"
              rules={[
                {
                  required: true,
                  message: "Please enter retail selling price!",
                },
              ]}
            >
              <InputNumber />
            </Form.Item>

            <Form.Item label=" " colon={false}>
              <Button type="primary" htmlType="submit" ghost>
                Save
              </Button>
            </Form.Item>
          </Form>
        </Spin>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  gridData: state.gridData,
});

export default connect(mapStateToProps, { setInitGrid })(PriceForm);
