import React from 'react'
import { CCard,CCol,CRow} from '@coreui/react'
//import { Button,Dropdown, Menu, Drawer, Form, Input,Alert, message, Row, Col, Spin} from 'antd';

// import Screen from './create'
import GridTable from '../../components/grid/index'

// import GridTable from '../grid/index'
class PlayList extends React.Component {
  //formRef = React.createRef();
  state = {
    visible: false,
    loading: false,
    isEdit: false,
    edit: {}
  }
  handeEditAction(e,row){
    e.preventDefault()
    this.setState({
      visible:true,
      isEdit: true,
      edit: row
    })
  }
  handleCreate(){
    this.setState({
      visible:true,
      isEdit: false
    })
  }
  render(){
    const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: true,
      editable: true
    },
    {
      title: 'created_at',
      dataIndex: 'created_at',
      sorter: true,
    }]
    const acolumns = {
      apiurl: 'playlists',
      title: 'PlayList',
      reduxKey: 'playlists',
      content_type: 'playlists',
      settings: {
        show:true,
        createTitle: 'Create PlayList',
        createLink: '/playlists/create',
      },
      statusColumn: {
        column: 'status',
        show: true
      },
      pivotRelation: {
        items: [{
          column: 'playlists',
          show: true,
          nested: true, //simply true or false 
          relationName: {
            parent: 'playlists',
            child: 'content',
            childRname: 'name' // bydefault its is name
          },
          relationColName: 'original_name',
          dType: 'array'//we can pass dType like array or object
        }]
      },
      actions: {
        show: true,
        edit: {
          show: true,
          name: 'Edit',
          type: 'url' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
        },
        delete:{
          show: true,
          name: 'Delete',
        },
        screen: {
          show: true,
          name: 'Set to Screen',
          type: 'set-to-screen' //it should be only two types set-to-screen or set-content
        }
        
      }
    }  
    return (
      <CRow>
        <CCol xl={12} md={12}>
          <CCard>
            <GridTable 
              columns={columns} 
              acolumns={acolumns} 
              handleCreate={()=>this.handleCreate()}
              handleEdit={(e, row)=>this.handeEditAction(e, row)}
            />
          </CCard>
        </CCol>
      </CRow>
    )
  }
}

export default PlayList