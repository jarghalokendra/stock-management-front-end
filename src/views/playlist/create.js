import React from 'react'
import { CCard,CCol,CRow} from '@coreui/react'
import { Tabs, Input} from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import Container from './container'
import MediaList from './media-list'
const { TabPane } = Tabs;

const Create = () => {
	return(
		<CRow>
			<DndProvider backend={HTML5Backend}>
        <CCol xl={8} md={8}>
          <CCard>
          	<Container/>
          </CCard>
        </CCol>
        <CCol xl={4} md={4}>
          <CCard style={{padding: '10px'}}>
          	<Input placeholder="search" prefix={<SearchOutlined/>} />
          	<Tabs defaultActiveKey="1">
        	    <TabPane tab="Medias" key="1">
        	    	<MediaList type="medias"/>
        	    </TabPane>
        	    <TabPane tab="Links" key="2">
        	      <MediaList type="links"/>
        	    </TabPane>
        	    <TabPane tab="Canvas" key="3">
        	      <MediaList type="canvas"/>
        	    </TabPane>
              <TabPane tab="Apps" key="4">
                <MediaList type="apps"/>
              </TabPane>
        	  </Tabs>
          </CCard>
        </CCol>
      </DndProvider>
    </CRow>
	)
}

export default Create;