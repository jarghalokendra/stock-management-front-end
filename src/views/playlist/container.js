import React,{useState, useEffect} from 'react';
import { useDrop } from 'react-dnd';
import {connect} from 'react-redux';
import { useHistory } from 'react-router-dom'
import { Table, Tooltip, Input, Button, message, Spin,Alert, Form,Popconfirm} from 'antd';
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc';
import { MenuOutlined,CloseOutlined, FilePdfOutlined, VideoCameraOutlined} from '@ant-design/icons';
import arrayMove from 'array-move';

import {removePlayList, updatePlayList, updateEditplaylist} from '../../redux/actions/index'
import http from '../../config'
import EditableCell from '../../components/commons/edit-table-column/cell'
import styles from './playlist.module.css'

const DragHandle = sortableHandle(() => (
	<Tooltip placement="top" title={`Sort order of item`}>
  	<MenuOutlined style={{ cursor: 'pointer', color: '#999' }} />
  </Tooltip>
));

const SortableItem = sortableElement(props => <tr {...props} />);
const SortableContainer = sortableContainer(props => <tbody {...props} />);
const Dustbin = (props) => {
	const [form] = Form.useForm();
	const [loading, setLoading] = useState(false);
	const history = useHistory();
	const [title, setTtitle] = useState(null);
	const [isEdit, setIsEdit] = useState(false);
	const [id, setId] = useState(null);
	const [errors, setErrors] = useState([]);
	const {playList} = props;
	let path = history.location.pathname;
	const [editingKey, setEditingKey] = useState('');

	let columns = [
	{
	  title: 'Duration',
	  dataIndex: 'duration',
	  className: 'drag-visible',
	  editable: true,
	  width: 150
	},
	{
	  title: 'Type',
	  dataIndex: 'playlist_type',
	  className: 'drag-visible',
	  width: 100,
	},
	{
	  title: 'Sort',
	  dataIndex: 'sort',
	  width: 70,
	  className: 'drag-visible',
	  render: () => <DragHandle />,
	}];

  const isEditing = (record) => record.index === editingKey;
	path = path.split('/');
	useEffect(()=>{
		if(path.length > 0 && path[2] != 'create'){
		  initEdit(path[2])
		  setId(path[2])
		}
	},[])

	const handleEditableSave = async (key) => {
		try {
			const row = await form.validateFields();
			const newData = playList;
			const index = newData.findIndex((item) => key === item.index);
			if (index > -1) {
				let item = newData[index];
				item.duration = item.file_type === 'mp4' && item.media_type === 'medias' ? item.duration : row.duration;
				props.updateEditplaylist({item})
				setEditingKey('');
			} else {
				newData.push(row);
				props.updatePlayList(newData)
				setEditingKey('');
			}
		} catch (errInfo) {
			console.log('Validate Failed:', errInfo);
		}
	};

	const handleEditable = (record) => {
    form.setFieldsValue({
      duration: record.duration,
      ...record,
    });
    setEditingKey(record.index);
  };

  const handleEditableCancel = () => {
    setEditingKey('');
  };
	const initEdit = async(id) => {
		try {
			setLoading(true)
			let result = await http.get(`/playlists/${id}/edit`);
			let playlist = result.data.edit;
			setTtitle(playlist.name);
			const playlists = playlist.playlists;
			let d = handlePlayList(playlists)
			setIsEdit(true);
			props.updatePlayList(d);
			setLoading(false)
		}
		catch(err){
			setLoading(false)
		}
	}

	const handlePlayList =(playlists)=>{
		let d = [];
		playlists.forEach((item)=>{
			let obj = {};
			obj.playlist_id = item.id;
			let imgType = ['png','jpg','gif','jpeg']
			if(item.content){
				let content = item.content;
				obj.id = content.id;
				obj.name = content.original_name ? content.original_name : content.name;
				obj.media_type = item.content_type;
				obj.duration = content.file_type === 'mp4' ? content.duration : item.duration;
				obj.playlist_type = imgType.indexOf(content.file_type) > -1 ? 'image' : (content.file_type === 'mp4' ? 'video':(content.file_type === 'pdf' ? 'file' : item.content_type));
				obj.file_type = content.preview ? 'png' : content.file_type;
				obj.save_path = content.preview ? content.preview : content.save_path;
			}
			if(item.app){
				obj.save_path = item.app.app_thumb;
				obj.file_type = 'png'
			}
			d.push(obj)
		})
		return d;
	}

	let fileName={
		title: 'Media',
		key: 'name',
		render: (row) => handleFileName(row)
	}

	const handleFileName = (row) => {
		let imgType = ['png','jpg','jpeg','gif'];
		let name = row.name ? row.name.substring(0,30) : ''
		let img;
		if(imgType.indexOf(row.file_type) > -1){
			img = <div><img src={row.save_path} alt={``} style={{height: '40px', width: '50px'}}/>{name}</div>
		}
		else if(row.file_type === 'mp4'){
			img = <div className="file-type"> <VideoCameraOutlined/> {name}</div>
		}
		else {
			img = <div className="file-type"> <FilePdfOutlined/> {name}</div>
		}
		const t = <div>
			{img}
		</div>
		return t;
	}
	columns.splice(0,0,fileName);

	let remove={
		title: '',
		key: 'remove',
		width: 70,
		render: (row) => handleRemove(row)
	}
	const handleRemove = (row) => {
		const t = <div>
			<Tooltip placement="top" title={`Remove`}>
				<a href="#" onClick={(e)=>handleRemoveAction(e,row)}><CloseOutlined /></a>
			</Tooltip>
		</div>
		return t;
	}
	let index = columns.length;
	columns.splice(index,0,remove);

	let edit={
		title: 'Edit',
		key: 'edit',
		width: 110,
		render: (_, record) => {
		  const editable = isEditing(record);
		  const editDisabled = record.file_type === 'mp4' && record.media_type === 'medias'
		  return editable ? (
		    <span>
		      <a href="#" onClick={() => handleEditableSave(record.index)} style={{marginRight: 8}}>
		        Save
		      </a>
		      <Popconfirm title="Sure to cancel?" onConfirm={handleEditableCancel}>
		        <a>Cancel</a>
		      </Popconfirm>
		    </span>
		  ) : (
		    <a href="#" disabled={editingKey !== '' || editDisabled} onClick={() => handleEditable(record)}>
		      Edit
		    </a>
		  );
		}
	}
	let eindex = columns.length-1;
	columns.splice(eindex,0,edit);

	let obj={};
	let cols = []
	for(let i=0; i<columns.length; i++){
		obj[columns[i].title] = columns[i];
	}
	for(let prop in obj){
		cols.push(obj[prop])
	}
	const handleRemoveAction = (e,row) => {
		e.preventDefault();
		props.removePlayList(row)
	}
	const handleTitle = (e) => {
		setTtitle(e.value)
	}

	const handleSubmitDisabled = () => {
		return !(playList.length !== 0 && title)
	}
	const handleSubmit = async () => {
		if(handleSubmitDisabled()){
			message.error({ content: 'Please set playList and title!', duration: 2});
			return 
		}
		const params = {
			name: title,
			playlist: playList
		}
		try{
			setLoading(true)
			let url = isEdit ? `/playlists/update/${id}` : '/playlists/create';
			let r = await http.post(url, params)
			setLoading(false)
			history.push('/playlists')
		}
		catch(err){
			let error;
			if(typeof err.response.data.message === 'string'){
				error = err.response.data.message;
			}
			let ers = []
			if(err.response.data.errors){
				let errs = err.response.data.errors
				for(let p in errs){
					let obj = {};
					obj.error = errs[p][0];
					ers.push(obj)
				}
				setErrors(ers)
			}
			setLoading(false)
			message.error({ content: error, duration: 2});
		}
	}


	const onSortEnd = ({ oldIndex, newIndex }) => {
    if (oldIndex !== newIndex) {
      const newData = arrayMove([].concat(playList), oldIndex, newIndex).filter(el => !!el);
      props.updatePlayList(newData)
    }
  };
  const DraggableBodyRow = ({ className, style, ...restProps }) => {
    // function findIndex base on Table rowKey props and should always be a right array index
    const index = playList.findIndex(x => x.index === restProps['data-row-key']);
    return <SortableItem index={index} {...restProps} />;
  };
	const DraggableContainer = props => (
	 <SortableContainer
	   useDragHandle
	   helperClass={styles['row-dragging']}
	   onSortEnd={onSortEnd}
	   {...props}
	 />
	);
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'box',
    drop: () => ({ name: 'Dustbin' }),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });
  const isActive = canDrop && isOver;
  playList.forEach((item,index)=>{
  	item.index = index;
  })
  cols = cols.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        editable: col.editable.toString(),
        dataindex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
        item: record
      }),
    };
  });
  return (
  	<div ref={drop}> 
  		<div className="input-wrapper" style={{padding: '10px'}}>
  			{
  				errors.length > 0 && 
  				 errors.map((item, index)=>{
  				 	return(
  				 		<Alert message={item.error} type="error" showIcon key={index} closable/>
  				 	)
  				 })
  			}
  			<Input name="title" value={title} placeholder="Enter the title" onChange={(e)=>handleTitle({name: e.target.name,value: e.target.value})}/>
  		</div>

  		<Spin spinning={loading} tip="Loading...">
  			<Form form={form} component={false}>
					<Table
		        pagination={false}
		        rowClassName={() => 'editable-row'}
		        dataSource={playList}
		        columns={cols}
		        rowKey={record => record.index}
		        fixed={true}
		        scroll={{y: 460 }}
		        components={{
		          body: {
		            wrapper: DraggableContainer,
		            row: DraggableBodyRow,
		            cell: EditableCell,
		          },
		        }}
		      />
		    </Form>
	    </Spin>
      <div className={styles['playlist-action']}>
      	<Button onClick={()=>history.push('/playlists')}>
      	  back
      	</Button>
      	<Button type="primary" onClick={()=>handleSubmit()} loading={loading} disabled={handleSubmitDisabled()}>
      	  Save
      	</Button>
      </div>
		</div>
	);
};
const mapStateToProps = state => ({
	playList: state.playList
});
export default connect(mapStateToProps, {removePlayList, updatePlayList,updateEditplaylist}) (Dustbin);
