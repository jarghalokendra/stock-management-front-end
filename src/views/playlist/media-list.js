import React from 'react'
import {PlusOutlined} from '@ant-design/icons';
import { Button,Spin,Skeleton,Modal} from 'antd';
import {connect} from 'react-redux'
import http from '../../config'
import Box from './box'
import Upload from '../../components/uploads/index'
import Links from '../links/create'
import Canvas from '../canvas/create'
import App from '../apps/store'
import {setInitGrid} from '../../redux/actions/index'
import styles from './media-list.module.css'

class MediaList extends React.Component{
	_isMounted = false;
	state= {
		medias: [],
		mVisible: false,
		loading: false,
		lVisible: false,
		cVisible: false,
		aVisible: false,
		apiUrl: `/commons/${this.props.type}/list`
	}

	componentDidMount(){
		this._isMounted = true
		this.handleInit(this.state.apiUrl)
	}
	componentDidUpdate() {
    if(this.props.initGrid){
    	let apiUrl = this.state.apiUrl ? this.state.apiUrl : `/commons/${this.props.type}/list`
    	this.handleInit(apiUrl);
    	this.props.setInitGrid({init: false})
    	this.setState({cVisible: false, lVisible: false, mVisible: false, aVisible: false})
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  handleLoadmore(url){
  	this.handleInit(url, true)
  }
	async handleInit(url, merge=false){
		const {type} = this.props
		try{
			this.setState({loading: true})
			let result = await http.get(url);
			let d = result.data.items.data;
			this.setState({apiUrl: result.data.items.next_page_url})
			if(type === 'links') {
				d.forEach((item)=>{
					item.save_path = item.preview;
					item.file_type = 'png';
					item.duration = 10;
					item.media_type = 'links'
					item.playlist_type = 'links'
				})
			}
			else if(type === 'canvas') {
				d.forEach((item)=>{
					item.save_path = item.preview;
					item.duration = 10;
					item.file_type = 'png';
					item.media_type = 'canvas'
					item.playlist_type = 'canvas'
				})
			}
			else if(type === 'apps') {
				d.forEach((item)=>{
					item.save_path = item.app.app_thumb;
					item.duration = 10;
					item.file_type = 'png';
					item.media_type = 'apps'
					item.playlist_type = 'apps'
				})
			}
			else if(type==='medias'){
				const imgType = ['png','jpg','gif','jpeg','PNG'];
				d.forEach((item)=>{
					item.name = item.original_name;
					item.playlist_type = imgType.indexOf(item.file_type) > -1 ? 'image' : (item.file_type === 'mp4' ? 'video':'file')
				})
			}
			if(this.state.medias.length > 0 && merge){
				d= this.state.medias.concat(d);
			}
			if(this._isMounted){
				this.setState({
					medias: d,
					loading: false
				})
			}
		}
		catch(err){
			console.log(err)
			if(this._isMounted)
			this.setState({loading: false})
		}

	}
	render(){
		const {medias,loading} = this.state;
		const {type} = this.props
		return(
			<>
				<div className={styles['playlist-item-wrapper']} style={{height: '450px',overflowY: 'scroll', padding: '0', margin: '0'}}>
					<Skeleton avatar title={false} loading={loading} active>
						<div className={styles['paylist-list-add']}>
							<a href="#">
								{type} 
	        		</a>
	        		{
	        			type === 'medias' &&
	        			<Button
	        				type="primary"
	        				onClick={()=>this.setState({mVisible: true})}
	        				icon={<PlusOutlined />}
	        			/>
	        		}
	        		{
	        			type === 'links' &&
	        			<Button
	        				type="primary"
	        				onClick={()=>this.setState({lVisible: true})}
	        				icon={<PlusOutlined />}
	        			/>
	        		}
	        		{
	        			type === 'canvas' &&
	        			<Button
	        				type="primary"
	        				onClick={()=>this.setState({cVisible: true})}
	        				icon={<PlusOutlined />}
	        			/>
	        		}
	        		{
	        			type === 'apps' &&
	        			<Button
	        				type="primary"
	        				onClick={()=>this.setState({aVisible: true})}
	        				icon={<PlusOutlined />}
	        			/>
	        		}
						</div>
						<ul>
							{
								medias.length > 0 && 
								medias.map((item,index)=>{
									return (
										<li key={index}><Box index={`${type}-${index}`} item={item} styles={styles}/></li>
									)
								})
							}
							{
								medias.length === 0 && 
									<li>No Items are found!</li>
							}
						</ul>
					</Skeleton>
				</div>
				{
					this.state.apiUrl &&
						<Button onClick={()=>this.handleLoadmore(this.state.apiUrl)} style={{float: 'right'}} type="primary" ghost>More Load</Button>
				}
				{
					this.state.mVisible &&
						<Upload visible={this.state.mVisible} handleClose={()=>this.setState({mVisible: false})}/>
				}
				{
					this.state.lVisible &&
					<Links
						visible={this.state.lVisible}
						handleClose={() => this.setState({ lVisible: false })}
					/>
				}
				<Modal
          title={`Create Canvas`}
          visible={this.state.cVisible}
          onCancel={()=>this.setState({cVisible: false})}
          className={styles['canvas']}
          style={{ top: 20 }}
          footer={[
            <Button key="back" onClick={()=>this.setState({cVisible: false})}>
              Cancel
            </Button>,
          ]}
        >
        	<Canvas settings={{type: 'normal', mode: 'create'}}/>
        </Modal>
        <Modal
          title={`App Store`}
          visible={this.state.aVisible}
          onCancel={()=>this.setState({aVisible: false})}
          className={styles['apps']}
          style={{ top: 50 }}
          footer={[
            <Button key="back" onClick={()=>this.setState({aVisible: false})}>
              Cancel
            </Button>,
          ]}
        >
        	<App noRedirect={true} isInit={true}/>
        </Modal>
			</>
		)
	}
}
const mapStateToProps = state=>({
	initGrid: state.initGrid
})
export default connect(mapStateToProps, {setInitGrid}) (MediaList)