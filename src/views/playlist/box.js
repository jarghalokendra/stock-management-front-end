import React from 'react';
import { useDrag } from 'react-dnd';
import {connect} from 'react-redux'
import {FilePdfOutlined, VideoCameraOutlined} from '@ant-design/icons';

import {setPlayList} from '../../redux/actions/index'
const style = {
  cursor: 'move',
};
const Box = (props) => {
  const {item,index} = props
  let type = item.file_type
  const [{ isDragging }, drag] = useDrag({
    item: { ...item,type: 'box', index: index},
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      if (item && dropResult) {
        props.setPlayList(item);
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? 0.4 : 1;
  let imgType = ['png','jpg','jpeg','gif','PNG'];
  let name = item.original_name ? item.original_name : item.name;
  name = name.length > 20 ? `${name.substring(0,20)} ....` : name
  let img;
  if(imgType.indexOf(item.file_type) > -1){
    img = <div><img src={item.save_path} alt={``} style={{height: '40px', width: '50px'}}/> {name}</div>
  }
  else if(item.file_type === 'mp4'){
    img = <div className={props.styles['playlist-file-type']}> 
      <VideoCameraOutlined/> 
      {name}
    </div>
  }
  else {
    img = <div className={props.styles['playlist-file-type']}> <FilePdfOutlined/> {name}</div>
  }
  return (
    <div ref={drag} style={{ ...style, opacity }}>
		  {img}
	  </div>
  );
};
const mapStateToProps = state => ({
});

export default connect(mapStateToProps, {setPlayList})(Box)
