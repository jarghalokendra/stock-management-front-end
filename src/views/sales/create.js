import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";
import { CCard, CCol, CRow } from "@coreui/react";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Drawer,
  Form,
  Input,
  Alert,
  message,
  Row,
  Col,
  Spin,
  Select,
  Modal,
  Radio,
} from "antd";
import http from "../../config";
import { setInitGrid } from "../../redux/actions/index";
import CustomerForm from "./customer";
import { useReactToPrint } from "react-to-print";
import PrintForm from "./print";
import NepaliDate from "../../components/classes/NepaliDate";

const nepali_date = new NepaliDate();

const Sales = (props) => {
  const componentRef = useRef(null);
  const formRef = useRef(null);

  const [loading, setLoading] = useState(false);
  const [customerVisible, setCustomerVisible] = useState(false);
  const [validationError, setValidationError] = useState("closed");

  const [submitDisable, setSubmitDisable] = useState(false);
  const [errors, setErrors] = useState([]);
  const [users, setUsers] = useState([]);
  const [products, setProducts] = useState([]);
  const [bills, setBills] = useState([]);
  const [remainingQuantity, setRemainingQuantity] = useState({});
  const [remainingQuantity2, setRemainingQuantity2] = useState();
  const [remainingAmount, setRemainingAmount] = useState({});
  const [dataForPrint, setDataForPrint] = useState({});
  const [printVisible, setPrintVisible] = useState(false);
  const [discountAmt, setDiscountAmt] = useState({});
  const [salesType, setSalesType] = useState({});
  const [salesType2, setSalesType2] = useState();
  const [discountDisable, setDiscountDisable] = useState(false);
  const [totalRemainingAmt, setTotalRemainingAmt] = useState(0);
  const [remAmtChange, setRemAmtChange] = useState(false);

  useEffect(() => {
    // let mounted = true;
    const { isEdit } = props;
    if (isEdit) handleEdit();
    initData();

    //   if (mounted) {
    //     initData();
    //   }
    //  return ()=>{
    //   mounted = false;
    //   }
  }, []);

  const loadUsers = async () => {
    let userResult = await http.get(`/commons/customers`);
    setUsers(userResult.data.data);
  };

  const initData = async () => {
    try {
      setLoading(true);
      let productResult = await http.get(`/commons/products`);
      let userResult = await http.get(`/commons/customers`);
      let BillResult = await http.get(`/commons/bill`);
      setProducts(productResult.data.data);
      setUsers(userResult.data.data);
      setBills(BillResult.data.data);
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };

  const handleEdit = () => {
    setLoading(true);
    const { edit } = props;
    const { quantity, paid_amount, product, bill, sales_type } = edit;
    const { price } = product;

    const rate = bill.value / 100;
    const sPrice =
      sales_type === "wholesale" ? price.whole_sp : price.retail_sp;
    let noVatAmt = sPrice * quantity;
    let vatAmt = noVatAmt + (rate > 0 ? rate * noVatAmt : 0);
    let remAmt = vatAmt - paid_amount;
    setTimeout(() => {
      formRef.current.setFieldsValue(edit);
      setSalesType2(sales_type);
      setRemainingAmount(remAmt);
      setLoading(false);
    }, 1000);
  };

  const handleProduct2 = (e) => {
    let product = products.find((item) => {
      return item.id === e;
    });
    const quantity = product.quantity;
    const saleQuantity = product.sale_quantity;
    let remQty = quantity - saleQuantity;
    setRemainingQuantity2(remQty);
  };

  const handleDiscount = ({ key, value }) => {
    setDiscountAmt({ ...discountAmt, [key]: value });

    // const paidAmountRef = formRef.current.getFieldValue("sales");
    // const paidAmount = paidAmountRef[key]["paid_amount"];
    // handlePaidAmount({ key, value: paidAmount });
  };
  const calc = (key) => {
    let productId = formRef.current.getFieldValue("sales");
    if (!productId[key]?.product_id) return;
    let product = products.find((item) => {
      return item.id === productId[key]["product_id"];
    });

    let SaleTypeRef = formRef.current.getFieldValue("sales");
    if (!SaleTypeRef[key]?.sales_type) return;
    let price;
    SaleTypeRef[key]["sales_type"] === "retail"
      ? (price = +product.price.retail_sp)
      : (price = +product.price.whole_sp);

    let billId = formRef.current.getFieldValue("sales");
    if (!billId[key]["bill_type"]) return;
    let bill = bills.find((item) => {
      return item.id === billId[key]["bill_type"];
    });
    const billRate = bill.value > 0 ? bill.value / 100 : 0;

    return [billRate, price];
  };

  const handlePaidAmount = ({ key, value }) => {
    if (!calc(key)) return;
    const [billRate, price] = calc(key);

    let remAmt, amountWithVat;
    let submitDisable = true;

    const salesRef = formRef.current.getFieldValue("sales");

    const discount = salesRef[key]?.discount;

    const amountWithoutVat = discount
      ? price * salesRef[key]?.quantity - discount
      : price * salesRef[key]?.quantity;

    amountWithVat =
      +amountWithoutVat + (billRate > 0 ? amountWithoutVat * billRate : 0);

    if (value) {
      remAmt = (+amountWithVat - value).toFixed(2);
      if (amountWithVat < value) {
        remAmt = amountWithVat.toFixed(2);
        submitDisable = true;

        // msg = "Please enter amount less than total payable amount.";
        // message.error({ content: msg, duration: 5 });
      } else {
        submitDisable = false;
      }
    } else {
      remAmt = amountWithVat.toFixed(2);
    }
    setSubmitDisable(submitDisable);
    setRemainingAmount({ ...remainingAmount, [key]: remAmt });
    setRemAmtChange(() => !remAmtChange);
  };

  const handleBill = ({ key, val }) => {
    const quantity = formRef.current.getFieldValue("sales");
    if (!quantity[key]?.quantity) return;
    const qty = quantity[key]["quantity"];
    handleQuantity({ key, value: qty });
  };

  const handleQuantity = ({ key }) => {
    const paidAmountRef = formRef.current.getFieldValue("sales");
    const paidAmount = paidAmountRef[key]?.paid_amount;
    handlePaidAmount({ key, value: paidAmount });
    // setRemAmtChange(() => !remAmtChange);
  };

  const onClose = () => {
    setErrors([]);
    setValidationError("closed");
    props.handleClose();
    formRef.current.resetFields();
  };

  const onFinish = async (values) => {
    try {
      setLoading(true);

      const { isEdit } = props;
      let url = isEdit ? `/sales/${props.edit.id}` : "/sales";
      if (!isEdit) {
        const dataResult = await http.post(url, values);

        if (dataResult.status === 200) setDataForPrint(values);

        setPrintVisible(true);
      } else {
        await http.put(url, values);
        formRef.current.resetFields();
        props.handleClose();
        props.setInitGrid({ init: true });
      }
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
        setValidationError(error);
      }
      message.error({ content: error, duration: 10 });

      setLoading(false);
    }
  };

  const handleCustomer = (e) => {
    if (e === "create-new") {
      formRef.current.setFieldsValue({
        customer_id: "",
      });

      setCustomerVisible(true);
    }
  };

  const handleCustomerForm = () => setCustomerVisible(false);

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  useEffect(() => {
    calcTotalRemainingAmt();
  }, [remAmtChange]);

  const calcTotalRemainingAmt = () => {
    const totalRem = Object.values(remainingAmount);
    let total = totalRem.reduce((a, b) => +a + +b, 0).toFixed(2);
    if (Math.sign(total) === -1) {
      total = 0;
    }
    setTotalRemainingAmt(total);
  };

  const handlePrintForm = () => {
    setPrintVisible(false);
    formRef.current.resetFields();
    props.handleClose();
    props.setInitGrid({ init: true });
  };

  const priceRadio = ({ key, value }) => {
    setSalesType({ ...salesType, [key]: value });
    const quantity = formRef.current.getFieldValue("sales");

    if (!quantity[key]?.quantity) return;
    const Qty = quantity[key]["quantity"];
    handleQuantity({ key, value: Qty });
  };

  const priceRadio2 = (e) => {
    setSalesType2(e.target.value);
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <Drawer
            title={`${props.isEdit ? "Edit Sale" : "Add New Sale"}`}
            width={props.isEdit ? "720" : "65%"}
            closable={true}
            onClose={onClose}
            visible={props.visible}
          >
            <Row>
              <Col span={14} offset={4}>
                {validationError !== "closed" && (
                  <Alert
                    message="Validation errors"
                    description={validationError}
                    type="error"
                    showIcon
                    closable
                    style={{ marginBottom: "10px" }}
                  />
                )}
                {errors.length > 0 &&
                  errors.map((item, index) => {
                    return (
                      <Alert
                        message={item.error}
                        type="error"
                        showIcon
                        key={index}
                        closable
                      />
                    );
                  })}
              </Col>
            </Row>
            <Spin spinning={loading}>
              {/* {props.isEdit && (
                <span style={{ position: "relative", left: 360, top:40  }}>
                  <p>Remaining Amount : {remainAmt}</p>
                </span>
              )} */}

              {!props.isEdit && (
                <span style={{ position: "relative", left: 360, top: 40 }}>
                  <p> Total Remaining Amount : {totalRemainingAmt}</p>
                </span>
              )}

              <Form
                ref={formRef}
                // labelCol={{ span: 4 }}
                // wrapperCol={{ span: 14 }}
                layout="horizontal"
                onFinish={onFinish}
              >
                <Form.Item
                  label="Customer"
                  name="customer_id"
                  rules={[
                    {
                      required: true,
                      message: "Please select customer name!",
                    },
                  ]}
                >
                  <Select
                    showSearch
                    placeholder="Select Customer Name"
                    optionFilterProp="children"
                    onChange={handleCustomer}
                    style={{ width: 200 }}
                  >
                    <Select.Option key="00" value="create-new">
                      Create New Customer
                    </Select.Option>
                    {users.map((user) => {
                      return (
                        <Select.Option key={user.id} value={user.id}>
                          {user.first_name} {user.last_name}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>

                {props.isEdit ? (
                  <>
                    <Form.Item
                      label="Product"
                      name="product_id"
                      rules={[
                        {
                          required: true,
                          message: "Please select product name!",
                        },
                      ]}
                      style={{ marginBottom: "0" }}
                    >
                      <Select
                        showSearch
                        placeholder="Select Product Name"
                        optionFilterProp="children"
                        onChange={handleProduct2}
                        style={{ width: 200 }}
                      >
                        {products.map((product) => {
                          return product.quantity <= product.sale_quantity ? (
                            ""
                          ) : (
                            <Select.Option key={product.id} value={product.id}>
                              {product.name} {"  "}(
                              {nepali_date.format(product.created_at)})
                            </Select.Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                    <Form.Item name="sales_type">
                      <Radio.Group
                        style={{
                          width: "100%",
                          marginLeft: "16%",
                          marginBottom: "1rem",
                        }}
                        // defaultValue="retail"
                        onChange={priceRadio2}
                        value={salesType2}
                      >
                        <Radio value="retail">Retail</Radio>
                        <Radio value="wholesale">Wholesale</Radio>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item
                      label="Bill Type"
                      name="bill_type"
                      rules={[
                        {
                          required: true,
                          message: "Please select bill type!",
                        },
                      ]}
                      help={"Vat of 13% is applied."}
                      style={{ marginBottom: 20 }}
                    >
                      <Select
                        placeholder="Select Bill Type"
                        style={{ width: 200 }}
                      >
                        {bills.map((bill) => {
                          return (
                            <Select.Option key={bill.id} value={bill.id}>
                              {bill.name}
                            </Select.Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                  </>
                ) : (
                  <>
                    <Form.List name="sales">
                      {(fields, { add, remove }) => (
                        <>
                          {fields.map(
                            ({ key, name, fieldKey, ...restField }, index) => (
                              <div key={key}>
                                <hr />

                                <div style={{ display: "flex" }}>
                                  <Form.Item
                                    {...restField}
                                    label="Product"
                                    name={[name, "product_id"]}
                                    fieldKey={[fieldKey, "product_id"]}
                                    rules={[
                                      {
                                        required: true,
                                        message: "Please select product name!",
                                      },
                                    ]}
                                    style={{ width: "calc(47% - 8px)" }}
                                  >
                                    <Select
                                      showSearch
                                      placeholder="Select Product Name"
                                      optionFilterProp="children"
                                    >
                                      {products.map((product) => {
                                        return product.quantity <=
                                          product.sale_quantity ? (
                                          ""
                                        ) : (
                                          <Select.Option
                                            key={product.id}
                                            value={product.id}
                                          >
                                            {product.name} {"  "}(
                                            {nepali_date.format(
                                              product.created_at
                                            )}
                                            )
                                          </Select.Option>
                                        );
                                      })}
                                    </Select>
                                  </Form.Item>

                                  <Form.Item
                                    {...restField}
                                    label="Bill Type"
                                    name={[name, "bill_type"]}
                                    fieldKey={[fieldKey, "bill_type"]}
                                    rules={[
                                      {
                                        required: true,
                                        message: "Please select bill type!",
                                      },
                                    ]}
                                    help={"Vat of 13% is applied."}
                                    style={{
                                      width: "calc(50% - 8px)",
                                      marginLeft: 25,
                                    }}
                                  >
                                    <Select
                                      placeholder="Select Bill Type"
                                      onChange={(e) =>
                                        handleBill({ key: index, val: e })
                                      }
                                    >
                                      {bills.map((bill) => {
                                        return (
                                          <Select.Option
                                            key={bill.id}
                                            value={bill.id}
                                          >
                                            {bill.name}
                                          </Select.Option>
                                        );
                                      })}
                                    </Select>
                                  </Form.Item>
                                </div>

                                <div style={{ display: "flex" }}>
                                  <Form.Item
                                    {...restField}
                                    label="Price Type"
                                    name={[name, "sales_type"]}
                                    fieldKey={[fieldKey, "sales_type"]}
                                    style={{
                                      marginLeft: 8,
                                      marginTop: "5px",
                                    }}
                                  >
                                    <Radio.Group
                                      onChange={(e) =>
                                        priceRadio({
                                          key: index,
                                          value: e.target.value,
                                        })
                                      }
                                      value={salesType[key]}
                                    >
                                      <Radio
                                        style={{
                                          marginRight: 30,
                                          marginLeft: 22,
                                        }}
                                        value="retail"
                                      >
                                        Retail
                                      </Radio>
                                      <Radio value="wholesale">Wholesale</Radio>
                                    </Radio.Group>
                                  </Form.Item>

                                  {/* Discount code Mark */}
                                  <Form.Item
                                    label="Quantity"
                                    style={{
                                      width: "48%",
                                      marginLeft: 100,
                                    }}
                                  >
                                    <Form.Item
                                      {...restField}
                                      name={[name, "quantity"]}
                                      fieldKey={[fieldKey, "quantity"]}
                                      rules={[
                                        {
                                          required: true,
                                          message:
                                            "Please enter product quantity!",
                                        },
                                        () => ({
                                          validator(rule, value) {
                                            const regex = /^\d+$/;
                                            if (!regex.test(value)) {
                                              return Promise.reject(
                                                "Please enter only number!"
                                              );
                                            }
                                            let inputProducts =
                                              formRef.current.getFieldValue(
                                                "sales"
                                              );
                                            if (!inputProducts)
                                              return Promise.resolve();
                                            const selectedProductId =
                                              inputProducts[index]?.product_id;

                                            let totalQty = 0;
                                            inputProducts.forEach((product) => {
                                              if (
                                                product.product_id ==
                                                +selectedProductId
                                              ) {
                                                totalQty += +product.quantity;
                                              }
                                            });
                                            const sales =
                                              formRef.current.getFieldValue(
                                                "sales"
                                              );
                                            let product = products.find(
                                              (item) => {
                                                return (
                                                  item.id ===
                                                  sales[index]?.product_id
                                                );
                                              }
                                            );
                                            if (!product)
                                              return Promise.resolve();
                                            const quantity = +product.quantity;
                                            const saleQuantity =
                                              +product.sale_quantity;
                                            let remQty =
                                              quantity - saleQuantity;

                                            const calcQty = remQty - totalQty;

                                            if (remQty < totalQty) {
                                              setSubmitDisable(true);
                                              setRemainingQuantity({
                                                ...remainingQuantity,
                                                [product.id]: remQty,
                                              });
                                              const salesRef =
                                                formRef.current.getFieldValue(
                                                  "sales"
                                                );
                                              Object.assign(salesRef[index], {
                                                quantity: 0,
                                              });
                                              formRef.current.setFieldsValue({
                                                salesRef,
                                              });

                                              return Promise.reject(
                                                "Please enter quantity less than the actual product quantity"
                                              );
                                            }

                                            setSubmitDisable(true);
                                            setRemainingQuantity({
                                              ...remainingQuantity,
                                              [index]: calcQty,
                                            });
                                            return Promise.resolve();
                                          },
                                        }),
                                      ]}
                                      noStyle
                                    >
                                      <Input
                                        placeholder="Quantity"
                                        onChange={(e) =>
                                          handleQuantity({
                                            key: index,
                                            value: e.target.value,
                                          })
                                        }
                                      />
                                    </Form.Item>
                                    <span>
                                      Remaining Quantity :{" "}
                                      {/* {remainingQty(index)
                                        ? remainingQty(index)
                                        : 0} */}
                                      {remainingQuantity[index]
                                        ? remainingQuantity[index]
                                        : 0}
                                    </span>
                                  </Form.Item>
                                </div>

                                <div
                                  style={{
                                    display: "flex",
                                  }}
                                >
                                  <Form.Item
                                    style={{
                                      width: "45%",
                                      marginLeft: 10,
                                    }}
                                    label="Paid Amount"
                                  >
                                    <Form.Item
                                      {...restField}
                                      name={[name, "paid_amount"]}
                                      fieldKey={[fieldKey, "paid_amount"]}
                                      rules={[
                                        {
                                          required: true,
                                          message: "Please enter paid amount!",
                                        },
                                        () => ({
                                          validator(rule, value) {
                                            if (!calc(index))
                                              return Promise.resolve();
                                            const [billRate, price] =
                                              calc(index);

                                            let amountWithVat;
                                            const salesRef =
                                              formRef.current.getFieldValue(
                                                "sales"
                                              );

                                            const discount =
                                              salesRef[index]?.discount;
                                            const quantity =
                                              salesRef[index]?.quantity;
                                            const amountWithoutVat =
                                              price * quantity;

                                            amountWithVat =
                                              +amountWithoutVat +
                                              (billRate > 0
                                                ? amountWithoutVat * billRate
                                                : 0);

                                            const calcRemAmt = discount
                                              ? amountWithVat - value - discount
                                              : amountWithVat - value;
                                            const initialAmt = discount
                                              ? amountWithVat - discount
                                              : amountWithVat;
                                            if (initialAmt < value) {
                                              setSubmitDisable(true);
                                              setRemainingAmount({
                                                ...remainingAmount,
                                                [index]: initialAmt.toFixed(2),
                                              });

                                              return Promise.reject(
                                                "Please enter amount less than total payable amount."
                                              );
                                            }
                                            setSubmitDisable(false);

                                            setRemainingAmount({
                                              ...remainingAmount,
                                              [index]: calcRemAmt.toFixed(2),
                                            });
                                            // discountToggle(key, value);

                                            const regex = /^\d+(\.\d{1,2})?$/;
                                            if (!regex.test(value)) {
                                              return Promise.reject(
                                                "Please enter only number!"
                                              );
                                            }
                                            setRemAmtChange(
                                              () => !remAmtChange
                                            );

                                            return Promise.resolve();
                                          },
                                        }),
                                      ]}
                                      noStyle
                                    >
                                      <Input placeholder="Paid" />
                                    </Form.Item>
                                    <span>
                                      Remaining Amount :{" "}
                                      {Math.sign(remainingAmount[index]) ===
                                        -1 ||
                                      Object.keys(remainingAmount).length < 1
                                        ? 0
                                        : remainingAmount[index]}
                                    </span>
                                  </Form.Item>

                                  <Form.Item
                                    {...restField}
                                    label="Discount"
                                    name={[name, "discount"]}
                                    fieldKey={[fieldKey, "discount"]}
                                    style={{
                                      width: "49%",
                                      marginLeft: 20,
                                    }}
                                    rules={[
                                      () => ({
                                        validator(rule, value) {
                                          console.log("remove key value", key);
                                          if (!calc(index))
                                            return Promise.resolve();
                                          const [billRate, price] = calc(index);

                                          let amountWithVat;
                                          const salesRef =
                                            formRef.current.getFieldValue(
                                              "sales"
                                            );
                                          const paidAmount =
                                            salesRef[index]?.paid_amount;
                                          const quantity =
                                            salesRef[index]?.quantity;
                                          const amountWithoutVat =
                                            price * quantity;

                                          amountWithVat =
                                            +amountWithoutVat +
                                            (billRate > 0
                                              ? amountWithoutVat * billRate
                                              : 0);

                                          const calcRemAmt = paidAmount
                                            ? amountWithVat - paidAmount - value
                                            : amountWithVat - value;
                                          const initialAmt = paidAmount
                                            ? amountWithVat - paidAmount
                                            : amountWithVat;
                                          if (isNaN(calcRemAmt))
                                            return Promise.resolve();

                                          if (initialAmt < value) {
                                            setSubmitDisable(true);

                                            setRemainingAmount({
                                              ...remainingAmount,
                                              [index]: initialAmt.toFixed(2),
                                            });

                                            return Promise.reject(
                                              "Please enter amount less than total payable amount."
                                            );
                                          }
                                          setSubmitDisable(false);

                                          setRemainingAmount({
                                            ...remainingAmount,
                                            [index]: calcRemAmt.toFixed(2),
                                          });
                                          setRemAmtChange(() => !remAmtChange);

                                          return Promise.resolve();
                                        },
                                      }),
                                    ]}
                                  >
                                    <Input
                                      disabled={discountDisable}
                                      onChange={(e) =>
                                        handleDiscount({
                                          key: index,
                                          value: e.target.value,
                                        })
                                      }
                                      placeholder=" Discount Amount"
                                    />
                                  </Form.Item>
                                </div>

                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    marginBottom: "1rem",
                                  }}
                                >
                                  <Button
                                    type="button"
                                    onClick={() => {
                                      delete remainingAmount[index];
                                      setRemAmtChange(() => !remAmtChange);
                                      remove(name);
                                    }}
                                    icon={<MinusCircleOutlined />}
                                  >
                                    Remove Products
                                  </Button>
                                </div>
                              </div>
                            )
                          )}
                          <Form.Item>
                            <Button
                              type="dashed"
                              onClick={() => add()}
                              block
                              icon={<PlusOutlined />}
                            >
                              Add Product
                            </Button>
                          </Form.Item>
                        </>
                      )}
                    </Form.List>
                  </>
                )}
                <Form.Item label=" " colon={false}>
                  <Button
                    type="primary"
                    htmlType="submit"
                    ghost
                    disabled={submitDisable}
                  >
                    Save
                  </Button>
                </Form.Item>
              </Form>
            </Spin>
          </Drawer>
          <Modal
            title="Create New Customer"
            width={650}
            centered
            onCancel={handleCustomerForm}
            visible={customerVisible}
            footer={null}
            afterClose={loadUsers}
          >
            <CustomerForm handleFormClose={handleCustomerForm} />
          </Modal>

          <Modal
            title=""
            width={850}
            centered
            onOk={handlePrint}
            onCancel={handlePrintForm}
            visible={printVisible}
            okText="Print"
            cancelText="Cancel"
          >
            <PrintForm
              ref={componentRef}
              customers={users}
              data={dataForPrint}
              products={products}
              bills={bills}
              discount={discountAmt}
              totalRemain={totalRemainingAmt}
              remainingAmount={remainingAmount}
              handleFormClose={handlePrintForm}
            />
          </Modal>
        </CCard>
      </CCol>
    </CRow>
  );
};

const mapStateToProps = (state) => ({
  gridData: state.gridData,
  initGrid: state.initGrid,
});

export default connect(mapStateToProps, { setInitGrid })(Sales);
