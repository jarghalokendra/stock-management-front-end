import React, { Component } from "react";
import { Card, Descriptions, Button } from "antd";
import { Table } from "react-bootstrap";
import "./print.css";
import NepaliDate from "../../components/classes/NepaliDate";
const nepali_date = new NepaliDate();

class Print extends Component {
  totalAmt = (a, b) => {
    let total = +a + +b;
    return total.toFixed(2);
  };
  calcProduct = (id) => {
    const product = this.props.products.find((product) => product.id === +id);
    return product.name;
  };

  calcbill = (id) => {
    const bill = this.props.bills.find((bill) => bill.id === +id);
    return bill.name;
  };

  calcPrice = (type, id) => {
    const product = this.props.products.find((item) => item.id === +id);
    let price =
      type === "retail" ? +product.price.retail_sp : +product.price.whole_sp;
    return price;
  };

  totalpaidAmt = () => {
    let totalpaid = [];
    this.props.data.sales.map((sale) => totalpaid.push(sale.paid_amount));
    return totalpaid.reduce((a, b) => +a + +b, 0).toFixed(2);
  };

  mergedobj = (arr1, arr2, arr3) => {
    let merged = [];

    for (let i = 0; i < arr1.length; i++) {
      merged.push({
        ...arr1[i],
        remain: arr2[i],
        discount: arr3[i],
      });
    }
    return merged;
  };

  single_customer = this.props.customers.filter(
    (customer) => customer.id === this.props.data.customer_id
  );
  mergeSale = this.mergedobj(
    this.props.data.sales,
    this.props.remainingAmount,
    this.props.discount
  );
  //console.log( mergeProduct);
  render() {
    //const { customer_id, sales } = this.props.data;
    // const { customers,bills,remainingAmount,products } = this.props;
    //const single_customer = customers.filter((customer) => customer.id === customer_id);
    //const mergeSale=mergedobj(sales,remainingAmount) ;
    return (
      <>
        <div id="printable" className="custom-style">
          <p className="h6">Customer Details</p>
          {this.single_customer.map((customer, index) => {
            return (
              <Descriptions key={index} title="" bordered size="small">
                <Descriptions.Item label="Email" span={3}>
                  {customer.email}
                </Descriptions.Item>
                <Descriptions.Item label="Address" span={3}>
                  {customer.address}
                </Descriptions.Item>
                <Descriptions.Item label="Phone" span={3}>
                  {customer.phone}
                </Descriptions.Item>
              </Descriptions>
            );
          })}

          <Card title="Sold Products">
            <Table>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Sales Type</th>
                  <th>Bill Type</th>
                  <th>Paid Amount</th>
                  <th>Remaining Amount</th>
                  <th>Discount Amount</th>
                  <th>Total Amount</th>
                </tr>
              </thead>
              <tbody>
                {this.mergeSale.map((sale, index) => {
                  return (
                    <tr key={index}>
                      <td>{nepali_date.format(new Date())}</td>
                      <td>{this.calcProduct(sale.product_id)}</td>
                      <td>{sale.quantity}</td>
                      <td>
                        {this.calcPrice(sale.sales_type, sale.product_id)}
                      </td>
                      <td>{sale.sales_type}</td>
                      <td>{this.calcbill(sale.bill_type)}</td>
                      <td>{sale.paid_amount}</td>
                      <td>{sale.remain}</td>
                      <td>{sale.discount}</td>
                      <td> {this.totalAmt(sale.paid_amount, sale.remain)}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </Card>
          <Descriptions title="" bordered size="small">
            <Descriptions.Item label="Total Received Amount" span={3}>
              {this.totalpaidAmt()}
            </Descriptions.Item>
            <Descriptions.Item label="Total Remaining Amount" span={3}>
              {this.props.totalRemain}
            </Descriptions.Item>
          </Descriptions>
        </div>
      </>
    );
  }
}

export default Print;
