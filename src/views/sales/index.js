import React, { useState } from "react";
import { CCard, CCol, CRow } from "@coreui/react";
import Sale from "./create";
import GridTable from "../../components/grid/index";
import Modal from "antd/lib/modal/Modal";
import Transaction from "./transaction";

const Sales = () => {
  const [visible, setVisible] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [edit, setEdit] = useState({});
  const [transactionVisible, setTransactionVisible] = useState(false);
  const [transactionInfo, setTransactionInfo] = useState({});

  const handleCreate = (e) => {
    e.preventDefault();
    setVisible(true);
    setIsEdit(false);
  };

  const handeEditAction = (e, row) => {
    e.preventDefault();
    setVisible(true);
    setIsEdit(true);
    setEdit(row);
  };

  const handleTransaction = (e, row) => {
    e.preventDefault();
    setTransactionInfo(row);
    setTransactionVisible(true);
  };

  const handleTransactionModal = () => {
    setTransactionVisible(false);
  };

  const columns = [
    {
      title: "Quantity",
      dataIndex: "quantity",
      order: 2,
      sorter: false,
      editable: false,
    },
    {
      title: "Paid Amount",
      order: 3,
      dataIndex: "paid_amount",
      sorter: false,
    },
    {
      title: "Sales Type",
      order: 4,
      dataIndex: "sales_type",
      sorter: false,
    },
    {
      title: "Created",
      dataIndex: "created_at",
      order: 6,
      sorter: true,
    },
  ];
  const acolumns = {
    apiurl: "sales",
    content_type: "sales",
    title: "Sales",
    reduxKey: "sales",
    settings: {
      show: true,
      createTitle: "Create Sales",
      createLink: "",
    },
    pivotRelation: {
      items: [
        // {
        //   column: "Price",
        //   show: true,
        //   order: "4",
        //   relationName: "product.price",
        //   relationColName: "selling_price",
        //   dType: "obj", //we can pass dType like array or object  ||Array->Many|| ||Obj->One||
        // },
        {
          column: "Product",
          show: true,
          order: 0,
          nested: false,
          relationName: "product",
          relationColName: "name",
          dType: "obj",
        },
        {
          column: "Customer",
          order: 1,
          show: true,
          nested: false,
          relationName: "customer",
          relationColName: "first_name",
          dType: "obj",
        },
        {
          column: "Bill Type",
          order: 5,
          show: true,
          nested: false,
          relationName: "bill",
          relationColName: "name",
          dType: "obj",
        },
      ],
    },
    actions: {
      show: true,
      edit: {
        show: true,
        name: "Edit",
        type: "popup", //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit},
        callFunction: "handleEdit",
      },
      transaction: {
        show: true,
        name: "Add Transaction",
        type: "popup",
        callFunction: "handleTransaction",
      },
      delete: {
        show: true,
        name: "Delete",
        type: "popup",
        callFunction: "handleDelete", // no need to add this option
      },
    },
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <GridTable
            columns={columns}
            acolumns={acolumns}
            handleCreate={(e) => handleCreate(e)}
            handleEdit={(e, row) => handeEditAction(e, row)}
            handleTransaction={(e, row) => handleTransaction(e, row)}
          />
        </CCard>
        {visible && (
          <Sale
            visible={visible}
            handleClose={() => setVisible(false)}
            isEdit={isEdit}
            edit={edit}
          />
        )}

        <Modal
          title="Add Transaction"
          width={800}
          centered
          visible={transactionVisible}
          onCancel={handleTransactionModal}
          footer={null}
          destroyOnClose={true}
        >
          <Transaction
            data={transactionInfo}
            handleFormClose={handleTransactionModal}
          />
        </Modal>
      </CCol>
    </CRow>
  );
};

export default Sales;
