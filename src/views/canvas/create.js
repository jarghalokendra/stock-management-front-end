import React,{useState, useEffect} from 'react'
import {fabric} from 'fabric'
import {connect} from 'react-redux'
import { CCard, CCol, CRow} from '@coreui/react'
import { useHistory, useLocation} from 'react-router-dom'
import {Button, Input,message, Spin} from 'antd'
import 'fabric-history';

import Cmenu from './menus/left-menu'
import CSubmenu from './menus/top-menu'
import styles from './canvas.module.css'
import RightMenu from './menus/right-menu'
import http from '../../config'
import {setCanvasObject, setCanvasConfig, setInitGrid} from '../../redux/actions/index'

const CanvasL = (props) => {
	const history = useHistory();
	const location = useLocation();
	const [canvas, setCanvas] = useState(''); 
	const [state, setState] = useState({
		name: '',
		loading: false,
		type: '',
		id: '',
		cHeight: '640',
		cWidth: '480'
	}) 
	let newCanvas = history.location.search.split('=')
	let newCanvasType;
	if(newCanvas.length === 2) {
		newCanvasType = newCanvas[1];
	}
	let path = history.location.pathname.split('/')
	let id = path[path.length-1];
	let type = path[path.length-2];
  if(props.settings){
    type = props.settings.type;
    id = props.settings.mode;
  }
  useEffect(() => {
   	initCanvas(id,type);
  }, [location]); 

  useEffect(()=>{
  	//console.log(props.canvasObject)
  },[props.canvasObject]) 

  const initCanvas = (id, type) => {
    let c = new fabric.Canvas('canvas', {
      backgroundColor: '#ffffff',
      useCORS: true,
      preserveObjectStacking: true,
      objectCaching:false
    })
    c.setHeight(state.cHeight);
    c.setWidth(state.cWidth);
    setCanvas(c);
    setState({
    	...state,
    	type: type,
    	id: id
    })
    if(id !== 'create'){
    	initEdit(type,id,c)
    }
  };
  
  const initEdit = async (type,id,canvas) =>{
  	try{
  		let r = await http.get(`/canvas/${type}/${id}/edit`);
  		r = r.data.edit;
  		setState({
  			...state,
  			name: r.name,
  		})
  		let json = r.canvas;
  		let jparse = JSON.parse(json)
  		let objects = [];
  		canvas.loadFromJSON(json, canvas.renderAll.bind(canvas), function(o, object) {
  		  objects.push(object)
  		});
  		r.config.isEdit = true;
  		r.config.isTextConfig = false;
  		if(r.config){  			
  			canvas.setHeight(r.config.cHeight);
  			canvas.setWidth(r.config.cWidth);
  			r.config.bgcolor = jparse.background;
  			r.config.canvas_size = r.config.canvas_size
  		}
  		props.setCanvasConfig(r.config)
  		props.setCanvasObject(objects);
  	}
  	catch(err){

  	}
  } 

  const handleSubmit = async() =>{
  	if(state.name === '' || canvasObject.length <= 0){
  		message.error('No canvas object(s) are slected!');
  		return
  	}
  	let json = canvas.toJSON()
  	let dataurl = canvas.toDataURL('image/jpeg');
  	let size = dataURItoBlob(dataurl);
  	
  	let file = new File([size], 'test', {
  	  type: "image/jpeg",
  	  lastModified: new Date(),
  	  size: size
  	});
  	let preview = URL.createObjectURL(file);
  	file.preview = preview

  	let formData = new FormData()
	  formData.append('preview', file);
	  formData.append('name', state.name);
	  formData.append('canvas', JSON.stringify(json));
	  formData.append('config', JSON.stringify(props.config));
	  formData.append('type',type);
  	try{
  	  setState({...state,loading: true});
  	  let url;
  	  if(newCanvasType === 'new-canvas' || id === 'create'){
  	  	type = newCanvasType === 'new-canvas' ? 'normal' : type;
  	  	url = id === 'create' ? `/canvas/${type}/create` : `/canvas/normal/create` ;
  	  }
  	  else{
  	  	url = `/canvas/${type}/update/${id}`
  	  }
  	  
  	  let result = await http.post(url, formData);

      //if canvas is created from media list
      if(props.settings){
        props.setInitGrid({init: true})
      }
      else{
        history.push(`/canvas?type=${type}`)
      }
  	  setState({...state, loading: false})
  	}
  	catch(err){
  		setState({...state, loading: false})
  	}
  }
  const dataURItoBlob = (dataURI) => {
	  // convert base64 to raw binary data held in a string
	  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
	  var byteString = atob(dataURI.split(',')[1]);

	  // separate out the mime component
	  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

	  // write the bytes of the string to an ArrayBuffer
	  var ab = new ArrayBuffer(byteString.length);
	  var ia = new Uint8Array(ab);
	  for (var i = 0; i < byteString.length; i++) {
	      ia[i] = byteString.charCodeAt(i);
	  }
	  // write the ArrayBuffer to a blob, and you're done
	  var bb = new Blob([ab]);
	  return bb;
  }
  const handleInputTitle = (e) =>{
  	setState({
  		...state,
  		name: e.value
  	})
  }
  const {canvasObject} = props;
	return (
		<>
			<Spin spinning={state.loading} tip="Loading...">
				<CRow className="test">
					<CCol xl={12} md={12}>
						<div className={styles['title-save-wrapper']}>
							<div className={styles['title-wrapper']}>
								<label htmlFor="title">Title</label>
								<Input 
									id="title"
									placeholder="Please enter the name" 
									value={state.name} 
									name="name"
									onChange={(e)=>handleInputTitle({
										name: e.target.name,
										value: e.target.value
									})}
								/>
							</div>
							<div className={styles['button-submit']}>
								<Button 
									onClick={()=>handleSubmit()} 
									disabled={state.name === '' || canvasObject.length <= 0} 
									type="primary" style={{float:'right'}}
								>Save</Button>
							</div>
						</div>
					</CCol>
					<CCol xl={12} md={12}>
						<div className={styles['canvas-sidebar-content-wrapper']}>
							<div className={styles['side-bar-menu']}>
								<Cmenu canvas={canvas}/>
							</div>
							<div className={styles['canvas-menu-wrapper']}>
								<div className={styles['top-menu']}>
									<CSubmenu canvas={canvas}/>
								</div>
								<div className={styles['canvas-wrapper']}>
									<canvas id="canvas"/>
								</div>
							</div>
							<div className={styles['right-menus']}>
								<RightMenu canvas={canvas} canvasSize={state.canvas_size}/>
							</div>
						</div>
					</CCol>
				</CRow>
			</Spin>
		</>
	)
}
const mapStateToProps = state => ({
	canvasObject: state.canvasObject,
	config: state.config
})
export default connect(mapStateToProps, {setCanvasObject, setCanvasConfig, setInitGrid}) (CanvasL)