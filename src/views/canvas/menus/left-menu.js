import React,{useState, useEffect} from 'react';
import { Menu,Modal,Button,Tooltip,Divider,Popover} from 'antd';
import { FontColorsOutlined} from '@ant-design/icons';
import {fabric} from 'fabric'
import {connect} from 'react-redux'
import { BsImageAlt, BsFillCircleFill, 
	BsSquareFill, BsTriangleFill, 
	BsTabletLandscape, BsSlash,BsFillPentagonFill,
	BsFillOctagonFill,BsStarFill,BsHeartFill
} from 'react-icons/bs';
import { FaDrawPolygon} from 'react-icons/fa';
import { BiCube} from 'react-icons/bi';
import { FiHexagon} from 'react-icons/fi';

import styles from './leftmenu.module.css'
import Upload from '../../../components/uploads/index'
import MediaList from '../medias/index'
import {setCanvasMenu, setCanvasObject} from '../../../redux/actions/index'
const { SubMenu } = Menu;

const CanvasMenu = (props) => {
	const [state, setState] = useState({
		visible: false,
		mVisible: false
	})
	const {canvas} = props

	useEffect(()=>{
		// console.log(props.gridData)
	}, [props.gridData])

	useEffect(()=>{
		//console.log(props.selectedGridIds)
	},[props.selectedGridIds])

	const handleCanvasShape = (canvas,type) => {
		let rect;
		if(type === 'rectangle'){
			rect = new fabric.Rect({
			  height: 30,
			  width: 50,
			  fill: '#ffa500',
			  top: 100, 
				left: 100 
			});
		}
		else if(type === 'polygon'){
			return 0;
		}
		else if(type === 'image'){
			setState({
				mVisible: true
			})
		}
		else if(type === 'square'){
			rect = new fabric.Rect({
			  height: 50,
			  width: 50,
			  fill: '#ff0000',
			  top: 100, 
				left: 100 
			});
		}
		else if(type === 'pentagon'){
			let points=regularPolygonPoints(5,30);
			rect = new fabric.Polygon(points, {
			  left: 20,
			  top: 20,
			  //strokeWidth: 2,
			  strokeLineJoin: 'bevil',
			  fill: '#321fdb'
			},false);
		}
		else if(type === 'hexagon'){
			let points=regularPolygonPoints(6,30);
			rect = new fabric.Polygon(points, {
			  left: 20,
			  top: 20,
			  strokeWidth: 2,
			  strokeLineJoin: 'bevil',
			  fill: '#321fdb'
			},false);
		}
		else if(type === 'septagon'){
			let points=regularPolygonPoints(7,30);
			rect = new fabric.Polygon(points, {
			  left: 20,
			  top: 20,
			  strokeWidth: 2,
			  strokeLineJoin: 'bevil',
			  fill: '#321fdb'
			},false);
		}
		else if(type === 'octagon'){
			let points=regularPolygonPoints(8,30);
			rect = new fabric.Polygon(points, {
			  left: 20,
			  top: 20,
			  strokeWidth: 2,
			  strokeLineJoin: 'bevil',
			  fill: '#321fdb'
			},false);
		}
		else if(type === 'star'){
			let points=starPolygonPoints(5,30,10);
			rect = new fabric.Polygon(points, {
			  stroke: '#ffffff',
			  left: 100,
			  top: 10,
			  fill: '#ffff00',
			  // strokeWidth: 2,
			  strokeLineJoin: 'bevil'
			},false);
		}
		else if(type === 'heart'){
			rect = new fabric.Path('M 272.70141,238.71731 \
			    C 206.46141,238.71731 152.70146,292.4773 152.70146,358.71731  \
			    C 152.70146,493.47282 288.63461,528.80461 381.26391,662.02535 \
			    C 468.83815,529.62199 609.82641,489.17075 609.82641,358.71731 \
			    C 609.82641,292.47731 556.06651,238.7173 489.82641,238.71731  \
			    C 441.77851,238.71731 400.42481,267.08774 381.26391,307.90481 \
			    C 362.10311,267.08773 320.74941,238.7173 272.70141,238.71731  \
			    z ');    
			let scale = 40 / rect.width;
			rect.set({ left: 20, top: 20, scaleX: scale, scaleY: scale,  fill: '#fe0000', });
		}
		else if(type === 'line'){
			rect = new fabric.Line([50, 10, 180, 50], { 
        stroke: '#321fd9',
        top: 100, 
				left: 100 
      }); 
		}
		else if(type === 'traingle'){
			rect = new fabric.Triangle({ 
        width: 40, 
        height: 40, 
        fill: '#00b9ff', 
        strokeWidth: 2,
        top: 20, 
				left: 100 
    	});
		}
		else if(type==='text'){
			rect = new fabric.Textbox("My Text",{
				fontSize: 16,
				width: 100,
				fill: '#000',
				top: 100, 
				left: 100,
				fontFamily: 'Verdana'
			})
			props.setCanvasMenu({type: type})
		}
		else if(type==='color'){
			rect = new fabric.Color({
				color: 'rgb(0-255,0-255,0-255)'
			})
		}
		else if(type === 'circle'){
			rect = new fabric.Circle({ 
				radius: 30, 
				fill: '#800080', 
				top: 100, 
				left: 100 
			})
		  canvas.selectionColor = 'rgba(0,255,0,0.3)';
		  canvas.selectionBorderColor = '#fe0000';
		  canvas.selectionLineWidth = 5;
		}
    if(type !== 'image'){
    	canvas.add(rect);
    	canvas.renderAll();
    }
    props.setCanvasObject(canvas.getObjects())
  }

  const regularPolygonPoints = (sideCount,radius)=>{
    let sweep=Math.PI*2/sideCount;
    let cx=radius;
    let cy=radius;
    let points=[];
    for(let i=0;i<sideCount;i++){
      let x=cx+radius*Math.cos(i*sweep);
      let y=cy+radius*Math.sin(i*sweep);
      points.push({x:x,y:y});
    }
    return(points);
  }
  const starPolygonPoints = (spikeCount, outerRadius, innerRadius)=>{
    let rot = Math.PI / 2;
    let cx = outerRadius;
    let cy = outerRadius;
    let sweep = Math.PI / spikeCount;
    let points = [];
    let angle = -180;
    for (let i = 0; i < spikeCount; i++) {
      let x = cx + Math.cos(angle) * outerRadius;
      let y = cy + Math.sin(angle) * outerRadius;
      points.push({x: x, y: y});
      angle += sweep;

      x = cx + Math.cos(angle) * innerRadius;
      y = cy + Math.sin(angle) * innerRadius;
      points.push({x: x, y: y});
      angle += sweep
    }
    return (points);
  }
  const handleMenus =(e, key)=>{
  	e.preventDefault();
  	handleCanvasShape(canvas,key)
  }
  const handleMediaClose = () =>{
  	setState({mVisible: false})
  }
  const handleMediaConfirm = () => {
  	const {selectedGridIds, gridData} = props;
  	if(selectedGridIds.length > 0){
  		let id = selectedGridIds[0];
  		let img = gridData.medias.find((item)=>{
  			return item.id === id;
  		})
		  let path = img.save_path;
  		fabric.util.loadImage(path, function(img) {
		    let object = new fabric.Image(img);
		    object.set({ 
	        left: 0, 
	        top: 0
		    });
		    object.hasRotatingPoint = true;
		    object.scaleX = object.scaleY = .25;
		    object.scaleToHeight(300);
		    object.scaleToWidth(300);
		    canvas.add(object);
		    canvas.renderAll();  
		    setState({mVisible: false})  
  		}, null, {
		    crossOrigin: 'anonymous'
		  });
  	}
  }
  const handleUploadClose = () => {
  	setState({
  		mVisible: true,
  		visible: false
  	})
  }
  const handleUploadOpen = () =>{
  	setState({
  		visible: true,
  		mVisible: true
  	})
  }
  const content = (
  	<div className={styles['polygon-wrapper']}>
      <a href="#" onClick={(e)=>handleMenus(e,'pentagon')}>
				<Tooltip placement="bottom" title="Add pentagon">
					<BsFillPentagonFill/>
				</Tooltip>
			</a>
			<a href="#" onClick={(e)=>handleMenus(e,'hexagon')}>
				<Tooltip placement="bottom" title="Add hexagon">
					<FiHexagon/>
				</Tooltip>
			</a>
			<a href="#" onClick={(e)=>handleMenus(e,'septagon')}>
				<Tooltip placement="bottom" title="Add Septagon">
					<FiHexagon/>
				</Tooltip>
			</a>
			<a href="#" onClick={(e)=>handleMenus(e,'octagon')}>
				<Tooltip placement="bottom" title="Add Octagon">
					<BsFillOctagonFill/>
				</Tooltip>
			</a>
			<a href="#" onClick={(e)=>handleMenus(e,'star')}>
				<Tooltip placement="bottom" title="Add Star">
					<BsStarFill/>
				</Tooltip>
			</a>
			<a href="#" onClick={(e)=>handleMenus(e,'heart')}>
				<Tooltip placement="bottom" title="Add Heart">
					<BsHeartFill/>
				</Tooltip>
			</a>
    </div>
   )
	return(
		<>
			<div className={styles['menu-wrapper']}>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'text')}>
						<Tooltip placement="bottom" title="Add Text">
							<FontColorsOutlined/>
						</Tooltip>
					</a>
				</div>
				<Divider type="horizontal" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'image')}>
						<Tooltip placement="bottom" title="Add Image">
							<BsImageAlt/>
						</Tooltip>
					</a>
				</div>
				<Divider type="horizontal" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'circle')}>
						<Tooltip placement="bottom" title="Add Circle">
							<BsFillCircleFill/>
						</Tooltip>
					</a>
				</div>
				<Divider type="horizontal" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'square')}>
						<Tooltip placement="bottom" title="Add Square">
							<BsSquareFill/>
						</Tooltip>
					</a>
				</div>
				<Divider type="horizontal" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'rectangle')}> 
						<Tooltip placement="bottom" title="Add Rectangle">
							<BsTabletLandscape/>
						</Tooltip>
					</a>
				</div>
				<Divider type="horizontal" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'traingle')}>
						<Tooltip placement="bottom" title="Add Traingle">
							<BsTriangleFill/>
						</Tooltip>
					</a>
				</div>
				<Divider type="horizontal" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'polygon')}>
						<Popover content={content} title="Add Polygon" placement="topLeft">
						  <FaDrawPolygon/>
						</Popover>,
					</a>
				</div>
				<Divider type="horizontal" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'line')}>
						<Tooltip placement="bottom" title="Add Line">
							<BsSlash/>
						</Tooltip>
					</a>
				</div>
			</div>
			{
				state.visible &&
					<Upload visible={state.visible} handleClose={()=>handleUploadClose()}/>
			}
			{
				state.mVisible &&
					<Modal
						title="Set Image"
						visible={state.mVisible}
						onCancel={()=>handleMediaClose()}
						width={1200}
						bodyStyle={{padding:'0'}}
						style={{ top: 20 }}
						onOk={()=>handleMediaConfirm()}
						footer={[
						  <Button key="back" onClick={()=>handleMediaClose()}>
						    Cancel
						  </Button>,
						  <Button 
						  	key="submit" 
						  	type="primary" 
						  	onClick={()=>handleMediaConfirm()} 
						  >
						    Confirm
						  </Button>,
						]}
					>
						<Button onClick={()=>handleUploadOpen()} type="primary" style={{float: 'right', margin: '20px 30px 10px 30px'}}>
							Upload
						</Button>
						<MediaList/>
					</Modal>
			}
		</>
	)
}

const mapStateToProps = state => ({
	selectedGridIds: state.selectedGridIds,
	gridData: state.gridData
})
export default connect(mapStateToProps, {setCanvasMenu, setCanvasObject}) (CanvasMenu)