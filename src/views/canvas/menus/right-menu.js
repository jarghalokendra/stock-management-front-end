import React,{useState, useEffect} from 'react'
import {connect} from 'react-redux'
import FontFaceObserver from 'fontfaceobserver'
import {fabric} from 'fabric'
import {Slider,Modal, InputNumber, Select, Checkbox, message, Radio,Input, Button} from 'antd'

import {setCanvasConfig} from '../../../redux/actions/index'
import styles from './rightmenu.module.css'
import Upload from '../../../components/uploads/index'
import MediaList from '../medias/index'

const RightMenu = (props) => {
	const options = ['bold', 'italic', 'underline'];
	const canvasSizes = ['640x480','800x600','1024x768','1280x960','1920x1080','custom'];
	const alignments = ['Left','Center','Right','Justify']
	const fontsizes = ['10', '11', '12', '13', '14','15', '16','18','20','22','25','30','35','45','60','75','90','120']
	const {canvas} = props;
	const [state, setState] = useState({
		fontFamily: '',
		fontSize: '',
		alignment: '',
		opacity: '',
		bold: false,
		italic: false,
		underline: false,
		isTextConfig: false,
		objectColor: '#000000',
		canvas_size: '640x480',
		cHeight: '',
		cWidth: '',
		bgcolor: '#ffffff',
		custom_size: false,
		bgflag:false
	})
	const [bgcog,setBgCog]=useState({
		visible: false,
		mVisible: false
	})
	const { Option } = Select;
	const fonts = ['Arial','Times New Roman','Helvetica','Delicious','Verdana','Georgia','Preeti']
	const notActiveObj = "No object is selected!";
	const notTextObj = "This setting is only applicable for text!"
	
	useEffect(()=>{
		watchCanvas()
	},[canvas])

	useEffect(()=>{
		if(props.config)
		setState({...state, ...props.config})
	},[props.config])

	useEffect(()=>{
		//console.log(props.selectedGridIds)
	},[props.selectedGridIds,props.gridData])

	const handleColorChange =(e, canvas)=>{
  	canvas.setBackgroundColor(e.value)
  	setState({...state, bgcolor: e.value})
  	props.setCanvasConfig({...state, bgcolor: e.value})
  	canvas.renderAll();
  }

  const watchCanvas = () =>{
  	let o ={};
	  if(canvas){
	  	canvas.on('mouse:down', function(obj){
	  		console.log(canvas.backgroundImage)
	  		if(canvas.backgroundImage){
	  			o.backgroundImage = canvas.backgroundImage.src;
	  			o.bgflag = true
	  		}
	  		else{
	  			o.bgcolor= canvas.backgroundColor;
	  			o.bgflag = false
	  		}
	  		let canvas_size = `${canvas.height}x${canvas.width}`;
	  		o.canvas_size= canvasSizes.indexOf(canvas_size) > -1 ? canvas_size : 'custom'
	  		let ob = obj.target
				o.isTextConfig = false
				o.custom_size = canvasSizes.indexOf(canvas_size) === -1
				o.cHeight = canvas.height;
				o.cWidth = canvas.width
				if(ob) {
					o.objectColor = ob.fill;
					o.opacity= ob.opacity*100;
					o.alignment = ob.textAlign
					if(ob.type === 'textbox')
					{
						let s = Object.keys(ob.styles).length > 0 ? ob.styles[0][0] : {};
						o = Object.assign(o,s);
						o.isTextConfig = true;
					}
				}
				canvasMouseEvent(o)
	  	})
	  }
  }

  const canvasMouseEvent = (o) =>{
  	let ob = canvas.getActiveObject();
  	o.italic =  o.fontStyle === 'italic' ? true : false
  	o.bold =  o.fontWeight === 'bold' ? true : false
  	setState({...props.config,...o})
  	props.setCanvasConfig({...state,...o})
  }

  const handleTextColor =(e, canvas)=>{
  	let obj = canvas.getActiveObject();
  	if(!obj){
  		displayMessage(notActiveObj)
  		return
  	}
		obj.set({
			fill: e.value
		})
		setState({...state, objectColor: e.value})
		canvas.renderAll();	
  }
  const handleFont = (e,canvas) => {
  	let obj = canvas.getActiveObject()
  	if(!obj){
  		displayMessage(notActiveObj)
  		return
  	}
  	if(obj.type !== 'textbox'){
  		displayMessage(notTextObj)
  		return
  	}
  	if(e==='Preeti'){
  		let font =e ;
  		let myfont = new FontFaceObserver(font)
		  myfont.load()
	    .then(function() {
	      // when font is loaded, use it.
	      obj.set("fontFamily", font)
	      obj.setSelectionStyles({
	      	fontFamily: font
	      })
	    }).catch(function(e) {
	      console.log(e)
	      alert('font loading failed ' + font);
	    });
  		
  	}
  	else{
  		obj.set("fontFamily", e)
  		obj.setSelectionStyles({
  			fontFamily: e
  		})
  	}
		props.setCanvasConfig({...state, fontFamily: e})
		setState({
			...state,
			fontFamily: e
		})
		canvas.renderAll();
  }

  const handleFontsize = (e,canvas) => {
  	let obj = canvas.getActiveObject()
  	if(!obj){
  		displayMessage(notActiveObj)
  		return
  	}
  	if(obj.type !== 'textbox'){
  		displayMessage(notTextObj)
  		return
  	}
  	obj.setSelectionStyles({
  		fontSize: e
  	})
  	props.setCanvasConfig({...state, fontSize: e})
  	setState({
  		...state,
  		fontSize: e,
  	})
  	canvas.renderAll();
  }

  const handleAlignment = (e, canvas) => {
  	let obj = canvas.getActiveObject();
  	if(!obj){
  		displayMessage(notActiveObj)
  		return
  	}
  	if(obj.type !== 'textbox'){
  		displayMessage(notTextObj)
  		return
  	}
  	obj.textAlign = e.toLowerCase()
  	props.setCanvasConfig({...state, alignment: e.toLowerCase()})
  	setState({...state, alignment: e.toLowerCase()})
  	canvas.renderAll();
  }
  const handleSlider = (e,canvas) => {
  	let obj = canvas.getActiveObject()
  	if(!obj){
  		displayMessage(notActiveObj)
  		return
  	}
  	obj.set({
  		opacity: e/100
  	})
  	props.setCanvasConfig({...state, opacity: e})
  	setState({
  		...state,
  		opacity: e
  	})
  	canvas.renderAll();
  }
  const displayMessage = (mgs) => {
		message.error(mgs);
	}
  const handleFontOptions = (e) => {
  	let obj = canvas.getActiveObject();
  	let name = e.target.name;
  	let value = e.target.checked;
  	setState({
  		...state,
  		[name]: value
  	})
  	if(!obj){
  		displayMessage(notActiveObj)
  		return
  	}
  	if(obj.type !== 'textbox'){
  		displayMessage(notTextObj)
  		return
  	}
  	if(name === 'bold'){
  		obj.setSelectionStyles({fontWeight: value ? 'bold' : 'normal'})
  	}
  	else if(name === 'underline'){
  		obj.setSelectionStyles({underline: value ? true : false})
  	}
  	else if(name === 'italic'){
  		obj.setSelectionStyles({fontStyle: value ? 'italic' : 'normal'})
  	}  	
  	props.setCanvasConfig({...state, [name]: value})
  	setState({...state, [name]: value})
  	canvas.renderAll();
  }
  const handleCustomSize = (e, name, canvas) =>{
  	setState({...state, [name]:e})
  	props.setCanvasConfig({...state, [name]:e})
  }
  const handleCustomSave = (e,canvas) => {
  	if(state.cHeight && state.cWidth){
  		canvas.setHeight(state.cHeight);
  		canvas.setWidth(state.cWidth);
  	}
  }
  const handleCanvasSize = (e,canvas) => {
  	let custom_size;
  	if(e === 'custom'){
  		setState({...state, custom_size: true, canvas_size: e, cHeight:'',cWidth: ''})
  		props.setCanvasConfig({...state, custom_size: true, canvas_size: e, cHeight:'',cWidth: ''})
			canvas.renderAll();
  	}
  	else{
  		let size = e.split('x');
  		let cHeight = size[0];
	  	let cWidth = size[1];
	  	canvas.setHeight(cHeight);
			canvas.setWidth(cWidth);
			setState({...state,canvas_size: e,  cHeight: cHeight, cWidth:cWidth, custom_size: false})
			props.setCanvasConfig({...state, cHeight: cHeight, cWidth:cWidth, canvas_size: e, custom_size: false})
			canvas.renderAll();
  	}
  }

  const handleUploadClose = () =>{
  	setBgCog({
  		visible: false,
  		mVisible: true
  	})
  }

  const handleMediaClose = () => {
  	setBgCog({mVisible: false})
  }
  const handleUploadOpen = () => {
  	setBgCog({
  		visible: true,
  		mVisible: true
  	})
  }
  
  const handleMediaConfirm = (canvas) => {
  	const {selectedGridIds, gridData} = props;
  	if(selectedGridIds.length > 0){
  		let id = selectedGridIds[0];
  		let data = gridData.medias.find((item)=>{
  			return item.id === id;
  		})
		  let path = data.save_path;
  		fabric.Image.fromURL(path, function(img) {
  			img.src=path;
				canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
				  scaleX: canvas.width / img.width,
				  scaleY: canvas.height / img.height
				});
				setBgCog({mVisible: false})
				setState({...state, backgroundImage:path, bgflag: true})
				props.setCanvasConfig({...state, backgroundImage:path, bgflag: true})
      },{ crossOrigin: 'anonymous' });
		}
  }
  const handleRemoveBgImage =() =>{
  	canvas.setBackgroundImage(null, canvas.renderAll.bind(canvas))
		setState({...state, backgroundImage:null, bgflag: false})
		props.setCanvasConfig({...state, backgroundImage:null, bgflag: false})

  }
	return(
		<>
			<div className={styles['right-menu-wrapper']}>
				<div className={styles['canvas-size']}>
		      <label htmlFor="text-align">Canvas Size</label>
		      <Select
	          showSearch
	          style={{ width: 120 }}
	          placeholder="Size"
	          optionFilterProp="children"
	          onChange={(e)=>handleCanvasSize(e,canvas)}
	          value={state.canvas_size}
	          filterOption={(input, option) =>
	            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
	          }
		        >
		        	{
		        		canvasSizes.map((item,index)=>{
		        			return(
		        				<Option key={index} value={item}>{item}</Option>
		        			)
		        		})
		        	}
		      </Select>
				</div>
				{
					state.custom_size &&
						<div className={styles['custom-size']}>
							<div className={styles['form-wrapper']}>
								<label htmlFor="height">Height</label>
								<InputNumber 
									onChange={(e)=>handleCustomSize(e,'cHeight',canvas)} 
									id="height" 
									min={100}
									size="small" 
									name="cHeight" 
									value={state.cHeight}
									max={3000}
								/>
							</div>
							<div className={styles['form-wrapper']}>
								<label htmlFor="width">width</label>
								<InputNumber 
									onChange={(e)=>handleCustomSize(e,'cWidth',canvas)} 
									id="width" 
									size="small" 
									min={100}
									max={3000}
									name="cWidth" 
									value={state.cWidth}
								/>
							</div>
							<div className={styles['custom-size-button']}>
								<Button onClick={(e)=>handleCustomSave(e, canvas)} size="small">Save</Button>
							</div>
						</div>
				}
				<div className={styles['color-wrapper']}>
					<div className={styles['right-menu-bg-color']}>
						<p>Background</p>
							{
								!state.bgflag &&
									<div className={styles['bg-wrapper']}>
										<input style={{width: '50px'}} id="color" value={state.bgcolor || ''} name="bgcolor" onChange={(e)=>handleColorChange({name: e.target.name, value: e.target.value},canvas)} type="color"/>
										<Button onClick={()=>setBgCog({mVisible: true})} size="small">Use Image</Button>
									</div>
							}
							{
								state.bgflag &&
								<div className={styles['bg-wrapper']}>
									<img src={state.backgroundImage}/>
									<Button onClick={()=>handleRemoveBgImage()} size="small">Remove Image</Button>
								</div>
							}
					</div>
					<div className={styles['right-menu-color']}>
						<p>Text Color</p>
						<input style={{width: '50px'}} id="color" value={state.objectColor || ''} name="objcolor" onChange={(e)=>handleTextColor({name: e.target.name, value: e.target.value},canvas)} type="color"/>
					</div>
				</div>
				{
					state.isTextConfig &&
						<div className={styles['text-config-wrapper']}>
							<div className={styles['fonts']}>
					      <label htmlFor="font-family">Font family:</label>
					      <Select
				          showSearch
				          style={{ width: 120 }}
				          placeholder="Select font"
				          optionFilterProp="children"
				          onChange={(e)=>handleFont(e, canvas)}
				          value={state.fontFamily}
				          filterOption={(input, option) =>
				            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
				          }
					        >
					        	{
					        		fonts.map((item, index)=>{
					        			return(
					        				<Option key={index} value={item}>{item}</Option>
					        			)
					        		})
					        	}
					        </Select>
							</div>
							<div className={styles['fonts']}>
					      <label htmlFor="font-size">Font Size:</label>
			  	      <Select
			            showSearch
			            style={{ width: 120 }}
			            placeholder="Select font size"
			            optionFilterProp="children"
			            onChange={(e)=>handleFontsize(e, canvas)}
			            value={state.fontSize}
			            filterOption={(input, option) =>
			              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
			            }
			  	        >
				        	{
				        		fontsizes.map((item, index)=>{
				        			return(
				        				<Option key={index} value={item}>{item}</Option>
				        			)
				        		})
				        	}
			  	      </Select>
							</div>
							<div className={styles['fonts']}>
					      <label htmlFor="text-align">Alignmnent</label>
			  	      <Select
			            showSearch
			            style={{ width: 120 }}
			            placeholder="Alignmnent"
			            optionFilterProp="children"
			            onChange={(e)=>handleAlignment(e,canvas)}
			            value={state.alignment}
			            filterOption={(input, option) =>
			              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
			            }
			  	        >
			  	        	{
			  	        		alignments.map((item,index)=>{
			  	        			return(
			  	        				<Option key={index} value={item}>{item}</Option>
			  	        			)
			  	        		})
			  	        	}
			  	        </Select>
							</div>
							<div className={styles['options']}>
								<label htmlFor="options">Options</label>
								<div id="options" className={styles['option-wrapper']}>
						      <Checkbox name="bold" checked={state.bold} onChange={(e)=>handleFontOptions(e)}>Bold</Checkbox>
						      <Checkbox name="italic" checked={state.italic} onChange={(e)=>handleFontOptions(e)}>Italic</Checkbox>
						      <Checkbox name="underline" checked={state.underline} onChange={(e)=>handleFontOptions(e)}>Underline</Checkbox>
						     </div>
							</div>
						</div>
				}
				
				<div className={styles['sliders']}>
					<label htmlFor="Slider">Opacity</label>
	      	<Slider id="Slider" min={1} max={100} onChange={(e)=>handleSlider(e, canvas)} value={state.opacity}/>
	      </div>
			</div>
			{
				bgcog.visible &&
					<Upload visible={bgcog.visible} handleClose={()=>handleUploadClose()}/>
			}
			{
				bgcog.mVisible &&
					<Modal
						title="Set Image"
						visible={bgcog.mVisible}
						onCancel={()=>handleMediaClose()}
						width={1200}
						bodyStyle={{padding:'0'}}
						style={{ top: 20 }}
						onOk={()=>handleMediaConfirm(canvas)}
						footer={[
						  <Button key="back" onClick={()=>handleMediaClose()}>
						    Cancel
						  </Button>,
						  <Button 
						  	key="submit" 
						  	type="primary" 
						  	onClick={()=>handleMediaConfirm(canvas)} 
						  >
						    Confirm
						  </Button>,
						]}
					>
						<Button onClick={()=>handleUploadOpen()} type="primary" style={{float: 'right', margin: '20px 30px 10px 30px'}}>
							Upload
						</Button>
						<MediaList/>
					</Modal>
			}
		</>
	)
}

const mapStateToProps = state => ({
	canvasObject: state.canvasObject,
	selectedGridIds: state.selectedGridIds,
	gridData: state.gridData,
	config : state.config
})
export default connect(mapStateToProps, {setCanvasConfig}) (RightMenu)