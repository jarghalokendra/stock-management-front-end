import React,{useState, useEffect} from 'react'
import {fabric} from 'fabric'
import {connect} from 'react-redux'
import { Menu, Tooltip, message, Divider } from 'antd';
import {
	AlignCenterOutlined, 
	AlignLeftOutlined,
	AlignRightOutlined, 
	DeleteOutlined, 
	UpOutlined, 
	DownOutlined, 
	UndoOutlined,
	RedoOutlined,
	SettingOutlined,
	ArrowUpOutlined,
	ArrowDownOutlined,
	DoubleLeftOutlined,
	DoubleRightOutlined
} from '@ant-design/icons';
import 'fabric-history';
import {setCanvasMenu, setCanvasObject} from '../../../redux/actions/index'
import styles from './topmenu.module.css'

const { SubMenu } = Menu;

const CMenus = (props)=>{
	const [state, setState] = useState({})
	const {canvas} = props;
	const handleMenus = (e,key) => {
		e.preventDefault();
		let obj = canvas.getActiveObject()
		if(key === 'delete'){
			if(!obj){
				displayMessage()
				return 
			}
			if(obj.type === 'textbox'){
				props.setCanvasMenu({})
			}
			canvas.remove(obj);
			props.setCanvasObject(canvas.getObjects())
		}
		else if(key === 'undo'){
			canvas.undo()
			props.setCanvasObject(canvas.getObjects())
		}
		else if(key === 'redo'){
			canvas.redo();
			props.setCanvasObject(canvas.getObjects())
		}
		else if(key === 'bring-forward'){
			canvas.bringToFront(obj);
		}
		else if(key === 'bring-to-front'){
			canvas.bringForward(obj);
		}
		else if(key === 'send-backward'){
			canvas.sendToBack(obj);
		}
		else if(key === 'bring-to-back'){
			canvas.sendBackwards(obj);
		}
		else {
			if(!obj){
				displayMessage()
				return 
			}
			processAlign(key, obj)
			obj.setCoords();
		}
		canvas.renderAll();
	}

	const displayMessage = () => {
		message.error('No active object is selected!');
	}

	const processAlign = (val,obj)=>{
	  switch (val) {
	    case 'left':
	      obj.set({
	        left: 0
	      });
	      break;
	    case 'right':
	      obj.set({
	        left: canvas.width - (obj.width * obj.scaleX)
	      });
	      break;
	    case 'top':
	      obj.set({
	        top: 0
	      });
	      break;
	    case 'bottom':
	      obj.set({
	        top: canvas.height - (obj.height * obj.scaleY)
	      });
	      break;
	    case 'center':
	      obj.set({
	        left: (canvas.width / 2) - ((obj.width * obj.scaleX) / 2)
	      });
	      break;
	  }
	}
	return (
		<>
			<div className={styles['top-menu-wrappers']}>
				<Divider type="vertical" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'bring-forward')}>
						<Tooltip placement="bottom" title="Bring Forward">
							<ArrowUpOutlined />
						</Tooltip>
					</a>
					<a href="#" onClick={(e)=>handleMenus(e, 'send-backward')}>
						<Tooltip placement="bottom" title="Send Backward">
							<ArrowDownOutlined />
						</Tooltip>
					</a>
					<a href="#" className={styles['bring-to-front']} onClick={(e)=>handleMenus(e,'bring-to-front')}>
						<Tooltip placement="bottom" title="Bring to front">
							<DoubleLeftOutlined />
						</Tooltip>
					</a>
					<a href="#" className={styles['bring-to-back']} onClick={(e)=>handleMenus(e,'bring-to-back')}>
						<Tooltip placement="bottom" title="Bring to back">
							<DoubleRightOutlined />
						</Tooltip>
					</a>
				</div>
				<Divider type="vertical" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'left')}>
						<Tooltip placement="bottom" title="Left">
							<AlignLeftOutlined/>
						</Tooltip>
					</a>
					<a href="#" onClick={(e)=>handleMenus(e, 'center')}>
						<Tooltip placement="bottom" title="Center">
							<AlignCenterOutlined/>
						</Tooltip>
					</a>
					<a href="#" onClick={(e)=>handleMenus(e,'right')}>
						<Tooltip placement="bottom" title="Right">
							<AlignRightOutlined/>
						</Tooltip>
					</a>
					<a href="#" onClick={(e)=>handleMenus(e,'up')}>
						<Tooltip placement="bottom" title="Up">
							<UpOutlined/>
						</Tooltip>
					</a>
					<a href="#" onClick={(e)=>handleMenus(e,'down')}>
						<Tooltip placement="bottom" title="Down">
							<DownOutlined/>
						</Tooltip>
					</a>
				</div>
				<Divider type="vertical" className={styles['divider']}/>
				<div className={styles['objects']}>
					<a href="#" onClick={(e)=>handleMenus(e,'undo')}>
						<Tooltip placement="bottom" title="Undo">
							<UndoOutlined/>
						</Tooltip>
					</a>
					<a href="#" onClick={(e)=>handleMenus(e, 'redo')}>
						<Tooltip placement="bottom" title="Redo">
							<RedoOutlined/>
						</Tooltip>
					</a>
					<a href="#" onClick={(e)=>handleMenus(e,'delete')}>
						<Tooltip placement="bottom" title="Delete">
							<DeleteOutlined/>
						</Tooltip>
					</a>
				</div>
				<Divider type="vertical" className={styles['divider']}/>
			</div>
		</>
	);
}
const mapStateToProps = state => ({})
export default connect(mapStateToProps, {setCanvasMenu, setCanvasObject}) (CMenus)




