import React from 'react'
import { CCard,CCol,CRow} from '@coreui/react'
import { Tabs} from 'antd';
import { withRouter } from 'react-router-dom'
// import Screen from './create'
import GridTable from '../../components/grid/index'
import styles from './index.module.css'
const { TabPane } = Tabs;

class Canvas extends React.Component {
  //formRef = React.createRef();
  state = {
    visible: false,
    loading: false,
    isEdit: false,
    edit: {},
    tabKey: '1'
  }
  componentDidMount(){
  	let path = this.props.location.search.split('=');
  	if(path.length === 2){
  		let tab = path[path.length-1];
  		let tabKey = tab === 'gallery' ? '2': '1'
  		this.setState({tabKey: tabKey})
  	}
  }
  handeEditAction(e,row){
    e.preventDefault()
    this.setState({
      visible:true,
      isEdit: true,
      edit: row
    })
  }
  handleCreate(){
    this.setState({
      visible:true,
      isEdit: false
    })
  }
  handleTab(e){
		this.setState({
			tabKey:e
		})
  }
  render(){
    const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: true,
      editable: true
    },
    {
      title: 'created_at',
      dataIndex: 'created_at',
      sorter: true,
    }]
    const acolumns = {
      apiurl: `${this.state.tabKey === '1' ? 'canvas/normal' : 'canvas/gallery'}`,
      title: `${this.state.tabKey === '1' ? 'Canvas' : 'Canvas Gallery'}`,
      reduxKey: `canvas${this.state.tabKey === '1' ? '' : '-gallery'}`,
      content_type: 'canvas',
      settings: {
        show:true,
        createTitle: `Create Canvas ${this.state.tabKey === '1' ? '' : 'Gallery'}`,
        createLink: `/canvas/${this.state.tabKey === '1' ? 'normal' : 'gallery'}/create`,
      },
      statusColumn: {
        column: 'status',
        show: true
      },
      fileColumn: {
      	column: 'preview',
        imageName: 'preview',
      	show: true,
      },	
      actions: {
        show: true,
        addNew:{
          show: this.state.tabKey === '1' ? false : true,
          name: 'Add New Canvas',
          url: `/canvas/gallery`
        },
        edit: {
          show: true,
          name: 'Edit',
          type: 'url' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
        },
        delete:{
          show: true,
          name: 'Delete',
        },
        screen: {
          show: this.state.tabKey === '1' ? true : false,
          name: 'Set to Screen',
          type: 'set-to-screen' //it should be only two types set-to-screen or set-content
        }
        
      }
    }  
    return (
      <CRow>
        <CCol xl={12} md={12}>
        	<CCard>
        		<div className="card-container">
		        	<Tabs type="card" className="canvas-tab" onChange={(e)=>this.handleTab(e)} activeKey={this.state.tabKey}>
		    	      <TabPane tab="My Canvas" key="1">
		  	      	  <GridTable 
		  	      	    columns={columns} 
		  	      	    acolumns={acolumns} 
		  	      	    handleCreate={()=>this.handleCreate()}
		  	      	    handleEdit={(e, row)=>this.handeEditAction(e, row)}
		  	      	  />
		    	      </TabPane>
		    	      <TabPane tab="Gallery" key="2">
		  	          <GridTable 
		  	            columns={columns} 
		  	            acolumns={acolumns} 
		  	            handleCreate={()=>this.handleCreate()}
		  	            handleEdit={(e, row)=>this.handeEditAction(e, row)}
		  	          />
		    	      </TabPane>
		    	    </Tabs>
		    	  </div>
	    	  </CCard>
        </CCol>
      </CRow>
    )
  }
}

export default withRouter(Canvas)