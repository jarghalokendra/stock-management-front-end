import React from "react";
import { CChartLine } from "@coreui/react-chartjs";
import { getStyle, hexToRgba } from "@coreui/utils/src";

const brandSuccess = getStyle("success") || "#4dbd74";
const brandInfo = getStyle("info") || "#20a8d8";
const brandDanger = getStyle("danger") || "#f86c6b";

const MainChartExample = (props) => {
  const { data } = props;
  const defaultDatasets = (() => {
    return [
      {
        label: "Users Added",
        backgroundColor: hexToRgba(brandInfo, 10),
        borderColor: brandInfo,
        pointHoverBackgroundColor: brandInfo,
        borderWidth: 2,
        data: data["users_count"],
      },
      {
        label: "Transations Incurred",
        backgroundColor: "transparent",
        borderColor: brandSuccess,
        pointHoverBackgroundColor: brandSuccess,
        borderWidth: 2,
        // borderDash: [8, 5],
        data: data["trans_count"],
      },
      {
        label: "Sales Incurred",
        backgroundColor: "transparent",
        borderColor: brandDanger,
        pointHoverBackgroundColor: brandDanger,
        borderWidth: 2,
        // borderDash: [8, 5],
        data: data["sales_count"],
      },
    ];
  })();

  const defaultOptions = (() => {
    return {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: "top",
      },
      title: {
        display: false,
        text: "Chart.js Line Chart",
      },
      scales: {
        xAxes: [
          {
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              // maxTicksLimit: 5,
              // stepSize: Math.ceil(100 / 5),
              // max: 100,
            },
            gridLines: {
              display: true,
            },
          },
        ],
      },
      elements: {
        point: {
          radius: 0,
          hitRadius: 10,
          hoverRadius: 4,
          hoverBorderWidth: 3,
        },
      },
    };
  })();

  // render
  return (
    <>
      <CChartLine
        {...props}
        datasets={defaultDatasets}
        options={defaultOptions}
        labels={data["date"]}
        // labels={[ "May", "Jun",  "Jul",  "Aug",  "Sep",   "Oct",  "Nov",  "Dec", "Jan",  "Feb","Mar", "Apr",]}
      />
    </>
  );
};

export default MainChartExample;
