import React from 'react'
//import { useHistory, useLocation } from 'react-router-dom'
import { CCard,CCol,CRow } from '@coreui/react'
//import { SettingOutlined, DownOutlined} from '@ant-design/icons';

import Upload from '../../components/uploads/index'
//import {DebounceInput} from 'react-debounce-input';
//import http from '../../config'
import GridTable from '../../components/grid/index'

// import GridTable from '../grid/index'
class Medias extends React.Component {
	formRef = React.createRef();
	state = {
		visible: false,
		loading: false,
	}
	render(){
		const columns = [
		{
		  title: 'Name',
		  dataIndex: 'original_name',
		  sorter: true,
		  editable: true
		},
		{
		  title: 'Duration',
		  dataIndex: 'duration',
		  sorter: true,
		  editable: true
		},
		{
		  title: 'created_at',
		  dataIndex: 'created_at',
		  sorter: true,
		}]
		const acolumns = {
		  apiurl: 'medias',
		  content_type: 'medias',
		  title: 'Medias',
		  reduxKey: 'medias',
		  settings: {
		  	show:true,
		    createTitle: 'Upload',
		    createLink: '',
		  },
		  statusColumn: {
		    column: 'status',
		    show: true
		  },
		  fileColumn: {
		    column: 'file',
		    imageName: 'save_path',
		    show: true
		  },
		  actions: {
		  	show: true,
		    edit: {
		      show: false,
		      name: 'Edit',
		      type: 'popup' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
		    },
		    delete:{
		      show: true,
		      name: 'Delete',
		    },
		    screen: {
		      show: true,
		      name: 'Set to Screen',
		      type: 'set-to-screen' //it should be only two types set-to-screen or set-content
		    }
		    
		  }
		}  
		return (
		  <CRow>
		    <CCol xl={12} md={12}>
		      <CCard>
		        <GridTable columns={columns} acolumns={acolumns} handleCreate={()=>this.setState({visible: true})}/>
		      </CCard>
		      {
		      	this.state.visible &&
		      		<Upload visible={this.state.visible} handleClose={()=>this.setState({visible: false})}/>
		      }
		    </CCol>
		  </CRow>
		)
	}
}

export default Medias