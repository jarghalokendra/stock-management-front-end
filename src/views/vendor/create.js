import React, { useState, useEffect, useRef } from "react";
import { useHistory } from "react-router";
import { Card, Form, Input, Button, Alert, Spin, message } from "antd";
import {
  UserOutlined,
  UserAddOutlined,
  PhoneOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import http from "../../config";

const VendorCreate = () => {
  const formRef = useRef(null);
  let history = useHistory();

  const [loading, setLoading] = useState(false);
  const [path, setPath] = useState([]);
  const [validationerror, setValidationerror] = useState("closed");
  const [errors, setErrors] = useState([]);

  useEffect(() => {
    let path = history.location.pathname.split("/");
    setPath(path);
    console.log(path[2]);
    if (path.length && path[2] != "create") {
      handleEdit(path[2]);
    }
  }, []);

  const handleEdit = async (id) => {
    try {
      setLoading(false);
      let result = await http.get(`/vendors`);
      let vendors = result.data.data.data;
      let vendor = vendors.find((item) => {
        return item.id === +id;
      });
      setTimeout(() => {
        formRef.current.setFieldsValue(vendor);
        setLoading(false);
      }, 1000);
    } catch (err) {
      console.log(err);
    }
  };

  const onFinish = async (values) => {
    try {
      setLoading(true);
      let url =
        path.length && path[2] != "create" ? `/vendors/${path[2]}` : `/vendors`;
      if (path.length && path[2] != "create") {
        await http.put(url, values);
      } else {
        await http.post(url, values);
      }
      history.push("/vendors");
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
        setValidationerror(error);
      }
      message.error({ content: error, duration: 5 });
      setLoading(false);
    }
  };
  return (
    <>
      <Card title={path[2] != "create" ? "Edit Party" : "Create New Party"}>
        <div className="title-wrapper"></div>
        {validationerror !== "closed" && (
          <Alert
            message="Validation Errors"
            description={validationerror}
            type="error"
            showIcon
            closable
            style={{ marginBottom: "10px" }}
          />
        )}
        {errors.length > 0 &&
          errors.map((item, index) => {
            return (
              <Alert
                key={index}
                message={item.error}
                type="error"
                showIcon
                closable
              />
            );
          })}
        <Spin spinning={loading}>
          <Card style={{ marginLeft: "auto", marginRight: "auto" }}>
            <Form
              ref={formRef}
              name="normal_login"
              initialValues={{ remember: true }}
              onFinish={onFinish}
            >
              <div style={{ display: "flex" }}>
                <Form.Item
                  name="first_name"
                  rules={[
                    {
                      required: true,
                      message: "Please input First Name of Vendor!",
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                  }}
                >
                  <Input
                    prefix={<UserAddOutlined className="site-form-item-icon" />}
                    placeholder="First Name"
                  />
                </Form.Item>
                <Form.Item
                  name="last_name"
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px",
                  }}
                >
                  <Input
                    prefix={<UserAddOutlined className="site-form-item-icon" />}
                    placeholder="Last Name"
                  />
                </Form.Item>
              </div>
              <div style={{ display: "flex" }}>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      type: "email",
                      message: "The input is not valid E-mail!",
                    },
                  ]}
                  style={{ marginRight: "8px", width: "calc(50% - 8px)" }}
                >
                  <Input prefix={<UserOutlined />} placeholder="Email" />
                </Form.Item>

                {/* <Form.Item style={{ marginBottom: 0 }}> */}
                <Form.Item
                  name="phone"
                  rules={[
                    {
                      required: true,
                      message: "Please input phone number of Vendor!",
                    },
                    () => ({
                      validator(rule, value) {
                        const regex =
                          /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
                        if (regex.test(value)) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          "Please enter 10 digit number value"
                        );
                      },
                    }),
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                  }}
                >
                  <Input
                    prefix={<PhoneOutlined />}
                    placeholder="Phone Number"
                  />
                </Form.Item>
              </div>
              <Form.Item
                name="address"
                rules={[
                  {
                    required: true,
                    message: "Please input address of Vendor!",
                  },
                ]}
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                }}
              >
                <Input prefix={<HomeOutlined />} placeholder="Address" />
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                  ghost
                >
                  Save
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Spin>
      </Card>
    </>
  );
};

export default VendorCreate;
