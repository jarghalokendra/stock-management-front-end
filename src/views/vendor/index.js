import React, { useState } from "react";
import { CCard, CCol, CRow } from "@coreui/react";
import GridTable from "../../components/grid/index";
import { Drawer } from "antd";
import Report from "./report";

const Vendor = () => {
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState({});

  const handleReport = (e, row) => {
    e.preventDefault();
    setVisible(true);
    setData(row);
  };

  const onClose = () => {
    setVisible(false);
  };

  const columns = [
    {
      title: "First Name",
      dataIndex: "first_name",
      sorter: true,
    },
    {
      title: "Last Name",
      dataIndex: "last_name",
      sorter: true,
    },
    {
      title: "Phone Number",
      dataIndex: "phone",
      sorter: true,
    },
    {
      title: "Address",
      dataIndex: "address",
      sorter: true,
    },
    {
      title: "Created At",
      dataIndex: "created_at",
      sorter: true,
    },
  ];

  const acolumns = {
    apiurl: "vendors",
    title: "Vendors",
    reduxKey: "vendors",
    content_type: "vendors",
    settings: {
      show: true,
      createTitle: "Create Party",
      createLink: "/vendors/create",
    },
    statusColumn: {
      column: "Status",
      show: false,
    },
    actions: {
      show: true,
      edit: {
        show: true,
        name: "Edit",
        type: "url",
      },
      delete: {
        show: true,
        name: "Delete",
        type: "popup",
      },
      report: {
        show: true,
        name: "Report",
        type: "popup",
        callFunction: "handleReport",
      },
    },
  };

  return (
    <>
      <CRow>
        <CCol xl={12} md={12}>
          <CCard>
            <GridTable
              columns={columns}
              acolumns={acolumns}
              handleReport={(e, row) => handleReport(e, row)}
            />
          </CCard>

          <Drawer
            title={`Report of ${data.first_name} ${data.last_name}`}
            width="920"
            closable={true}
            onClose={onClose}
            visible={visible}
          >
            <Report data={data} />
          </Drawer>
        </CCol>
      </CRow>
    </>
  );
};

export default Vendor;
