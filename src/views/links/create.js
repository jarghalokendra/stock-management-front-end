import React from 'react'
import { connect } from "react-redux"
import { CCard, CCol, CRow } from '@coreui/react'
import { Button, Space, Divider, Drawer, Form, Input, Alert, message, Row, Col, Spin } from 'antd';
import http from '../../config'
import { setInitGrid } from "../../redux/actions/index";

// import GridTable from '../grid/index'
class Links extends React.Component {
	formRef = React.createRef();
	state = {
		visible: false,
		loading: false,
		validationerror: 'closed',
		errors: [],
		preview:"",
		save:true,
		linkedited:false,
	}
	componentDidMount() {
		const { isEdit } = this.props;
		if (isEdit)
			this.handleEdit()
	}
	handleEdit =  () => {
		this.setState({ loading: true })
		const { edit } = this.props;
		setTimeout(async () => {
			this.formRef.current.setFieldsValue(edit)
			if(edit.preview)
				this.setState({preview: edit.preview, save:false})
			this.setState({ loading: false, save:false })
		}, 1000)

	}
	showDrawer = (e) => {
		e.preventDefault();
		this.setState({
			visible: true,
		});
	}

	onClose = () => {
		this.setState({
			validationerror: 'closed',
			errors: []
		});
		this.props.handleClose();
		this.formRef.current.resetFields()
	}
	onFinish = async (values) => {
		let formdata = {};
		formdata.name = this.formRef.current.getFieldValue('name');
		formdata.link_url = this.formRef.current.getFieldValue('link_url');
		formdata.image = this.state.videoPreview;
		try {
			this.setState({ loading: true })
			const { isEdit } = this.props;
			let url = isEdit ? `/links/update/${this.props.edit.id}` : '/links'
			await http.post(url, formdata)
			this.formRef.current.resetFields()
			this.props.handleClose();
			this.props.setInitGrid({ init: true })
		}
		catch (err) {
			let error;
			if (typeof err.response.data.message === 'string') {
				error = err.response.data.message;
			}
			let ers = []
			if (err.response.data.errors) {
				let errs = err.response.data.errors
				for (let p in errs) {
					let obj = {};
					obj.error = errs[p][0];
					ers.push(obj)
				}
				this.setState({
					errors: ers,
					validationerror: error
				})
			}
			message.error({ content: error, duration: 10 });
			this.setState({ loading: false })
		}
	}
	urlChange = (e) => {
		this.setState({save:true})
	}
	handleUrlChange = async(event) => {
		let value= event.target.value
		// event.persist();
		this.setState({ value: value,
			validationerror: 'closed',
			iframeerror:false,
			urlexists:false,
			previewloading:true,
			previewstatus:false,
			nopreview:false,
			
		});
		if (!/^https?:\/\//.test(value)) {
			value = "https://" + value;

			this.formRef.current.setFieldsValue({
				link_url: value,
			 })
		}

		var url = value;
		let param = {};
		param.url = url;
		try{
			this.setState({previewloading:true});
			const previewImg = await http.post('/links/preview',param);
			 this.setState({
				 videoPreview: previewImg.data.image,
				 previewloading:false,
				 previewstatus:true,
				 iframeerror:false,
				 urlexists:false,
				 save:false
			 });
			 this.formRef.current.setFieldsValue({
				name: previewImg.data.title,
			 })
		}
		catch(err){
			this.setState({previewloading:false});
			if(err.response && err.response.data.message === "Invalid web address")
			{
				this.setState({
					iframeerror:false,
					urlexists:true,
					previewloading:false,
					previewstatus:false,
					save:true
				});
			}
			else if(err.response && err.response.data.message === "Cannot be iframed")
			{
				this.setState({
					urlexists:false,
					iframeerror:true,
					previewloading:false,
					previewstatus:false,
					save:true,
				});
			}
			else if(err.response && err.response.data.message === "Enter Valid Url"){
				this.setState({
					urlexists:false,
					iframeerror:false,
					previewloading:false,
					previewstatus:false,
					save:true,
					nopreview:true,
				})
			}
		}
  }

	render() {
		return (
			<CRow>
				<CCol xl={12} md={12}>
					<CCard>
						<Drawer
							title="New Link"
							className="drawer-wrapper"
							closable={true}
							onClose={this.onClose}
							visible={this.props.visible}
							bodyStyle={{ paddingBottom: 80 }}
							footer={
								<div
									style={{
										textAlign: 'right',
									}}
								>
										<Button type="primary" style={{ marginRight: '10px' }} onClick={this.onFinish} disabled={this.state.save}>
											Save
										</Button>
									<Button onClick={this.onClose}>
										Cancel
                                </Button>
								</div>
							}
						>
							<Row>
								<Col span={14} offset={4}>
									{
										this.state.validationerror !== 'closed' &&
										<Alert message="Validation errors" description={this.state.validationerror} type="error" showIcon closable style={{ marginBottom: '10px' }} />
									}
									{
										this.state.errors.length > 0 &&
										this.state.errors.map((item, index) => {
											return (
												<Alert message={item.error} type="error" showIcon key={index} closable />
											)
										})
									}
								</Col>
							</Row>
							<Spin spinning={this.state.loading}>
								
								<Form
									ref={this.formRef}
									labelCol={{ span: 4 }}
									wrapperCol={{ span: 14 }}
									layout="horizontal"
									// onFinish={this.onFinish}
								>
									<Form.Item
										label="Link url"
										name="link_url"
										rules={[{ required: true, message: 'Please enter Link url!' }]}
									>
										<Input placeholder="https://" onChange = {()=>this.urlChange()} onBlur={(e)=>this.handleUrlChange(e)} value={this.state.previewLink} disabled={this.props.isEdit}/>
									</Form.Item>
									{this.props.isEdit || this.state.previewstatus ?
										<Form.Item
											label="Name"
											name="name"
											rules={[{ required: true, message: 'Please enter Link name!' }]}
										>
											<Input placeholder="Link Name" />
										</Form.Item>
									: null}
								</Form>
								<Divider> Preview</Divider>
								{this.state.urlexists ? (
									<Alert
										message="Is this an internal link?"
										description="We couldn’t access this URL"
										type="warning"
										showIcon
									/>) : null}
								{this.state.iframeerror ? (
									<Alert
										message="This website doesn’t support iframes"
										description="We can only display this website using Secure Sites. Please contact support to learn more about this feature."
										type="error"
										showIcon
									/>) : null}
								<Row style={{ textAlign: 'center', display: 'block' }}>
									{this.state.previewloading ? (
										<Space size="middle">
											<Spin size="large" />
										</Space>
									) : null}
								</Row>
								<Row style={{ textAlign: 'center', display: 'block' }}>
								{
                   this.props.isEdit &&
										<img src={this.state.preview} style={{ height: 250, width: 400, boxShadow: "5px 10px 8px #888888" }} alt={``}/>
								}
									{this.state.previewstatus ? (
										<img src={this.state.videoPreview} style={{ height: 250, width: 400, boxShadow: "5px 10px 8px #888888" }} />
									) : null}
									{this.state.nopreview ? (
										<div>No Preview Available</div>
									) : null}
								</Row>
							</Spin>
						</Drawer>
					</CCard>
				</CCol>
			</CRow>
		)
	}
}
const mapStateToProps = state => ({
	gridData: state.gridData
});
export default connect(mapStateToProps, { setInitGrid })(Links)