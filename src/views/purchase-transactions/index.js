import React, { useState } from "react";
import { CCard, CCol, CRow } from "@coreui/react";
import GridTable from "../../components/grid/index";
import ViewTransaction from "./view";
import Modal from "antd/lib/modal/Modal";

const PurchaseTransaction = () => {
  const [data, setData] = useState({});
  const [transactionVisible, setTransactionVisible] = useState(false);

  const handleViewTransaction = (e, row) => {
    e.preventDefault();
    setData(row);
    setTransactionVisible(true);
  };

  const handleTransactionModal = () => {
    setTransactionVisible(false);
  };

  const columns = [
    {
      title: "Paid Amount",
      dataIndex: "paid_amount",
      sorter: true,
      order: 1,
    },
    {
      title: "Created At",
      dataIndex: "created_at",
      sorter: true,
      order: 2,
    },
  ];
  const acolumns = {
    apiurl: "purhcase-transactions",
    content_type: "transactions",
    title: "Purchase Transactions",
    reduxKey: "purhcase-transactions",
    settings: {
      show: false,
      createTitle: "",
      createLink: "",
    },
    pivotRelation: {
      items: [
        {
          column: "Product Name",
          order: 0,
          show: true,
          nested: false,
          relationName: "product",
          relationColName: "name",
          dType: "obj",
        },
      ],
    },
    actions: {
      show: true,
      edit: {
        show: false,
        name: "Edit",
        type: "popup", //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit}
      },
      delete: {
        show: false,
        name: "Delete",
      },
      view: {
        show: true,
        name: "View Transaction",
        type: "popup",
        callFunction: "handleViewTransaction",
      },
    },
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <GridTable
            columns={columns}
            acolumns={acolumns}
            handleViewTransaction={(e, row) => handleViewTransaction(e, row)}
          />
        </CCard>

        <Modal
          title="View Transaction"
          visible={transactionVisible}
          onCancel={handleTransactionModal}
          footer={null}
        >
          <ViewTransaction data={data} />
        </Modal>
      </CCol>
    </CRow>
  );
};

export default PurchaseTransaction;
