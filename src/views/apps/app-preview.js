import React,{useState,useEffect} from 'react';
import { connect } from "react-redux"
import { Select, Input,Popover,Modal,Button,message,Alert,Spin } from 'antd';
import { useHistory } from 'react-router-dom'
import TwitterLogin from 'react-twitter-auth/lib/react-twitter-auth-component.js';

import styles from './apppreview.module.css'
import http from '../../config'
import {setInitGrid} from "../../redux/actions/index";


const { Option } = Select;

const AppPreview = (props) => {
	const setting = JSON.parse(props.item.setting);
	const fields = setting.fields;
	const history = useHistory();
	const [state, setState] = useState({
		app_name: null,
		setting:setting
	})
	const[loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([])
	let mfields = [];
	useEffect(()=>{
		if(props.isEdit){
			fields.forEach((item)=>{
				let type = item.input_type;
				let name = item.name;
				if(type === 'select'){
					handleSelect(setting.default[name].value, name, props.isEdit)
				}
				else if(type === 'signin'){
					// const newState = Object.assign({}, state);
					// newState.setting.credentials = setting.credentials;
					// if(Object.keys(setting.credentials).length > 0){
						handlePostMessage(state);
					//}
				}
			})
			setState({
				...state, app_name: props.item.name
			})
		}
	},[props.isEdit])

	const handleSelect = (e, name, isEdit=false) =>{
		const newState = Object.assign({}, state);
		newState.setting.default[name].value = e;
		setState(newState);
		handlePostMessage(newState, isEdit)
	}

	const handleAppName = (e) => {
		setState({
			...state, app_name: e.value
		})
	}

	const modalClose = () =>{
		props.handleClose();
	}

	const handleTwitterFeedSuccess = (success) =>{
    success.json().then(body => {
    	let obj = {
				oauth_token: body.data.oauth_token,
				oauth_token_secret: body.data.oauth_token_secret,
				user_id: body.data.user_id,
				screen_name: body.data.screen_name
    	};
    	const data = Object.assign({}, state);
    	data.setting.credentials = obj;
    	setState(data);
      handlePostMessage(data, false);
    });
  }

  const handlePostMessage = (data, isEdit=false) => {
  	setLoading(true);
  	setTimeout(()=>{
	  	let win = window.frames.date_app
	  	let target = process.env.NODE_ENV === 'development' ? process.env.REACT_APP_BACKEND_PATH : 'https://api.unifiednotice.com'
	  	win.postMessage(data, target)
	  	setLoading(false);
	  },2000);
  }

  const handleInput = (e) =>{
		const newState = Object.assign({}, state);
		newState.setting.default[e.name].value = e.value;
		handlePostMessage(newState);
		setState(newState);
  }

	const handleTwitterFeedFailed = (error) => {
    console.log(error);
  }

	const handleConfirm = async() => {
		try{
			setLoading(true)
			let url = props.isEdit ? `/apps/update/${props.item.id}` : '/apps/store'
			let app_id,id;
			if(props.isEdit){
				app_id = props.item.app_id;
				id = props.item.id;
			}
			else{
				app_id = props.item.id;
			}
			
			let params = Object.assign({},state);
			params.id = id;
			params.app_id = app_id
			let r = await http.post(url,params);
			setLoading(false)
			props.handleClose();
			if(props.isRedirect)
				history.push('/apps');
			if(props.isInit)
				props.setInitGrid({init: true})
		}
		catch(err){
			let error;
			if(typeof err.response.data.message === 'string'){
				error = err.response.data.message;
			}
			let ers = []
			if(err.response.data.errors){
				let errs = err.response.data.errors
				for(let p in errs){
					let obj = {};
					obj.error = errs[p][0];
					ers.push(obj)
				}
				setErrors(ers)
			}
			setLoading(false)
			message.error({ content: error, duration: 2});
		}
	}

	fields.forEach((item,prop)=>{
		let name = item.name;
		let label = item.label
		let type = item.input_type;
		if(type === 'select'){
			let opts = [];
			item.options.forEach((item,key)=>{
				opts.push(<Option key={key} value={item.value}>{item.name}</Option>)
			})
			mfields.push(<div className={styles['label-wrapper']} key={prop}>
				<label>{label}</label>
				<Select style={{width: '180px'}} onChange={(e)=>handleSelect(e,name)} defaultValue={state.setting.default[name].value} allowClear>
					{opts}
			</Select></div>)
		}
		else if(type === 'signin'){
			let requestTokenUrl = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_VERSION}/social/auth/reverse`
			let loginUrl = `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_VERSION}/social/auth`;
			mfields.push(<div className={styles['label-wrapper']} key={prop}>
				<TwitterLogin loginUrl={loginUrl}
          onFailure={handleTwitterFeedFailed}
          onSuccess={handleTwitterFeedSuccess}
          requestTokenUrl={requestTokenUrl}
          showIcon={true}
          forceLogin={true}
        />
			</div>)
		}
		else if(type === 'input'){
			mfields.push(<div className={styles['label-wrapper']} key={prop}>
				<label>{label}</label>
				<Input style={{width: '60%'}} onBlur={(e)=>handleInput({
					name: e.target.name,
					value: e.target.value
				})} name={name} defaultValue={state.setting.default[name].value}/>
			</div>)
		}
	})
	return(
		<>
			<Modal
        title={`Install ${props.item.name} App`}
        visible={props.visible}
        onCancel={()=>modalClose()}
        width={1050}
        style={{ top: 20 }}
        onOk={()=>handleConfirm()}
        footer={[
        	<Button key="back" onClick={()=>modalClose()}>
        	  Cancel
        	</Button>,
          <Button 
          	key="submit" 
          	type="primary" 
          	onClick={()=>handleConfirm()}
          	loading={loading}
          	disabled={!state.app_name}
          >
            Install App
          </Button>,
        ]}
      >
      	{
      		errors.length > 0 && 
      		 errors.map((item, index)=>{
      		 	return(
      		 		<Alert message={item.error} type="error" showIcon key={index} closable/>
      		 	)
      		 })
      	}
      	<Spin spinning={loading}>
					<div className={styles['app-name']}>
						<label>App Name</label>
						<Input placeholder="App Name" onChange={(e)=>handleAppName({
							name: e.target.name,
							value: e.target.value
						})} name="app_name" value={state.app_name}/>
					</div>

					<div className={styles['app-preview-wrapper']}>
						<div className={styles['app-setting']}>
							{mfields}
						</div>
						<div className={styles['app-preview']}>
							<iframe name="date_app" src={props.item.app_path} style={{border: 'none',height:' 500px',width: '100%'}}></iframe>
						</div>
					</div>
				</Spin>
			</Modal>
		</>
	)
}

const mapStateToProps = state => ({
});
export default connect(mapStateToProps,{setInitGrid})(AppPreview)