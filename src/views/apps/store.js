import React,{useState,useEffect} from 'react'
import { CCard,CCol,CRow } from '@coreui/react'
import { Card,Popover, Spin} from 'antd';
import { BsThreeDotsVertical } from "react-icons/bs";

import http from '../../config'
import AppPreview from './app-preview'
import styles from './store.module.css'

const { Meta } = Card;

const Store = (props) =>{
	const [apps, setApps] = useState([]);
	const [state, setState] = useState({
		visible: false,
		item: {},
		loading: false
	})
	useEffect(()=>{
		initStore();
	},[])
	const initStore = async () =>{
		try{
			setState({...state, loading: true})
			let r = await http.get('apps/stores');
			r = r.data.apps;
			setState({...state, loading: false})
			setApps(r);
		}
		catch(err){

		}
	}
	const handleInstall =(e,item) =>{
		e.preventDefault();
		setState({...state,visible: true, item: item})
	}

	return (
		<>
			<CRow>
			  <CCol xl={12} md={12}>
			    <CCard>
			    	<Spin spinning={state.loading}>
				    	<div className={styles['app-wrapper']}>
				    		{
				    			apps.map((item,index)=>{
				    				return(
							    		<Card
						    		    hoverable
						    		    key={index}
						    		    className={styles['card']}
						    		    extra={<a href="#">
				  								<Popover content={ <div>
		    											<a href="#" onClick={(e)=>handleInstall(e,item)}>Install App</a>
		  											</div>
		  											} 
		  											placement="bottom">
				  									<BsThreeDotsVertical/>
				  								</Popover>,
				  							</a>}
						    		    title={item.name}
						    		    cover={<img alt={``} src={item.app_thumb} />}
						    		  >
						    		    <Meta title={item.description} description={item.app_path} />
						    		  </Card>
				    				)
				    			})
				    		}
				    	</div>
				    </Spin>
			    </CCard>
			  </CCol>
			</CRow>
			{
				state.visible &&
					<AppPreview isInit={props.isInit ? true : false} isRedirect={props.noRedirect ? false : true} item={state.item} visible={state.visible} handleClose={()=>setState({...state, visible:false})}/>
			}
		</>
	)
}

export default Store;