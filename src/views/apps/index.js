import React from 'react'
import { CCard,CCol,CRow } from '@coreui/react'
import GridTable from '../../components/grid/index'
import AppPreview from './app-preview'

class Apps extends React.Component {
	formRef = React.createRef();
	state = {
		visible: false,
		loading: false,
		item: {}
	}

	handeEditAction =(e,row)=>{
		row.app_path = row.app.app_path;
		e.preventDefault()
		this.setState({
			item:row,
			visible:true
		})
	}
	render(){
		const columns = [
		{
		  title: 'Name',
		  dataIndex: 'name',
		  sorter: true,
		  editable: true
		},
		{
		  title: 'Duration',
		  dataIndex: 'duration',
		  sorter: true,
		  editable: true
		},
		{
		  title: 'created_at',
		  dataIndex: 'created_at',
		  sorter: true,
		}]
		const acolumns = {
		  apiurl: 'apps',
		  content_type: 'apps',
		  title: 'Apps',
		  reduxKey: 'apps',
		  settings: {
		  	show:true,
		    createTitle: 'Go to Store',
		    createLink: '/apps/store',
		  },
		  statusColumn: {
		    column: 'status',
		    show: true
		  },
		  fileColumn: {
		    column: 'file',
		    relationName: 'app',
		    imageName: 'app_thumb',
		    show: true
		  },
		  actions: {
		  	show: true,
		    edit: {
		      show: true,
		      name: 'Edit',
		      type: 'popup' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
		    },
		    delete:{
		      show: true,
		      name: 'Delete',
		    },
		    screen: {
		      show: true,
		      name: 'Set to Screen',
		      type: 'set-to-screen' //it should be only two types set-to-screen or set-content
		    }
		    
		  }
		}  
		return (
		  <CRow>
		    <CCol xl={12} md={12}>
		      <CCard>
		        <GridTable 
		        	columns={columns} 
		        	acolumns={acolumns}
		        	handleEdit={(e, row)=>this.handeEditAction(e, row)}
		        />
		      </CCard>
		    </CCol>
		    {
		    	this.state.visible &&
		    		<AppPreview isEdit={true} isInit={true} item={this.state.item} visible={this.state.visible} handleClose={()=>this.setState({...this.state, visible:false})}/>
		    }
		  </CRow>
		)
	}
}

export default Apps