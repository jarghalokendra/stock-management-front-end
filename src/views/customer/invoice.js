import React from "react";
import { Document, Page, Text, StyleSheet } from "@react-pdf/renderer";
import {
  Table,
  TableHeader,
  TableCell,
  TableBody,
  DataTableCell,
} from "@david.kucsai/react-pdf-table";

const Invoice = ({ data }) => (
  <Document>
    <Page size="A4" style={styles.body}>
      <Text style={styles.header} fixed>
        ~ {new Date().toLocaleString()} ~
      </Text>

      <Text style={styles.title}>
        {data.first_name} {data.last_name}'s transactions report
      </Text>
      <Text style={styles.container}>
        <Text style={styles.heading}>Email</Text>
        <Text style={styles.content}>
          {"                 "}: {"     "} {data.email}
        </Text>
      </Text>
      <Text style={styles.container}>
        <Text style={styles.heading}>Phone Number</Text>
        <Text style={styles.content}>
          {"   "}: {"     "} +977-{data.phone}
        </Text>
      </Text>
      <Text style={styles.container}>
        <Text style={styles.heading}>Address</Text>
        <Text style={styles.content}>
          {"              "}: {"     "} {data.address}
        </Text>
      </Text>

      <Text style={styles.sale}>Sold Products</Text>
      <Table>
        <TableHeader>
          <TableCell weighting={0.3} style={headerCellStyle}>
            Date
          </TableCell>
          <TableCell weighting={0.4} style={headerCellStyle}>
            Name
          </TableCell>
          <TableCell weighting={0.4} style={headerCellStyle}>
            Price
          </TableCell>
          <TableCell weighting={0.4} style={headerCellStyle}>
            Quantity
          </TableCell>
          <TableCell weighting={0.3} style={headerCellStyle}>
            VAT
          </TableCell>
          <TableCell weighting={0.6} style={headerCellStyle}>
            Payable Amount
          </TableCell>
          <TableCell weighting={0.5} style={headerCellStyle}>
            Paid Amount
          </TableCell>
          <TableCell weighting={0.9} style={headerCellStyle}>
            Remaining Amount
          </TableCell>
        </TableHeader>
      </Table>
      <Table data={data.sales}>
        <TableBody>
          <DataTableCell
            weighting={0.3}
            style={bodycellStyle}
            getContent={(x) =>
              new Intl.DateTimeFormat("en-GB", {
                month: "2-digit",
                day: "2-digit",
              }).format(new Date(x.created_at))
            }
          />
          <DataTableCell
            weighting={0.4}
            style={bodycellStyle}
            getContent={(x) => x.product.name}
          />
          <DataTableCell
            weighting={0.4}
            style={bodycellStyle}
            getContent={(x) => x.product.price.selling_price}
          />
          <DataTableCell
            weighting={0.4}
            style={bodycellStyle}
            getContent={(x) => x.quantity}
          />
          <DataTableCell
            weighting={0.3}
            style={bodycellStyle}
            getContent={(x) => (x.bill.value > 0 ? "Yes" : "No")}
          />
          <DataTableCell
            weighting={0.6}
            style={bodycellStyle}
            getContent={(x) => x.recieve_amt}
          />
          <DataTableCell
            weighting={0.5}
            style={bodycellStyle}
            getContent={(x) => x.paid_amount}
          />
          <DataTableCell
            weighting={0.9}
            style={bodycellStyle}
            getContent={(x) => x.rem_amt.toFixed(2)}
          />
        </TableBody>
      </Table>

      <Text style={{ lineHeight: 1.2, marginBottom: 8, marginTop: 22 }}>
        <Text style={styles.heading}>Total Recievable Amount</Text>
        <Text style={styles.content}>
          {"     "}: {"     "} Rs. {data.total_recieve}
        </Text>
      </Text>
      <Text style={styles.container}>
        <Text style={styles.heading}>Total Paid Amount</Text>
        <Text style={styles.content}>
          {"               "}: {"     "} Rs. {data.total_paid}
        </Text>
      </Text>
      <Text style={styles.container}>
        <Text style={styles.heading}>Total Remaining Amount</Text>
        <Text style={styles.content}>
          {"     "}: {"     "} Rs. {data.total_remain}
        </Text>
      </Text>
    </Page>
  </Document>
);

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 35,
  },

  header: {
    fontSize: 12,
    marginBottom: 20,
    textAlign: "center",
    color: "grey",
  },

  title: {
    fontSize: 16,
    marginBottom: 17,
    textTransform: "capitalize",
    fontFamily: "Times-Bold",
    borderBottom: "2px solid #cccaca",
  },

  container: {
    lineHeight: 1.2,
    marginBottom: 8,
  },

  heading: {
    fontSize: 14,
    fontFamily: "Times-Bold",
  },

  content: {
    fontSize: 14,
    textAlign: "left",
    textTransform: "capitalize",
    fontFamily: "Helvetica",
    paddingLeft: 10,
  },

  sale: {
    fontSize: 16,
    marginBottom: 8,
    marginTop: 10,
    fontFamily: "Times-Bold",
  },
});

const headerCellStyle = {
  fontWeight: "bold",
  textAlign: "center",
  padding: "5px 0",
  backgroundColor: "#c7c9c8",
};

const bodycellStyle = {
  textAlign: "center",
  padding: "5px 0",
};

export default Invoice;
