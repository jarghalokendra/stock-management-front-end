import React from "react";
import { Card, Descriptions } from "antd";
import { Table } from "react-bootstrap";
import { BlobProvider } from "@react-pdf/renderer";
import { CButton } from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Invoice from "./invoice";

const Report = (props) => {
  const {
    address,
    phone,
    email,
    sales,
    total_recieve,
    total_paid,
    total_remain,
  } = props.data;
  console.log(sales, total_recieve, total_paid);

  return (
    <>
      <div style={{ textAlign: "center", marginBottom: "20px" }}>
        <BlobProvider
          document={<Invoice data={props.data} />}
          // fileName={`${first_name}-${last_name}.pdf`}
        >
          {({ url }) => (
            <a href={url} target="_blank">
              <CButton
                style={{
                  color: "#FFFFFF",
                  backgroundColor: "#40A9FF",
                  borderRadius: 50,
                  paddingTop: 3,
                  paddingBottom: 3,
                }}
              >
                <CIcon name="cil-cloud-download" /> Download Report
              </CButton>
            </a>
          )}
        </BlobProvider>
      </div>
      <Descriptions title="" bordered size="middle">
        <Descriptions.Item label="Email" span={3}>
          {email}
        </Descriptions.Item>
        <Descriptions.Item label="Address" span={3}>
          {address}
        </Descriptions.Item>
        <Descriptions.Item label="Phone" span={3}>
          {phone}
        </Descriptions.Item>
      </Descriptions>
      <Card title="Sold Products">
        <Table responsive>
          <thead>
            <tr>
              <th>Date</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Bill Type</th>
              <th>Recievable Amount</th>
              <th>Paid Amount</th>
              <th>Remaining Amount</th>
            </tr>
          </thead>
          <tbody>
            {sales.map((sale, index) => {
              return (
                <tr key={index}>
                  <td>
                    {new Intl.DateTimeFormat("en-GB", {
                      month: "long",
                      day: "2-digit",
                    }).format(new Date(sale.created_at))}
                  </td>
                  <td>{sale.product.name}</td>
                  <td>{sale.quantity}</td>
                  <td>
                    {sale.sales_type === "retail"
                      ? sale.product.price.retail_sp
                      : sale.product.price.whole_sp}
                  </td>
                  <td>{sale.bill.name}</td>
                  <td>{sale.recieve_amt}</td>
                  <td>{sale.paid_amount}</td>
                  <td>{parseFloat(sale.rem_amt.toFixed(2))}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Card>
      <Descriptions title="" bordered size="middle">
        <Descriptions.Item label="Total Recievable Amount" span={3}>
          {total_recieve}
        </Descriptions.Item>
        <Descriptions.Item label="Total Paid Amount" span={3}>
          {total_paid}
        </Descriptions.Item>
        <Descriptions.Item label="Total Remaining Amount" span={3}>
          {total_remain}
        </Descriptions.Item>
      </Descriptions>
    </>
  );
};

export default Report;
