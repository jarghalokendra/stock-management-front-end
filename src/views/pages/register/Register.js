import React from "react";
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CRow,
} from "@coreui/react";
import styled from "styled-components";
import Register from "../../../components/auth/register";
import image from "../../../images/register.jpg";

const Login = () => {
  return (
    <Wrapper>
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="12">
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                    <Register />
                  </CCardBody>
                </CCard>
                {/* <CCard className="text-white bg-primary py-5">
                <CCardBody className="text-center">
                  <div >
                    <h2>New Here?</h2>
                    <p>
                      Sign Up and discover a modern way of managing all of your
                      products.
                    </p>
                  </div>
                </CCardBody>
              </CCard> */}
                <CCard>
                  <CCardBody className="backgroundImage">
                    <div className="text">
                      <h2>New Here?</h2>
                      <p>
                        Sign up and discover a modern way of managing all of
                        your products!
                      </p>
                    </div>
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  .backgroundImage {
    background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
      url(${image});
    background-size: cover;
    overflow: hidden;
  }

  .text {
    color: #fff;
    text-align: center;
    padding: 135px 25px 0 30px;
  }

  h2 {
    color: #fff;
  }

  p {
    font-size: 1.2rem;
    line-height: 1.5;
  }
`;

export default Login;
