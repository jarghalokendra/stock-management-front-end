import React from "react";
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CRow,
} from "@coreui/react";
import styled from "styled-components";
import LoginForm from "../../../components/auth/login";
import image from "../../../images/login.jpg";

const Login = () => {
  return (
    <Wrapper>
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="8">
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                    <LoginForm />
                  </CCardBody>
                </CCard>
                {/* <CCard className="text-white bg-primary py-5">
                <CCardBody className="text-center">
                  <div>
                    <h2>SauryaTech Inventory System</h2>
                    <p>Your goto place for all inventory related works.</p>
                  </div>
                </CCardBody>
              </CCard> */}
                <CCard>
                  <CCardBody className="backgroundImage">
                    <div className="text">
                      <h2>SauryaTech Inventory Management</h2>
                      <p>
                        Your goto place for all of the inventory related works.
                      </p>
                    </div>
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  .backgroundImage {
    background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
      url(${image});
    background-size: cover;
    overflow: hidden;
  }

  .text {
    color: #fff;
    text-align: center;
    padding-top: 100px;
  }

  h2 {
    color: #fff;
  }

  p {
    font-size: 1rem;
    line-height: 1.5;
    padding: 0 20px;
  }
`;

export default Login;
