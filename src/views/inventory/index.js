import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Row, Spin, Space, Empty, Button, AutoComplete } from "antd";
import { CCard, CCardBody, CCardHeader } from "@coreui/react";
import http from "../../config";
import Display from "./display";

const Inventory = () => {
  const [data, setData] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState([]);
  const [loading, setLoading] = useState(false);
  const [select, setSelect] = useState(false);

  useEffect(() => {
    initData();
  }, []);

  const initData = async () => {
    try {
      setLoading(true);
      let res = await http(`/inventory`);
      setData(res.data.data);
      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  };

  const onSelect = (value) => {
    let product = data.filter((item) => {
      return item.name === value;
    });
    setSelectedProduct(product);
    setSelect(true);
  };

  return (
    <CCard>
      <CCardHeader>
        <div style={{ display: "grid", justifyContent: "right" }}>
          <AutoComplete
            style={{ width: 150 }}
            onSelect={onSelect}
            filterOption="children"
            placeholder="Search Product..."
            notFoundContent="No Product Found"
          >
            {data.map((product, i) => (
              <AutoComplete.Option key={i} value={product.name}>
                {product.name}
              </AutoComplete.Option>
            ))}
          </AutoComplete>
        </div>
      </CCardHeader>
      {!select ? (
        <CCardBody>
          <Spin spinning={loading} size="large">
            <Row justify="center">
              <Space direction="vertical" size={40}>
                {data.length > 0 ? (
                  data.slice(0, 5).map((item, index) => {
                    return (
                      <div key={index}>
                        <Display item={item} />
                      </div>
                    );
                  })
                ) : (
                  <Empty
                    description={
                      <span>Oops! No product found for stock keeping.</span>
                    }
                  >
                    <Link to="/products">
                      <Button type="primary">Create Now</Button>
                    </Link>
                  </Empty>
                )}
              </Space>
            </Row>
          </Spin>
        </CCardBody>
      ) : (
        <CCardBody>
          {selectedProduct.map((item, index) => {
            return (
              <div key={index}>
                <Display item={item} />
              </div>
            );
          })}
        </CCardBody>
      )}
    </CCard>
  );
};

export default Inventory;
