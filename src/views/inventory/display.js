import React from "react";
import { Descriptions, Divider, Statistic } from "antd";
import { Table } from "react-bootstrap";
import NepaliDate from "../../components/classes/NepaliDate";
const nepali_date = new NepaliDate();

const Display = ({ item }) => {
  return (
    <>
      <Divider style={{ fontSize: "1.2rem" }}>
        {item.name.toUpperCase()}
      </Divider>

      <Descriptions style={{ marginLeft: "9rem" }}>
        <Descriptions.Item label="On">
          {/* {new Intl.DateTimeFormat("en-GB", {
            month: "long",
            day: "2-digit",
            year: "numeric",
          }).format(new Date(item.created_at))} */}
          {nepali_date.format(item.created_at)}
        </Descriptions.Item>
        <Descriptions.Item label="Purchased Quantity">
          <b>{item.quantity}</b>
        </Descriptions.Item>
        <Descriptions.Item label="Vendor">
          {item.vendor.first_name} {item.vendor.last_name}
        </Descriptions.Item>
      </Descriptions>

      {item.sales.length > 0 ? (
        <Table responsive>
          <thead>
            <tr>
              <th>Date of Sale</th>
              <th>Quantity</th>
              <th>Customer</th>
            </tr>
          </thead>
          {item.sales.map((sale, index) => {
            return (
              <tbody key={index}>
                <tr>
                  <td>{nepali_date.format(item.created_at)}</td>
                  <td>{sale.quantity}</td>
                  <td>
                    {sale.customer.first_name} {sale.customer.last_name}
                  </td>
                </tr>
              </tbody>
            );
          })}
        </Table>
      ) : (
        <div style={{ textAlign: "center" }}>
          <img
            src="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
            alt=""
            height="60px"
          />
          <br />
          <p style={{ color: "#00000040" }}>No Sales Made</p>
        </div>
      )}
      <Statistic
        title="Remaining Stock"
        value={item.quantity - item.total_sale}
        valueStyle={{ fontWeight: "bold" }}
        style={{ textAlign: "center" }}
      />
      <Divider />
    </>
  );
};

export default Display;
