import React from "react";
import { Descriptions } from "antd";
import { BlobProvider } from "@react-pdf/renderer";
import { CButton } from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Invoice from "./invoice";

const ViewTransaction = (props) => {
  const { sale, paid_amount } = props.data;
  const { quantity, customer, product } = sale;
  const { first_name, last_name, address, phone } = customer;
  return (
    <>
      <Descriptions title="" bordered style={{ textTransform: "capitalize" }}>
        <Descriptions.Item label="Product Name" span={3}>
          {product.name}
        </Descriptions.Item>
        <Descriptions.Item label="Quantity" span={3}>
          {quantity}
        </Descriptions.Item>
        <Descriptions.Item label="Paid Amount" span={3}>
          {paid_amount}
        </Descriptions.Item>
        <Descriptions.Item label="Customer" span={3}>
          {first_name} {last_name}
        </Descriptions.Item>
        <Descriptions.Item label="Address" span={3}>
          {address}
        </Descriptions.Item>
        <Descriptions.Item label="Phone" span={3}>
          {phone}
        </Descriptions.Item>
      </Descriptions>
      <div
        style={{
          display: "grid",
          justifyContent: "end",
          marginTop: 10,
        }}
      >
        <BlobProvider
          document={<Invoice order={props.data} />}
          // fileName={`${first_name}-${last_name}.pdf`}
        >
          {({ url }) => (
            <a href={url} target="_blank">
              <CButton
                style={{
                  color: "#FFFFFF",
                  backgroundColor: "#40A9FF",
                  borderRadius: 50,
                  paddingTop: 3,
                  paddingBottom: 3,
                }}
              >
                <CIcon name="cil-cloud-download" /> Download PDF
              </CButton>
            </a>
          )}
        </BlobProvider>
      </div>
    </>
  );
};

export default ViewTransaction;
