import React from "react";
import { Document, Page, Text, StyleSheet } from "@react-pdf/renderer";

const Invoice = ({ order }) => (
  <Document>
    <Page size="A4" style={styles.body}>
      <Text style={styles.header} fixed>
        ~ {new Date().toLocaleString()} ~
      </Text>
      <Text style={styles.title}>Transaction Details</Text>

      <Text style={styles.container}>
        <Text style={styles.heading}>Product Name</Text>
        <Text style={styles.content}>
          {"       "}: {"     "} {order.sale.product.name}
        </Text>
      </Text>

      <Text style={styles.container}>
        <Text style={styles.heading}> Quantity </Text>
        <Text style={styles.content}>
          {"               "}: {"    "} {order.sale.quantity}
        </Text>
      </Text>

      <Text style={styles.container}>
        <Text style={styles.heading}>Paid Amount </Text>
        <Text style={styles.content}>
          {"         "}: {"     "}
          Rs. {order.paid_amount}
        </Text>
      </Text>

      <Text style={styles.container}>
        <Text style={styles.heading}> Customer's Name </Text>
        <Text style={styles.content}>
          {"  "}: {"     "}
          {order.sale.customer.first_name} {order.sale.customer.last_name}
        </Text>
      </Text>

      <Text style={styles.container}>
        <Text style={styles.heading}> Address </Text>
        <Text style={styles.content}>
          {"                 "}: {"    "} {order.sale.customer.address}
        </Text>
      </Text>

      <Text style={styles.container}>
        <Text style={styles.heading}> Phone Number</Text>
        <Text style={styles.content}>
          {"       "}: {"     "}
          +977-{order.sale.customer.phone}{" "}
        </Text>
      </Text>
    </Page>
  </Document>
);

const styles = StyleSheet.create({
  body: {
    paddingTop: 35,
    paddingBottom: 65,
    paddingHorizontal: 50,
  },

  header: {
    fontSize: 12,
    marginBottom: 20,
    textAlign: "center",
    color: "grey",
  },

  title: {
    fontSize: 25,
    textAlign: "center",
    marginBottom: 17,
    textTransform: "capitalize",
    backgroundColor: "#EBEDEF",
    fontFamily: "Times-Bold",
  },

  container: {
    display: "flex",
    marginLeft: 135,
    lineHeight: 1.2,
    marginBottom: 10,
  },

  heading: {
    fontSize: 16,
    fontFamily: "Times-Bold",
  },

  content: {
    fontSize: 16,
    textAlign: "left",
    textTransform: "capitalize",
    fontFamily: "Helvetica",
    paddingLeft: 10,
  },
});

export default Invoice;
