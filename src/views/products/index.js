import React, { useState } from "react";
import { CCard, CCol, CRow } from "@coreui/react";
import Product from "./create";
import GridTable from "../../components/grid/index";
import Modal from "antd/lib/modal/Modal";
import Transaction from "./transaction";

const Products = () => {
  const [visible, setVisible] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [edit, setEdit] = useState({});
  const [transactionVisible, setTransactionVisible] = useState(false);
  const [transactionInfo, setTransactionInfo] = useState({});

  const handeEditAction = (e, row) => {
    e.preventDefault();
    setVisible(true);
    setIsEdit(true);
    setEdit(row);
    console.log(edit);
  };

  const handleCreate = (e) => {
    e.preventDefault(e);
    setVisible(true);
    setIsEdit(false);
  };

  const handleTransaction = (e, row) => {
    e.preventDefault();
    setTransactionInfo(row);
    setTransactionVisible(true);
  };

  const handleTransactionModal = () => {
    setTransactionVisible(false);
  };

  const columns = [
    {
      title: "SKU",
      dataIndex: "sku",
      order: 0,
      sorter: false,
    },
    {
      title: "Name",
      dataIndex: "name",
      order: 1,
      sorter: true,
      editable: true,
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      order: 3,
      sorter: false,
      editable: true,
    },
    {
      title: "Paid Amount",
      order: 4,
      dataIndex: "paid_amount",
      sorter: false,
    },
    {
      title: "Created At",
      dataIndex: "created_at",
      order: 7,
      sorter: true,
    },
  ];
  const acolumns = {
    apiurl: "products",
    content_type: "products",
    title: "Products",
    reduxKey: "products",
    settings: {
      show: true,
      createTitle: "Create Product",
      createLink: "",
    },
    pivotRelation: {
      items: [
        {
          column: "Price",
          show: true,
          order: 2,
          nested: false,
          relationName: "price",
          relationColName: "cost_price",
          dType: "obj",
        },
        {
          column: "Vendor",
          show: true,
          order: 6,
          nested: false,
          relationName: "vendor",
          relationColName: "first_name",
          dType: "obj",
        },
        {
          column: "Bill Type",
          order: 5,
          show: true,
          nested: false,
          relationName: "bill",
          relationColName: "name",
          dType: "obj",
        },
      ],
    },
    statusColumn: {
      column: "status",
      show: false,
    },
    fileColumn: {
      column: "file",
      imageName: "save_path",
      show: false,
    },
    actions: {
      show: true,
      edit: {
        show: true,
        name: "Edit",
        type: "popup", //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit},
        callFunction: "handleEdit",
      },
      transaction: {
        show: true,
        name: "Add Transaction",
        type: "popup",
        callFunction: "handleTransaction",
      },
      delete: {
        show: true,
        name: "Delete",
        type: "popup",
      },
    },
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <GridTable
            columns={columns}
            acolumns={acolumns}
            handleCreate={(e) => handleCreate(e)}
            handleEdit={(e, row) => handeEditAction(e, row)}
            handleTransaction={(e, row) => handleTransaction(e, row)}
          />
        </CCard>
        {visible && (
          <Product
            visible={visible}
            handleClose={() => setVisible(false)}
            isEdit={isEdit}
            edit={edit}
          />
        )}

        <Modal
          title="Add Transaction"
          width={800}
          centered
          visible={transactionVisible}
          onCancel={handleTransactionModal}
          footer={null}
          destroyOnClose={true}
        >
          <Transaction
            data={transactionInfo}
            handleFormClose={handleTransactionModal}
          />
        </Modal>
      </CCol>
    </CRow>
  );
};

export default Products;
