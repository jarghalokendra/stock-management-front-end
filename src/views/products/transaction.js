import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";
import {
  Button,
  Form,
  Alert,
  message,
  Row,
  Col,
  Spin,
  Input,
  Descriptions,
} from "antd";
import http from "../../config";
import { setInitGrid } from "../../redux/actions/index";

const Transaction = (props) => {
  const formRef = useRef(null);
  const { name, quantity, paid_amount, vendor, price, id, bill } = props.data;
  const { first_name, last_name, phone, address } = vendor;

  const [loading, setLoading] = useState(false);
  const [validationerror, setValidationerror] = useState("closed");
  const [errors, setErrors] = useState([]);
  const [remainAmount, setRemainAmount] = useState(0);
  const [submitDisable, setSubmitDisable] = useState(false);

  useEffect(() => {
    remainingAmount();
  }, []);

  const calcRemainingAmount = () => {
    const billRate = bill.value > 0 ? bill.value / 100 : 0;
    let amt = price.cost_price * quantity;
    let amtVat = amt + (billRate > 0 ? amt * billRate : 0);
    const remAmt = amtVat - paid_amount;
    return remAmt;
  };

  const remainingAmount = () => {
    const rAmt = calcRemainingAmount();
    setRemainAmount(rAmt);
  };

  const handlePaidAmount = (e) => {
    let remAmt, msg;
    let submitDisable = true;

    if (calcRemainingAmount() > 0) {
      if (e.value) {
        if (calcRemainingAmount() < e.value) {
          remAmt = calcRemainingAmount();
          submitDisable = true;
          msg = "Please enter amount less than remaining amount.";
          message.error({ content: msg, duration: 5 });
        } else {
          remAmt = calcRemainingAmount() - e.value;
          submitDisable = false;
        }
      } else {
        remAmt = calcRemainingAmount();
      }
    } else {
      remAmt = calcRemainingAmount();
      submitDisable = true;
      msg = "It looks like all of the amount is paid";
      message.success({ content: msg, duration: 5 });
    }

    setRemainAmount(remAmt);
    setSubmitDisable(submitDisable);
  };

  const onFinish = async (values) => {
    try {
      setLoading(true);
      values.product_id = id;
      await http.post(`purhcase-transactions`, values);
      setLoading(false);
      props.handleFormClose();
      props.setInitGrid({ init: true });
    } catch (err) {
      console.log(err);
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
        setValidationerror(error);
      }
      message.error({ content: error, duration: 10 });
      setLoading(false);
    }
  };

  return (
    <>
      <Row>
        <Col span={14} offset={4}>
          {validationerror !== "closed" && (
            <Alert
              message="Validation Errors"
              description={validationerror}
              type="error"
              showIcon
              closable
              style={{ marginBottom: "10px" }}
            />
          )}
          {errors.length > 0 &&
            errors.map((item, index) => {
              return (
                <Alert
                  message={item.error}
                  type="error"
                  showIcon
                  key={index}
                  closable
                />
              );
            })}
        </Col>
      </Row>
      <Spin spinning={loading}>
        <Descriptions title="Transaction" bordered>
          <Descriptions.Item label="Vendor" span={3}>
            {first_name} {last_name}
          </Descriptions.Item>
          <Descriptions.Item label="Phone" span={3}>
            {phone}
          </Descriptions.Item>
          <Descriptions.Item label="Address" span={3}>
            {address}
          </Descriptions.Item>
          <Descriptions.Item label="Product Name" span={3}>
            {name}
          </Descriptions.Item>
          <Descriptions.Item label="Paid Amount" span={3}>
            {paid_amount}
          </Descriptions.Item>
          <Descriptions.Item label="Remaining Amount" span={3}>
            {remainAmount.toFixed(2)}
          </Descriptions.Item>
        </Descriptions>

        <Form
          ref={formRef}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 10 }}
          layout="horizontal"
          onFinish={onFinish}
        >
          <Form.Item
            name="paid_amount"
            style={{ marginTop: "8px" }}
            label="Additional Amount Paid"
            rules={[
              {
                required: true,
                message: "Please enter additional amount!",
              },
              () => ({
                validator(rule, value) {
                  const regex = /^\d+(\.\d{1,2})?$/;
                  if (regex.test(value)) {
                    return Promise.resolve();
                  }
                  setRemainAmount(calcRemainingAmount());
                  return Promise.reject("Please enter only number!");
                },
              }),
            ]}
          >
            <Input
              placeholder="Paid"
              onChange={(e) =>
                handlePaidAmount({
                  value: e.target.value,
                })
              }
            />
          </Form.Item>

          <Form.Item label=" " colon={false}>
            <Button
              type="primary"
              htmlType="submit"
              ghost
              disabled={submitDisable}
            >
              Save
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    </>
  );
};

const mapStateToProps = (state) => ({
  gridData: state.gridData,
  initGrid: state.initGrid,
});

export default connect(mapStateToProps, { setInitGrid })(Transaction);
