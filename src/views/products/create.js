import React, { useState, useEffect, useRef } from "react";
import { connect } from "react-redux";
import { CCard, CCol, CRow } from "@coreui/react";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Button,
  Drawer,
  Form,
  Input,
  Alert,
  message,
  Row,
  Col,
  Spin,
  Select,
  Modal,
  Space,
} from "antd";
import http from "../../config";
import { setInitGrid } from "../../redux/actions/index";
import PriceForm from "../prices/form";
import VendorForm from "./vendor";

const Product = (props) => {
  const formRef = useRef(null);

  const [loading, setLoading] = useState(false);
  const [priceVisible, setPriceVisible] = useState(false);
  const [vendorVisible, setVendorVisible] = useState(false);
  const [validationError, setValidationError] = useState("closed");
  const [submitDisable, setSubmitDisable] = useState(false);
  const [errors, setErrors] = useState([]);
  const [users, setUsers] = useState([]);
  const [prices, setPrices] = useState([]);
  const [bills, setBills] = useState([]);
  const [categories, setCategories] = useState([]);
  const [remainingAmount, setRemainingAmount] = useState({});
  const [remainAmt, setRemainAmt] = useState([]);
  
  useEffect(() => {
    const { isEdit } = props;
    if (isEdit) handleEdit();
    initData();
    if (props.initGrid) {
      initData();
    }
  }, []);

  const loadVendors = async () => {
    let userResult = await http.get(`/commons/vendors`);
    setUsers(userResult.data.data);
  };

  const loadPrices = async () => {
    let priceResult = await http.get(`/commons/prices`);
    setPrices(priceResult.data.data);
  };

  const initData = async () => {
    try {
      setLoading(true);
      let priceResult = await http.get(`/commons/prices`);
      let userResult = await http.get(`/commons/vendors`);
      let billResult = await http.get(`/commons/bill`);
      let categoryResult = await http.get(`/commons/categories`);
      setUsers(userResult.data.data);
      setPrices(priceResult.data.data);
      setBills(billResult.data.data);
      setCategories(categoryResult.data.data);
      setLoading(false);
    } catch (err) {
      console.log(err);
    }
  };

  const handleEdit = () => {
    setLoading(true);
    const { edit } = props;
    const { categories, quantity, paid_amount, price, bill } = edit;
    let cats = [];
    categories.forEach((item) => {
      cats.push(item.id);
    });
    edit.category_id = cats;

    let rate = bill.value / 100;
    let noVatAmt = price.cost_price * quantity;
    let vatAmt = noVatAmt + (rate > 0 ? rate * noVatAmt : 0);
    let remainAmt = vatAmt - paid_amount;

    setTimeout(() => {
      formRef.current.setFieldsValue(edit);
      setRemainAmt(remainAmt);

      setLoading(false);
    }, 1000);
  };

  const onClose = () => {
    setValidationError("closed");
    setErrors([]);
    props.handleClose();
    formRef.current.resetFields();
  };

  const calc = (key) => {
    // console.log("in calc",key);
    let priceId = formRef.current.getFieldValue("products");
    // console.log("priceId",priceId[key]["price_id"]);
    if (!priceId[key]?.price_id) return;
    let price = prices.find((item) => {
      return item.id === priceId[key]["price_id"];
    });
    const cost = +price.cost_price;

    let billId = formRef.current.getFieldValue("products");
    if (!billId[key]["bill_type"]) return;
    let bill = bills.find((item) => {
      return item.id === billId[key]["bill_type"];
    });
    const billRate = bill.value > 0 ? bill.value / 100 : 0;

    return [cost, billRate];
  };

  const handleQuantity = ({ key, value }) => {
    if (!calc(key)) return;
    const [cost, billRate] = calc(key);

    let amtNoVat = cost * value;
    remainingAmount[key] = +amtNoVat + (billRate > 0 ? amtNoVat * billRate : 0);
    //remainingAmount[key]= parseFloat(remainingAmount[key].toFixed(2));
    remainingAmount[key] = remainingAmount[key].toFixed(2);
    //remainingAmount[key]=amtVat;

    setRemainingAmount({ ...remainingAmount, [key]: remainingAmount[key] });
    console.log(remainingAmount);
  };

  const totalRemainingAmt = () => {
    const totalRem = Object.values(remainingAmount);
    return totalRem.reduce((a, b) => +a + +b, 0).toFixed(2);
  };

  const handlePaid = ({ key, value }) => {
    if (!calc(key)) return;
    const [cost, billRate] = calc(key);   

    let msg, amtVat;
    let submitDisable = true;
    let paidAmt = value;

    const quantity = formRef.current.getFieldValue("products");

    const amtNoVat = cost * quantity[key]["quantity"];
    amtVat = +amtNoVat + (billRate > 0 ? amtNoVat * billRate : 0);
    if (value) {
      remainingAmount[key] = (+amtVat - paidAmt).toFixed(2);
      if (amtVat < paidAmt) {
        remainingAmount[key] = amtVat;
        submitDisable = true;
        msg = "Please enter amount less than total payable amount.";
        message.error({ content: msg, duration: 5 });
        const PaidAmount = formRef.current.getFieldValue("products");

        Object.assign(PaidAmount[key], { paid_amount: 0 });
        formRef.current.setFieldsValue({ PaidAmount });
      } else {
        submitDisable = false;
      }
    } else {
      remainingAmount[key] = amtVat;
    }
    setSubmitDisable(submitDisable);
    //setRemainingAmount(remAmt)
    setRemainingAmount({ ...remainingAmount, [key]: remainingAmount[key] });
  };

  const onFinish = async (values) => {
    try {
      setLoading(true);
      const { isEdit } = props;
      let url = isEdit ? `/products/${props.edit.id}` : "/products";
      if (!isEdit) {
        await http.post(url, values);
      } else {
        await http.put(url, values);
      }
      formRef.current.resetFields();
      props.handleClose();
      props.setInitGrid({ init: true });
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
        setValidationError(error);
      }
      message.error({ content: error, duration: 10 });
      setLoading(false);
    }
  };

  const handleProductPrice = (e, key) => {
    if (e === "create-new") {
      const ProductPrice = formRef.current.getFieldValue("products");
      Object.assign(ProductPrice[key], { price_id: "" });
      formRef.current.setFieldsValue({ ProductPrice });
      setPriceVisible(true);
    }
  };


          
  

  const handleVendor = (e) => {
    if (e === "create-new") {
      formRef.current.setFieldsValue({
        vendor_id: "",
      });
      setVendorVisible(true);
    }
  };

  const handlePriceForm = () => {
    setPriceVisible(false);
  };

  const handleVendorForm = () => {
    setVendorVisible(false);
  };

  
  

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <Drawer
            title={`${props.isEdit ? "Edit Product" : "Add New Product"}`}
            width={props.isEdit ? "720" : "65%"}
            closable={true}
            onClose={onClose}
            visible={props.visible}
          >
            <Row>
              <Col span={14} offset={4}>
                {validationError !== "closed" && (
                  <Alert
                    message="Validation errors"
                    description={validationError}
                    type="error"
                    showIcon
                    closable
                    style={{ marginBottom: "10px" }}
                  />
                )}
                {errors.length > 0 &&
                  errors.map((item, index) => {
                    return (
                      <Alert
                        message={item.error}
                        type="error"
                        showIcon
                        key={index}
                        closable
                      />
                    );
                  })}
              </Col>
            </Row>
            <Spin spinning={loading}>
              {props.isEdit && (
                <span style={{ position: "relative", left: 360, top: 40 }}>
                  <p>Remaining Amount : {remainAmt}</p>
                </span>
              )}

              {!props.isEdit && (
                <span style={{ position: "relative", left: 360, top: 40 }}>
                  <p>
                    {" "}
                    Total Remaining Amount :{" "}
                    {Object.keys(remainingAmount).length > 0
                      ? totalRemainingAmt()
                      : 0}
                  </p>
                </span>
              )}

              <Form
                ref={formRef}
                // labelCol={{ span: 4 }}
                // wrapperCol={{ span: 14 }}
                layout="horizontal"
                onFinish={onFinish}
               >
                <Form.Item
                  label="Party"
                  name="vendor_id"
                  rules={[
                    {
                      required: true,
                      message: "Please enter party name",
                    },
                  ]}
                >
                  <Select
                    showSearch
                    placeholder="Select Party Name"
                    optionFilterProp="children"
                    onChange={handleVendor}
                    style={{ width: 200 }}
                  >
                    <Select.Option key="00" value="create-new">
                      Create New Party
                    </Select.Option>
                    {users.map((user) => {
                      return (
                        <Select.Option key={user.id} value={user.id}>
                          {user.first_name} {user.last_name}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>

                {props.isEdit ? (
                  <>
                    <Form.Item
                      label="Name"
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Please enter product name!",
                        },
                      ]}
                      style={{ width: 400 }}
                    >
                      <Input placeholder="Product Name" />
                    </Form.Item>

                    <Form.Item
                      label="Price"
                      name="price_id"
                      rules={[
                        {
                          required: true,
                          message: "Please select price!",
                        },
                      ]}
                    >
                      <Select
                        showSearch
                        placeholder="Select Price"
                        optionFilterProp="children"
                        onChange={handleProductPrice}
                        style={{ width: 200 }}
                      >
                        <Select.Option key="00" value="create-new">
                          Create New Price
                        </Select.Option>
                        {prices.map((price) => {
                          return (
                            <Select.Option key={price.id} value={price.id}>
                              {`${price.product_name}-${price.cost_price}`}
                            </Select.Option>
                          );
                        })}
                      </Select>
                    </Form.Item>

                    <Form.Item label="Category" name="category_id">
                      <Select
                        mode="multiple"
                        showSearch
                        placeholder="Select Category"
                        optionFilterProp="children"
                        style={{ width: 400 }}
                      >
                        {categories.map((cat) => {
                          return (
                            <Select.Option key={cat.id} value={cat.id}>
                              {cat.name}
                            </Select.Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      label="Bill Type"
                      name="bill_type"
                      rules={[
                        {
                          required: true,
                          message: "Please select bill type!",
                        },
                      ]}
                      help={"Vat of 13% is applied."}
                      style={{ marginBottom: 20 }}
                    >
                      <Select
                        placeholder="Select Bill Type"
                        style={{ width: 200 }}
                      >
                        {bills.map((bill) => {
                          return (
                            <Select.Option key={bill.id} value={bill.id}>
                              {bill.name}
                            </Select.Option>
                          );
                        })}
                      </Select>
                    </Form.Item>

                    <Form.Item
                      label="Quantity"
                      name="quantity"
                      style={{ display: "none" }}
                      rules={[
                        {
                          required: true,
                          message: "Please enter product quantity!",
                        },
                        () => ({
                          validator(rule, value) {
                            const regex = /^\d+$/;
                            if (regex.test(value)) {
                              return Promise.resolve();
                            }
                            return Promise.reject("Please enter only number!");
                          },
                        }),
                      ]}
                    >
                      <Input
                        placeholder="Quantity"
                        // onChange={}
                      />
                    </Form.Item>
                  </>
                ) : (
                  <>
                    <Form.List name="products">
                      {(fields, { add, remove }) => (
                        <>
                          {fields.map(
                            ({ key, name, fieldKey, ...restField }) => (
                              <div key={key}>
                                <hr />

                                <div style={{ display: "flex" }}>
                                  <Form.Item
                                    {...restField}
                                    label="Name"
                                    name={[name, "name"]}
                                    fieldKey={[fieldKey, "name"]}
                                    rules={[
                                      {
                                        required: true,
                                        message: "Please enter product name!",
                                      },
                                    ]}
                                    style={{
                                      width: "calc(50% - 8px)",
                                    }}
                                  >
                                    <Input placeholder="Product Name" />
                                  </Form.Item>

                                  <Form.Item
                                    {...restField}
                                    label="Price"
                                    name={[name, "price_id"]}
                                    fieldKey={[fieldKey, "price_id"]}
                                    rules={[
                                      {
                                        required: true,
                                        message: "Please select price!",
                                      },
                                    ]}
                                    style={{
                                      width: "calc(50% - 8px)",
                                      margin: "0 8px",
                                    }}
                                  >
                                    <Select
                                      showSearch
                                      placeholder="Select Price"
                                      optionFilterProp="children"
                                      onChange={(e) =>
                                        handleProductPrice(e, key)
                                      }
                                    >
                                      <Select.Option
                                        key="00"
                                        value="create-new"
                                      >
                                        Create New Price
                                      </Select.Option>
                                      {prices.map((price) => {
                                        return (
                                          <Select.Option
                                            key={price.id}
                                            value={price.id}
                                          >
                                            {`${price.product_name}-${price.cost_price}`}
                                          </Select.Option>
                                        );
                                      })}
                                    </Select>
                                  </Form.Item>
                                </div>

                                <div style={{ display: "flex" }}>
                                  <Form.Item
                                    label="Category"
                                    name={[name, "category_id"]}
                                    fieldKey={[fieldKey, "category_id"]}
                                    style={{ width: "calc(50% - 8px)" }}
                                  >
                                    <Select
                                      mode="multiple"
                                      showSearch
                                      placeholder="Select Category"
                                      optionFilterProp="children"
                                     >
                                  {categories.map((cat) => {
                                        return (
                                          <Select.Option
                                            key={cat.id}
                                            value={cat.id}
                                          >
                                            {cat.name}
                                          </Select.Option>
                                        );
                                      })}
                                    </Select>
                                  </Form.Item>
                                  <Form.Item
                                    label="Bill Type"
                                    name={[name, "bill_type"]}
                                    fieldKey={[fieldKey, "bill_type"]}
                                    rules={[
                                      {
                                        required: true,
                                        message: "Please select bill type!",
                                      },
                                    ]}
                                    help={"Vat of 13% is applied."}
                                    style={{
                                      width: "calc(50% - 8px)",
                                      margin: "0 8px",
                                    }}
                                  >
                                    <Select placeholder="Select Bill Type">
                                      {bills.map((bill) => {
                                        return (
                                          <Select.Option
                                            key={bill.id}
                                            value={bill.id}
                                          >
                                            {bill.name}
                                          </Select.Option>
                                        );
                                      })}
                                    </Select>
                                  </Form.Item>
                                </div>

                              
                                <div style={{ display: "flex" }}>
                                  <Form.Item
                                    label="Quantity"
                                    name={[name, "quantity"]}
                                    fieldKey={[fieldKey, "quantity"]}
                                    style={{ width: "calc(50% - 8px)" }}
                                    rules={[
                                      {
                                        required: true,
                                        message:
                                          "Please enter product quantity!",
                                      },
                                      () => ({
                                        validator(rule, value) {
                                          const regex = /^\d+$/;
                                          if (regex.test(value)) {
                                            return Promise.resolve();
                                          }
                                          return Promise.reject(
                                            "Please enter only number!"
                                          );
                                        },
                                      }),
                                    ]}
                                  >
                                    <Input
                                      placeholder="Quantity"
                                      onChange={(e) =>
                                        handleQuantity({
                                          value: e.target.value,
                                          key,
                                        })
                                      }
                                    />
                                  </Form.Item>

                                  <Form.Item
                                    label="Excerpt"
                                    name="excerpt"
                                    name={[name, "excerpt"]}
                                    fieldKey={[fieldKey, "excerpt"]}
                                    style={{
                                      width: "calc(50% - 8px)",
                                      margin: "0 8px",
                                    }}
                                  >
                                    <Input.TextArea />
                                  </Form.Item>
                                </div>
                                <div style={{ display: "flex" }}>
                                  <Form.Item
                                    label="Paid Amount"
                                    style={{
                                      width: "calc(50% - 8px)",
                                    }}
                                  >
                                    <Form.Item
                                      name="paid_amount"
                                      name={[name, "paid_amount"]}
                                      fieldKey={[fieldKey, "paid_amount"]}
                                      rules={[
                                        {
                                          required: true,
                                          message: "Please enter paid amount!",
                                        },
                                        () => ({
                                          validator(rule, value) {
                                            const regex = /^\d+(\.\d{1,2})?$/;
                                            if (regex.test(value)) {
                                              return Promise.resolve();
                                            }
                                            return Promise.reject(
                                              "Please enter only number!"
                                            );
                                          },
                                        }),
                                      ]}
                                      noStyle
                                    >
                                      <Input
                                        placeholder="Paid"
                                        onChange={(e) =>
                                          handlePaid({
                                            key,
                                            value: e.target.value,
                                          })
                                        }
                                      />
                                    </Form.Item>
                                    <span>
                                      Remaining Amount :{" "}
                                      {remainingAmount[key]
                                        ? remainingAmount[key]
                                        : 0}
                                    </span>
                                  </Form.Item>
                                </div>
                              

                                <div
                                  style={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    marginBottom: "1rem",
                                  }}
                                >
                                  <Button
                                    type="button"
                                    onClick={() => remove(name)}
                                    icon={<MinusCircleOutlined />}
                                  >
                                    Remove Products
                                  </Button>
                                </div>
                              </div>
                            )
                          )}
                          <Form.Item>
                            <Button
                              type="dashed"
                              onClick={() => add()}
                              block
                              icon={<PlusOutlined />}
                            >
                              Add Product
                            </Button>
                          </Form.Item>
                        </>
                      )}
                    </Form.List>
                  </>
                )}
                <Form.Item label=" " colon={false}>
                  <Button
                    type="primary"
                    htmlType="submit"
                    ghost
                    disabled={submitDisable}
                  >
                    Save
                  </Button>
                </Form.Item>
              </Form>
            </Spin>
          </Drawer>

          <Modal
            title="Create New Price"
            width={800}
            centered
            onCancel={handlePriceForm}
            visible={priceVisible}
            footer={null}
            afterClose={loadPrices}
          >
            <PriceForm handleFormClose={handlePriceForm} />
          </Modal>

          <Modal
            title="Create New Party"
            width={650}
            centered
            onCancel={handleVendorForm}
            visible={vendorVisible}
            footer={null}
            afterClose={loadVendors}
          >
            <VendorForm handleFormClose={handleVendorForm} />
          </Modal>
        </CCard>
      </CCol>
    </CRow>
  );
};

const mapStateToProps = (state) => ({
  gridData: state.gridData,
  initGrid: state.initGrid,
});

export default connect(mapStateToProps, { setInitGrid })(Product);
