import React, { useState, useRef } from "react";
import { connect } from "react-redux";
import { Card, Form, Input, Button, Alert, Spin, message } from "antd";
import {
  UserOutlined,
  UserAddOutlined,
  PhoneOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import http from "../../config";
import { setInitGrid } from "../../redux/actions/index";

const VendorForm = (props) => {
  const formRef = useRef(null);

  const [loading, setLoading] = useState(false);
  const [validationerror, setValidationerror] = useState("closed");
  const [errors, setErrors] = useState([]);

  const onFinish = async (values) => {
    try {
      setLoading(true);
      await http.post(`/vendors`, values);
      props.handleFormClose();
      props.setInitGrid({ init: true });
      formRef.current.resetFields();
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
        setValidationerror(error);
      }
      message.error({ content: error, duration: 5 });
      setLoading(false);
    }
  };

  return (
    <>
      {validationerror !== "closed" && (
        <Alert
          message="Validation Errors"
          description={validationerror}
          type="error"
          showIcon
          closable
          style={{ marginBottom: "10px" }}
        />
      )}
      {errors.length > 0 &&
        errors.map((item, index) => {
          return (
            <Alert
              key={index}
              message={item.error}
              type="error"
              showIcon
              closable
            />
          );
        })}
      <Spin spinning={loading}>
        <Card style={{ width: 600, marginLeft: "auto", marginRight: "auto" }}>
          <Form
            ref={formRef}
            name="normal_login"
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <div style={{ display: "flex" }}>
              <Form.Item
                name="first_name"
                rules={[
                  {
                    required: true,
                    message: "Please input First Name of Vendor!",
                  },
                ]}
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                }}
              >
                <Input
                  prefix={<UserAddOutlined className="site-form-item-icon" />}
                  placeholder="First Name"
                />
              </Form.Item>
              <Form.Item
                name="last_name"
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                  margin: "0 8px",
                }}
              >
                <Input
                  prefix={<UserAddOutlined className="site-form-item-icon" />}
                  placeholder="Last Name"
                />
              </Form.Item>
            </div>
            <div style={{ display: "flex" }}>
              <Form.Item
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                ]}
                style={{ marginRight: "8px", width: "calc(50% - 8px)" }}
              >
                <Input prefix={<UserOutlined />} placeholder="Email" />
              </Form.Item>

              {/* <Form.Item style={{ marginBottom: 0 }}> */}
              <Form.Item
                name="phone"
                rules={[
                  {
                    required: true,
                    message: "Please input phone number of Vendor!",
                  },
                ]}
                style={{
                  display: "inline-block",
                  width: "calc(50% - 8px)",
                }}
              >
                <Input prefix={<PhoneOutlined />} placeholder="Phone Number" />
              </Form.Item>
            </div>
            <Form.Item
              name="address"
              rules={[
                {
                  required: true,
                  message: "Please input address of Vendor!",
                },
              ]}
              style={{
                display: "inline-block",
                width: "calc(50% - 8px)",
              }}
            >
              <Input prefix={<HomeOutlined />} placeholder="Address" />
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                ghost
              >
                Save
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </Spin>
    </>
  );
};

const mapStateToProps = (state) => ({
  gridData: state.gridData,
});

export default connect(mapStateToProps, { setInitGrid })(VendorForm);
