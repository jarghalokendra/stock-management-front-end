import React from 'react'
import { CCard,CCol,CRow } from '@coreui/react'

import Screen from './create'
import GridTable from '../../components/grid/index'

// import GridTable from '../grid/index'
class ScreenList extends React.Component {
  formRef = React.createRef();
  state = {
    visible: false,
    loading: false,
    isEdit: false,
    edit: {}
  }
  handeEditAction(e,row){
    e.preventDefault()
    this.setState({
      visible:true,
      isEdit: true,
      edit: row
    })
  }
  handleCreate(){
    this.setState({
      visible:true,
      isEdit: false
    })
  }
  render(){
    const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: true,
      editable: true
    },
    {
      title: 'created_at',
      dataIndex: 'created_at',
      sorter: true,
    }]
    const acolumns = {
      apiurl: 'screens',
      title: 'Screens',
      reduxKey: 'screens',
      settings: {
        show:true,
        createTitle: 'Add Screen',
        createLink: '',
      },
      pivotRelation: {
        items: [{
          column: 'Active Screen',
          show: true,
          relationName: 'content',
          relationColName: 'original_name',
          imageExist: true,
          imageShow: true,
          dType: 'obj'//we can pass dType like array or object
        }]
      },
      statusColumn: {
        column: 'status',
        show: true
      },
      actions: {
        show: true,
        edit: {
          show: true,
          name: 'Edit',
          type: 'popup' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
        },
        delete:{
          show: true,
          name: 'Delete',
        },
        screen: {
          show: true,
          name: 'Set Content',
          type: 'set-content' //it should be only two types set-to-screen or set-content
        }
        
      }
    }  
    return (
      <CRow>
        <CCol xl={12} md={12}>
          <CCard>
            <GridTable 
              columns={columns} 
              acolumns={acolumns} 
              handleCreate={()=>this.handleCreate()}
              handleEdit={(e, row)=>this.handeEditAction(e, row)}
            />
          </CCard>
          {
            this.state.visible &&
              <Screen 
                visible={this.state.visible} 
                handleClose={()=>this.setState({visible: false})}
                isEdit={this.state.isEdit}
                edit={this.state.edit}
              />
          }
          
        </CCol>
      </CRow>
    )
  }
}

export default ScreenList