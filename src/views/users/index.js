import React from "react";
import { CCard, CCol, CRow } from "@coreui/react";
import GridTable from "../../components/grid/index";

class Users extends React.Component {
  state = {
    visible: false,
    loading: false,
    isEdit: false,
    edit: {},
  };
  handeEditAction(e, row) {
    e.preventDefault();
    this.setState({
      visible: true,
      isEdit: true,
      edit: row,
    });
  }
  handleCreate() {
    this.setState({
      visible: true,
      isEdit: false,
    });
  }
  render() {
    const columns = [
      {
        title: "First Name",
        dataIndex: "first_name",
        sorter: true,
        editable:true,
      },
      {
        title: "Last Name",
        dataIndex: "last_name",
        sorter: true,
        editable:true,
      },
      {
        title: "Phone Number",
        dataIndex: "phone",
        sorter: true,
        editable:true,
      },
      {
        title: "Address",
        dataIndex: "address",
        sorter: true,
        editable:true,
      },
      {
        title: "Created At",
        dataIndex: "created_at",
        sorter: true,
      },
    ];
    const acolumns = {
      apiurl: "users",
      title: "Users",
      reduxKey: "users",
      content_type: "users",
      settings: {
        show: true,
        createTitle: "Create User",
        createLink: "/users/create",
      },
      statusColumn: {
        column: "Status",
        show: false,
      },
      actions: {
        show: true,
        edit: {
          show: true,
          name: "Edit",
          type: "url", //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit}
        },
        delete: {
          show: true,
          name: "Delete",
          type: "popup",
        },
      },
    };
    return (
      <CRow>
        <CCol xl={12} md={12}>
          <CCard>
            <GridTable
              columns={columns}
              acolumns={acolumns}
              handleCreate={() => this.handleCreate()}
              handleEdit={(e, row) => this.handeEditAction(e, row)}
            />
          </CCard>
        </CCol>
      </CRow>
    );
  }
}

export default Users;
