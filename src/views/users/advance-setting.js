import React from 'react'
import { Card } from 'antd';
import People from "../../components/account/people"
import Roles from "./roles"
class AdvanceSetting extends React.Component {
    state = {
        noTitleKey: 'people',
      };
        onTabChange = (key, type) => {
          console.log(key, type);
          this.setState({ [type]: key });
        };
    render() {          
          const tabListNoTitle = [
            {
              key: 'people',
              tab: 'People',
            },
            {
              key: 'roles',
              tab: 'Roles',
            },
          ];
          
          const contentListNoTitle = {
            people: <People/>,
            roles: <Roles />
          };
          

        return (
            <>
                 <Card
                    style={{ width: '100%' }}
                    tabList={tabListNoTitle}
                    activeTabKey={this.state.noTitleKey}
                    onTabChange={key => {
                        this.onTabChange(key, 'noTitleKey');
                    }}
                    >
                    {contentListNoTitle[this.state.noTitleKey]}
                    </Card>
            </>
        )
    }
}

export default AdvanceSetting