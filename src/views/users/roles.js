import React, { useEffect, useState } from "react";
import { Spin, Card, Form, Tag, Select, Button } from 'antd';
import { CCard, CCol, CRow } from '@coreui/react'
import http from '../../config'
import styles from './roles.module.css'
const { Option } = Select;
const Roles = () => {
  const [state, setState] = useState({ roledata: [] });
  const [loading, setLoading] = useState(false);
  const [submit, setSubmit] = useState(false);
  const [form] = Form.useForm();


  const getRoles = async () => {
    setLoading(true);
    try {
      let roledata = await http.get('rolemodules');
      let roles = roledata.data.roledata;
      console.log(roles);
      roles.forEach((item, index) => {
          // let role = [];
          // let modules = item.selectedmodules;
          // if(modules)
          // {
          //   for(let prop in modules)
          //   {
          //     role.push(module[prop]);
          //   }
          // }
          form.setFieldsValue({ [item.roleid]: item.selectedmodules })
        console.log(typeof item.selectedmodules)
      })
      setState({ roledata: roles });
      // setState({ selectedrole: roledata.data.roledata.rolemod });
      setLoading(false);
      setSubmit(true);
      // setData({...data,role:value});

    }
    catch (err) {
      console.log(err);
      setLoading(false);
    }
  }
  useEffect(() => {
    getRoles();
  }, []);

  const onFinish = async (values) => {
    try {
      let data = [];
      for (let prop in values) {
        let obj = {};
        obj.roleid = prop;
        obj.modules = values[prop];
        data.push(obj);
      }
      setLoading(true);
      let result = await http.post('/rolemodules/store', data);
      setLoading(false)

    }
    catch (err) {
      console.log(err);
    }
  };
  return (
    <>
      <Card title="Roles" style={{ marginLeft: "auto", marginRight: "auto" }}>
        <p>Set modules visibility for user roles.
          </p>
        <Spin spinning={loading}>
          <CRow>
            <CCol xl={12} md={12}>
              <CCard>
                <Form
                  form={form}
                  className={styles['roleform']}
                  name="validate_other"
                  onFinish={onFinish}
                >

                  {state.roledata.map((d, i) => {
                    return (
                      <div key={i}>
                      
                        <label
                        >{d.rolename }</label>
                        
                        <Form.Item
                        
                          name={d.roleid}
                          rules={[{ type: 'array' }]}
                          className={styles['form-item']}
                          

                        >
                          <Select
                            className={styles['closeicon']}
                            mode="multiple"
                            allowClear
                            style={{ width: '100%' }}

                            showSearch

                              optionFilterProp="children"
                              filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                              }

                          >
                            {d.modules.map((option, index) => {
                              return (
                                <Option value={option.value} key={index}>{option.label}</Option>
                              )
                            })}
                          </Select>
                        </Form.Item>
                      </div>
                    );
                  })}
                  {submit? 
                  <Button type="primary" htmlType="submit" style={{ float: 'right' }}>
                    Submit
                </Button>
                :''
              }
                </Form>
              </CCard>
            </CCol>
          </CRow>
        </Spin>
      </Card>

    </>
  );
}
export default Roles
