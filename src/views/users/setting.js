import React from 'react'
import { Card } from 'antd';
import GeneralSetting from './general-setting'
import AdvanceSetting from './advance-setting'

class Setting extends React.Component {
    state = {
        noTitleKey: 'general',
      };
        onTabChange = (key, type) => {
          console.log(key, type);
          this.setState({ [type]: key });
        };
    render() {          
          const tabListNoTitle = [
            {
              key: 'general',
              tab: 'General Settings',
            },
            {
              key: 'advanced',
              tab: 'Advance Settings',
            },
          ];
          
          const contentListNoTitle = {
            general: <GeneralSetting/>,
            advanced: <AdvanceSetting/>,
          };
          

        return (
            <>
                 <Card
                    style={{ width: '100%' }}
                    tabList={tabListNoTitle}
                    activeTabKey={this.state.noTitleKey}
                    onTabChange={key => {
                        this.onTabChange(key, 'noTitleKey');
                    }}
                    >
                    {contentListNoTitle[this.state.noTitleKey]}
                    </Card>
            </>
        )
    }
}

export default Setting