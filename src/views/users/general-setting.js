import React from 'react'
import { Card } from 'antd';
import MyAccount from "../../components/account/myaccount"
import Credentials from "../../components/account/credentials"

class GeneralSetting extends React.Component {
    state = {
        noTitleKey: 'account',
      };
        onTabChange = (key, type) => {
          console.log(key, type);
          this.setState({ [type]: key });
        };
    render() {          
          const tabListNoTitle = [
            {
              key: 'account',
              tab: 'My Account',
            },
            {
              key: 'credentials',
              tab: 'Credentials',
            },
          ];
          
          const contentListNoTitle = {
            account: <MyAccount/>,
            credentials: <Credentials/>,
          };
          

        return (
            <>
                 <Card
                    style={{ width: '100%' }}
                    tabList={tabListNoTitle}
                    activeTabKey={this.state.noTitleKey}
                    onTabChange={key => {
                        this.onTabChange(key, 'noTitleKey');
                    }}
                    >
                    {contentListNoTitle[this.state.noTitleKey]}
                    </Card>
            </>
        )
    }
}

export default GeneralSetting