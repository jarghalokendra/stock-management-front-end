import React, { useEffect, useState } from "react";
import {
  CWidgetDropdown,
  CRow,
  CCol,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CDropdownToggle,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ChartLineSimple from "../charts/ChartLineSimple";
import ChartBarSimple from "../charts/ChartBarSimple";

import http from "../../config";
import SubMenu from "antd/lib/menu/SubMenu";

const WidgetsDropdown = (props) => {
  const { productscountbymonth, userscountbymonth, salescountbymonth } = props;
  const [productsCount, setProductsCount] = useState(0);
  const [salesCount, setSalesCount] = useState(0);
  const [usersCount, setUsersCount] = useState(0);

  useEffect(() => {
    loadProductsCount();
    loadSalesCount();
    loadUsersCount();
  }, []);

  const loadUsersCount = async () => {
    try {
      let result = await http.get(`/commons/users`);
      setUsersCount(result.data.usercount);
    } catch (err) {
      console.log(err);
    }
  };

  const loadProductsCount = async () => {
    try {
      let result = await http.get(`/commons/products`);
      setProductsCount(result.data.productcount);
    } catch (err) {
      console.log(err);
    }
  };

  const loadSalesCount = async () => {
    try {
      let result = await http.get(`/commons/sales`);
      setSalesCount(result.data.data);
    } catch (err) {
      console.log(err);
    }
  };

  // render
  return (
    <CRow>
      <CCol sm="6" lg="4">
        <CWidgetDropdown
          color="gradient-primary"
          header={productsCount.toString()}
          text="Total Number of Products"
          footerSlot={
            <ChartLineSimple
              pointed
              className="c-chart-wrapper mt-3 mx-3"
              style={{ height: "70px" }}
              dataPoints={productscountbymonth}
              pointHoverBackgroundColor="primary"
              label="Products"
              labels="months"
            />
          }
        ></CWidgetDropdown>
      </CCol>

      <CCol sm="6" lg="4">
        <CWidgetDropdown
          color="gradient-info"
          header={salesCount.toString()}
          text="Total Number of Sales"
          footerSlot={
            <ChartLineSimple
              pointed
              className="mt-3 mx-3"
              style={{ height: "70px" }}
              dataPoints={salescountbymonth}
              pointHoverBackgroundColor="info"
              options={{ elements: { line: { tension: 0.00001 } } }}
              label="Sales"
              labels="months"
            />
          }
        ></CWidgetDropdown>
      </CCol>

      <CCol sm="6" lg="4">
        <CWidgetDropdown
          color="gradient-success"
          header={usersCount.toString()}
          text="Total Number of Customers "
          footerSlot={
            <ChartLineSimple
              pointed
              className="c-chart-wrapper mt-3 mx-3"
              style={{ height: "70px" }}
              dataPoints={userscountbymonth[0]}
              pointHoverBackgroundColor="success"
              label="Users"
              labels="months"
            />
          }
        ></CWidgetDropdown>
      </CCol>
    </CRow>
  );
};

export default WidgetsDropdown;
