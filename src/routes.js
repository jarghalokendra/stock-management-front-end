import React from "react";

// dahsboard
const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));

// inventory
const Inventory = React.lazy(() => import("./views/inventory/index"));

//auth pages
const Login = React.lazy(() => import("./views/pages/login/Login"));
const ForgotPassword = React.lazy(() =>
  import("./views/pages/forgot-password/forgot-password")
);
const ResetPassword = React.lazy(() =>
  import("./views/pages/reset-password/reset-password")
);
const Register = React.lazy(() => import("./views/pages/register/Register"));

//products
const Products = React.lazy(() => import("./views/products/index"));

//sales
const Sales = React.lazy(() => import("./views/sales/index"));

//prices
const Prices = React.lazy(() => import("./views/prices/index"));

//categories
const Categories = React.lazy(() => import("./views/categories/index"));

//sale transactions
const SaleTransactions = React.lazy(() =>
  import("./views/sale-transactions/index")
);

// purchase transactions
const PurchaseTransactions = React.lazy(() =>
  import("./views/purchase-transactions/index")
);

// customer
const Customers = React.lazy(() => import("./views/customer/index"));
const CustomerCreate = React.lazy(() => import("./views/customer/create"));

// vendor
const Vendors = React.lazy(() => import("./views/vendor/index"));
const VendorCreate = React.lazy(() => import("./views/vendor/create"));

//users
const Users = React.lazy(() => import("./views/users/index"));
const User = React.lazy(() => import("./components/users/create"));
const UserUpdate = React.lazy(() => import("./components/users/update"));

const Account = React.lazy(() => import("./views/users/setting"));

const routes = [
  { path: "/", exact: true, name: "Home", component: Dashboard },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/inventory", name: "Inventory", component: Inventory },

  //auth pages
  { path: "/login", name: "login", component: Login },
  { path: "/register", name: "register", component: Register },
  {
    path: "/forgot-password",
    name: "forgot-password",
    component: ForgotPassword,
  },
  { path: "/reset-password", name: "reset-password", component: ResetPassword },

  //products
  { path: "/products", name: "Products", component: Products },

  //sales
  { path: "/sales", name: "Sales", component: Sales },

  //prices
  { path: "/prices", name: "Prices", component: Prices },

  //Categoriess
  { path: "/categories", name: "Categories", component: Categories },

  //Sale Transactions
  {
    path: "/sale-transactions",
    name: "Sale Transactions",
    component: SaleTransactions,
  },

  // Purchase Transaction
  {
    path: "/purchase-transactions",
    name: "Purchase Transactions",
    component: PurchaseTransactions,
  },

  //Vendors
  {
    path: "/vendors/:id",
    exact: true,
    name: "Vendors",
    component: VendorCreate,
  },
  { path: "/vendors", exact: true, name: "Vendor", component: Vendors },

  //Customers
  {
    path: "/customers/:id",
    exact: true,
    name: "Customers",
    component: CustomerCreate,
  },
  { path: "/customers", exact: true, name: "Customer", component: Customers },

  // Users
  {
    path: "/users/account",
    exact: true,
    name: "User Account",
    component: Account,
  },
  { path: "/users/create", exact: true, name: "User Create", component: User },
  {
    path: "/users/:id",
    exact: true,
    name: "User Details",
    component: UserUpdate,
  },
  { path: "/users", exact: true, name: "Users", component: Users },

  { path: "/account", exact: true, name: "Account", component: Account },
];

export default routes;
