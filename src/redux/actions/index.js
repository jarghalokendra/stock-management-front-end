import {
  AUTH,
  AUTHENTICATE,
  SET_SIDE_BAR_SHOW,
  SET_SELECTED_IMAGES,
  REMOVE_SELECTED_IMAGE,
  RESET_SELECTED_IMAGES,
  UPDATE_EDIT_IMAGE,
  INITGRID,
  SETGRIDDATA,
  SET_SELECTED_GRID_IDS,
} from "../constant/action-types";

export function setAuth(payload) {
  return { type: AUTH, payload };
}
export function setSidebarShow(payload) {
  return { type: SET_SIDE_BAR_SHOW, payload };
}
export function setSelectedImages(payload) {
  return { type: SET_SELECTED_IMAGES, payload };
}
export function removeSelectedImage(payload) {
  return { type: REMOVE_SELECTED_IMAGE, payload };
}
export function resetSelectedImages(payload) {
  return { type: RESET_SELECTED_IMAGES, payload };
}
export function updateEditImage(payload) {
  return { type: UPDATE_EDIT_IMAGE, payload };
}
export function setInitGrid(payload) {
  return { type: INITGRID, payload };
}
export function setSelectedGridIds(payload) {
  return { type: SET_SELECTED_GRID_IDS, payload };
}
export function setGridData(payload) {
  return { type: SETGRIDDATA, payload };
}
export function setAuthenticate(payload) {
  return { type: AUTHENTICATE, payload };
}
