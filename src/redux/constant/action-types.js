export const AUTH = "AUTH";
export const SET_SIDE_BAR_SHOW = "SET_SIDE_BAR_SHOW";
export const SET_SELECTED_IMAGES = "SET_SELECTED_IMAGES";
export const REMOVE_SELECTED_IMAGE = "REMOVE_SELECTED_IMAGE";
export const RESET_SELECTED_IMAGES = "RESET_SELECTED_IMAGES";
export const UPDATE_EDIT_IMAGE = "UPDATE_EDIT_IMAGE";
export const INITGRID = "INITGRID";
export const SETGRIDDATA = "SETGRIDDATA";
export const SET_SELECTED_GRID_IDS = "SET_SELECTED_GRID_IDS";
export const AUTHENTICATE = "AUTHENTICATE";
