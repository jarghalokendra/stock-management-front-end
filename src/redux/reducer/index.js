const initialState = {
  auth: {},
  isAuthenticate: false,
  sidebarShow: "responsive",
  selectedImages: [],
  initGrid: false,
  gridData: {},
  selectedGridIds: [],
  config: {},
};

function rootReducer(state = initialState, action) {
  if (action.type === "AUTH") {
    return Object.assign({}, state, {
      auth: action.payload,
    });
  }
  if (action.type === "SET_SIDE_BAR_SHOW") {
    return Object.assign({}, state, {
      sidebarShow: action.payload.sidebarShow,
    });
  }
  if (action.type === "SET_SELECTED_IMAGES") {
    return Object.assign({}, state, {
      selectedImages: [action.payload, ...state.selectedImages],
    });
  }
  if (action.type === "REMOVE_SELECTED_IMAGE") {
    return Object.assign({}, state, {
      selectedImages: state.selectedImages.filter(
        (s, i) => i !== action.payload
      ),
    });
  }
  if (action.type === "RESET_SELECTED_IMAGES") {
    return Object.assign({}, state, {
      selectedImages: [],
    });
  }
  if (action.type === "UPDATE_EDIT_IMAGE") {
    return Object.assign({}, state, {
      selectedImages: state.selectedImages.map((file, i) => {
        return i === action.payload.index ? action.payload.file : file;
      }),
    });
  }
  if (action.type === "INITGRID") {
    return Object.assign({}, state, {
      initGrid: action.payload.init,
    });
  }

  if (action.type === "SET_SELECTED_GRID_IDS") {
    return Object.assign({}, state, {
      selectedGridIds: action.payload,
    });
  }
  if (action.type === "SETGRIDDATA") {
    return Object.assign({}, state, {
      gridData: action.payload,
    });
  }
  if (action.type === "AUTHENTICATE") {
    return Object.assign({}, state, {
      isAuthenticate: action.payload,
    });
  }
  return state;
}

export default rootReducer;
