const nep_date = [];

// Month data.
const month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

// Month for leap year
const lmonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
// Data for nepali date
const bs = [
  [2000, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2001, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2002, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2003, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2004, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2005, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2006, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2007, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2008, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
  [2009, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2010, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2011, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2012, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
  [2013, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2014, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2015, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2016, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
  [2017, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2018, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2019, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2020, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
  [2021, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2022, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
  [2023, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2024, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
  [2025, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2026, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2027, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2028, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2029, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30],
  [2030, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2031, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2032, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2033, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2034, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2035, 30, 32, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
  [2036, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2037, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2038, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2039, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
  [2040, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2041, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2042, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2043, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
  [2044, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2045, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2046, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2047, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
  [2048, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2049, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
  [2050, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2051, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
  [2052, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2053, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
  [2054, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2055, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2056, 31, 31, 32, 31, 32, 30, 30, 29, 30, 29, 30, 30],
  [2057, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2058, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2059, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2060, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2061, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2062, 30, 32, 31, 32, 31, 31, 29, 30, 29, 30, 29, 31],
  [2063, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2064, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2065, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2066, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 29, 31],
  [2067, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2068, 31, 31, 32, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2069, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2070, 31, 31, 31, 32, 31, 31, 29, 30, 30, 29, 30, 30],
  [2071, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2072, 31, 32, 31, 32, 31, 30, 30, 29, 30, 29, 30, 30],
  [2073, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 31],
  [2074, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
  [2075, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2076, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
  [2077, 31, 32, 31, 32, 31, 30, 30, 30, 29, 30, 29, 31],
  [2078, 31, 31, 31, 32, 31, 31, 30, 29, 30, 29, 30, 30],
  [2079, 31, 31, 32, 31, 31, 31, 30, 29, 30, 29, 30, 30],
  [2080, 31, 32, 31, 32, 31, 30, 30, 30, 29, 29, 30, 30],
  [2081, 31, 31, 32, 32, 31, 30, 30, 30, 29, 30, 30, 30],
  [2082, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
  [2083, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30],
  [2084, 31, 31, 32, 31, 31, 30, 30, 30, 29, 30, 30, 30],
  [2085, 31, 32, 31, 32, 30, 31, 30, 30, 29, 30, 30, 30],
  [2086, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
  [2087, 31, 31, 32, 31, 31, 31, 30, 30, 29, 30, 30, 30],
  [2088, 30, 31, 32, 32, 30, 31, 30, 30, 29, 30, 30, 30],
  [2089, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
  [2090, 30, 32, 31, 32, 31, 30, 30, 30, 29, 30, 30, 30],
];
export default class NepaliDate {
  // Data for nepali date

  is_leap_year(year) {
    let a = year;
    if (a % 100 == 0) {
      if (a % 400 == 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if (a % 4 == 0) {
        return true;
      } else {
        return false;
      }
    }
  }
  get_day_of_week(d) {
    let day;
    switch (d) {
      case 1:
        day = "Sunday";
        break;
      case 2:
        day = "Monday";
        break;
      case 3:
        day = "Tuesday";
        break;
      case 4:
        day = "Wednesday";
        break;
      case 5:
        day = "Thursday";
        break;
      case 6:
        day = "Friday";
        break;
      case 7:
        day = "Staurday";
        break;
    }
    return day;
  }

  get_nepali_month(m) {
    //let month = false;
    let month;
    switch (m) {
      case 1:
        month = "Baishakh";
        break;
      case 2:
        month = "Jestha";
        break;
      case 3:
        month = "Asar";
        break;
      case 4:
        month = "Shrawan";
        break;
      case 5:
        month = "Bhadau";
        break;
      case 6:
        month = "Ashwin";
        break;
      case 7:
        month = "Kartik";
        break;
      case 8:
        month = "Mansir";
        break;
      case 9:
        month = "Poush";
        break;
      case 10:
        month = "Magh";
        break;
      case 11:
        month = "Falgun";
        break;
      case 12:
        month = "Chaitra";
        break;
    }
    return month;
  }

  eng_to_nep(yy, mm, dd) {
    // Month data.
    // month = new array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    // Month for leap year
    // lmonth = new array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    let def_eyy = 1944; // initial english date.
    let def_nyy = 2000;
    let def_nmm = 9;
    let def_ndd = 17 - 1; // inital nepali date.
    let total_eDays = 0;
    let total_nDays = 0;
    let a = 0;
    let day = 7 - 1;
    let m = 0;
    let y = 0;
    let i = 0;
    let j = 0;
    let numDay = 0;
    // Count total no. of days in-terms year
    for (
      i = 0;
      i < yy - def_eyy;
      i++ //total days for month calculation...(english)
    ) {
      if (this.is_leap_year(def_eyy + i) === true) {
        for (j = 0; j < 12; j++) {
          total_eDays += lmonth[j];
        }
      } else {
        for (j = 0; j < 12; j++) {
          total_eDays += month[j];
        }
      }
    }
    // Count total no. of days in-terms of month
    for (i = 0; i < mm - 1; i++) {
      if (this.is_leap_year(yy) === true) {
        total_eDays += lmonth[i];
      } else {
        total_eDays += month[i];
      }
    }
    // Count total no. of days in-terms of date
    total_eDays += dd;
    i = 0;
    j = def_nmm;
    total_nDays = def_ndd;
    m = def_nmm;
    y = def_nyy;
    // Count nepali date from array
    while (total_eDays != 0) {
      a = bs[i][j];

      total_nDays++; //count the days
      day++; //count the days interms of 7 days
      if (total_nDays > a) {
        m++;
        total_nDays = 1;
        j++;
      }

      if (day > 7) {
        day = 1;
      }

      if (m > 12) {
        y++;
        m = 1;
      }

      if (j > 12) {
        j = 1;
        i++;
      }

      total_eDays--;
    }
    numDay = day;
    nep_date["year"] = y;
    nep_date["month"] = m;
    nep_date["date"] = total_nDays;
    nep_date["day"] = this.get_day_of_week(day);
    nep_date["nmonth"] = this.get_nepali_month(m);
    nep_date["num_day"] = numDay;
    return nep_date;
  }

  formatFull(post_date) {
    //const post_date = ( !empty( post_date ) ) ? strtotime( post_date ) : new Date(post_date);
    const post_date2 = new Date(post_date);
    const nepali_calender = this.eng_to_nep(
      post_date2.getFullYear(),
      post_date2.getMonth() + 1,
      post_date2.getDate()
    );

    // $converted_date = str_replace( array( 'l', 'd', 'm', 'y' ), array( $nepali_day, $nepali_date, $nepali_month, $nepali_year ), 'l, d m y' );
    // return $converted_date .= ' ' . $nepali_hour . ':' . $nepali_minute;

    return (
      nepali_calender["nmonth"] +
      " " +
      nepali_calender["date"] +
      "," +
      nepali_calender["year"] +
      " " +
      post_date2.getHours() +
      ":" +
      post_date2.getMinutes() +
      ":" +
      post_date2.getSeconds()
    );
  }

  format(post_date) {
    //const post_date = ( !empty( post_date ) ) ? strtotime( post_date ) : new Date(post_date);
    const post_date2 = new Date(post_date);
    const nepali_calender = this.eng_to_nep(
      post_date2.getFullYear(),
      post_date2.getMonth() + 1,
      post_date2.getDate()
    );

    // $converted_date = str_replace( array( 'l', 'd', 'm', 'y' ), array( $nepali_day, $nepali_date, $nepali_month, $nepali_year ), 'l, d m y' );
    // return $converted_date .= ' ' . $nepali_hour . ':' . $nepali_minute;

    return (
      nepali_calender["nmonth"] +
      " " +
      nepali_calender["date"] +
      "," +
      nepali_calender["year"]
    );
  }
}
