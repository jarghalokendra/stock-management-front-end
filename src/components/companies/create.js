import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { CCard,CCardBody,CCardHeader,CCol,CRow } from '@coreui/react'
import { Row, Col, Form, Input, Button, Alert, Spin, message, Avatar} from 'antd';
import { useSelector, useDispatch } from 'react-redux'

import http from '../../config'
import Images from '../uploads/index'
import CardHeader from '../commons/card-header'

const Company = () => {
  const history = useHistory();
  const [form] = Form.useForm();

  const [loading, setLoading] = useState(false);
  const [init, setInit] = useState(false);
  const [validationError, setValidationError] = useState('closed');
  const [invalidfield, setInvalidfield] = useState('');
  const [visible, setVisible] = useState(false);
  const [logo, setLogo] = useState(null);
  const [logo_id, setLogoId] = useState(null);
  const [isEdit, setIsEdit] = useState(false);
  const [updateId, setUpdateId] = useState(null);
  const {TextArea} = Input;

  const insertImages = useSelector(state => state.imageHandler.single)

  useEffect(() => {
    if (!init) {
      let path = history.location.pathname;
      path = path.split('/');
      if(path.length > 0 && path[2] != 'create'){
        initEdit(path[2])
      }
    }
  }, []);

  useEffect(() => {
    if(insertImages.company){
      setLogo(insertImages.company.path);
      setLogoId(insertImages.company.id)
    }
  }, [insertImages]); 
 
  const onFinish = async (values) => {
    try {
      values.logo_id = logo_id;
      setLoading(true);
      let url = isEdit ? `/companies/update/${updateId}` : 'companies/create'
      await http.post(url,values);
      setLoading(false)
      history.push('/companies')
    }
    catch(err){
      let field = err.response.data.errors['name'] ? 'name' : 'invalid';
      setInvalidfield(err.response.data.errors)
      setValidationError(err.response.data.errors[field][0])
      setLoading(false)
      message.error({ content: err.response.data.errors[field][0], duration: 2});
    }
  };
  const initEdit = async(id) => {
    try{
      setLoading(true)
      setUpdateId(id);
      let values = {};
      setInit(true)
      let result = await http.get(`/companies/${id}/edit`);
      const edit = result.data.edit;

      values.name = edit.name;
      values.description = edit.description;
      values.email = edit.email;
      values.phone = edit.phone;
      form.setFieldsValue(values);
      if(edit.logos){
        setLogo(edit.logos.path)
        setLogoId(edit.logos.id)
      }
      setInit(false)
      setIsEdit(true)
      setLoading(false)
    }
    catch(err){

    }
  }

  const handleImage = () => {
    setVisible(true)
  }

  const handleClose = () => {
    setVisible(false)
    console.log(visible)
  }

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 5 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 10 },
    },
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <CardHeader actions={{linkTitle: 'Companies', listLink: '/companies'}}/>
          <CCardBody>
            <Row gutter={16}>
              <Col className="gutter-row" offset={4} span={8}>
                {
                  validationError !=='closed' && 
                  <Alert message={validationError} type="error" showIcon closable style={{marginBottom: '10px'}}/>
                }
              </Col>
            </Row>
            <Spin spinning={loading}>
              <Form 
                form={form}
                {...formItemLayout}
                name="complex-form" 
                onFinish={onFinish} 
              >
                <Form.Item
                  label="Name"
                  name="name"
                  rules={[{ required: true, message: 'Comapny Name is required!' }]}
                >
                  <Input placeholder="Company Name"/>
                </Form.Item>
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[
                    { required: true, message: 'Email is required!' },
                    { type: 'email', message: 'The input is not valid E-mail!'}
                  ]}
                >
                  <Input placeholder="Email"/>
                </Form.Item>
                <Form.Item
                  label="Phone"
                  name="phone"
                  rules={[{ required: true, message: 'Phone no is required!' }]}
                >
                  <Input placeholder="Phone no"/>
                </Form.Item>
                <Form.Item label=" " colon={false}>
                  <div className="avatar-wrapper">
                    {
                      logo &&
                      <Avatar shape="square" size={100} src={logo}/>
                    }
                    
                    <Button type="primary" ghost onClick={handleImage}>
                      Upload Logo
                    </Button>
                  </div>
                </Form.Item>
                <Form.Item 
                  label="Description"
                  name="description"
                >
                  <TextArea rows={5}/>
                </Form.Item>
                <Form.Item label=" " colon={false}>
                  <Button type="primary" htmlType="submit">
                    Submit
                  </Button>
                </Form.Item>
              </Form>
            </Spin>
            {
              visible &&
              <Images visible={visible} storeKey={`company`} insertType={`radio`} handleClose={handleClose}/>
            }
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}
export default Company
