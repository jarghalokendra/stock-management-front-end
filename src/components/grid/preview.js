import React from 'react'
import { Modal, Button } from 'antd';

class Preview extends React.Component{
	state={

	}
	render(){
		const {config} = this.props;
		let user = config.item.user ? config.item.user.first_name + ' ' + config.item.user.last_name : '';
		const imgType = ['jpeg','jpg','png','gif','PNG'];
    if(config.item.content && config.item.content.user){
      user = config.item.content.user.first_name + '' + config.item.content.user.last_name;
    }
    if(config.item.content){
      config.item.name = config.item.content.name || config.item.content.original_name;
      let file_type = config.item.content.file_type;
      config.item.save_path = config.item.content.preview || config.item.content.save_path;
      config.item.file_type = file_type ? file_type : 'png'
    }
    if(config.item.app) {
      config.item.save_path = config.item.app.app_thumb;
      config.item.file_type = 'png'
    }
		return (
			<>
				<Modal
          title={config.item.name}
          visible={this.props.config.visible}
          onCancel={this.props.handleClose}
          width={1050}
          style={{ top: 20 }}
          footer={[
            <Button key="back" onClick={this.props.handleClose}>
              Cancel
            </Button>,
          ]}
        >
          <div className="preview-wrapper">
          	<div className="preview">
          		{
          			config.item.file_type === 'mp4' ? 
          				<video src={config.item.save_path} controls autostart="true" autoPlay={true} />
          			:(imgType.indexOf(config.item.file_type) > -1) ?
          			<img src={config.item.save_path} alt={`preview-image`}/>
          			: <a href={config.item.save_path} target="_blank">{config.item.name}</a>
          		}
          	</div>
          	<div className="info">
          		<div className="info-wrapper">
          			<table>
          				<tbody>
          					<tr>
          						<td>Format</td>
          						<td>{imgType.indexOf(config.item.file_type) > -1 ? 'image' : config.item.file_type === 'mp4' ? 'video' : 'file'}/
          						{config.item.file_type}</td>
	          				</tr>
	          				<tr>
		          				{config.item.file_size ?
								  <>
								<td>File size</td>
		          				<td>{(config.item.file_size/(1024*1024)).toFixed(2)} mb</td>
								  </>
								  :null
								  }
	          				</tr>
	          				<tr>
	          					<td>Uploaded by</td>
	          					<td>{user}</td>
	          				</tr>
	          				<tr>
		          				<td>Uploaded date</td>
		          				<td>
		          					{new Intl.DateTimeFormat("en-GB", {
      					          year: "numeric",
      					          month: "long",
      					          day: "2-digit",
      					          hour: '2-digit', 
    					            minute: '2-digit', 
    					            hour12: true,
      					        }).format(new Date(config.item.created_at))}
		          				</td>
	          				</tr>
          				</tbody>
          			</table>
          		</div>
          	</div>
          </div>
        </Modal>
			</>
		)
	}
}

export default Preview;