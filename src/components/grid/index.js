import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  Table,
  Button,
  Switch,
  Tooltip,
  Dropdown,
  Menu,
  Modal,
  Tag,
} from "antd";
import { CCardBody, CCardHeader } from "@coreui/react";
import { Link } from "react-router-dom";
import { DebounceInput } from "react-debounce-input";
import {
  DownOutlined,
  SettingOutlined,
  FilePdfOutlined,
  VideoCameraOutlined,
  MoreOutlined,
} from "@ant-design/icons";

import {
  setInitGrid,
  setSelectedGridIds,
  setGridData,
} from "../../redux/actions/index";
import http from "../../config";
import Preview from "./preview";
import SetScreen from "../commons/set-screen";
import SetContent from "../commons/set-content/index";
import {
  EditableRow,
  EditableCell,
} from "../../components/commons/edit-table-column/row";
import NepaliDate from "../classes/NepaliDate";
const nepali_date = new NepaliDate();

const Grid = (props) => {
  const { initGrid, selectedGridIds } = props;
  const { columns, acolumns } = props;
  const ids =
    selectedGridIds && selectedGridIds[acolumns.reduxKey]
      ? selectedGridIds[acolumns.reduxKey]
      : [];
  const [data, setData] = useState([]);
  const [selectedRowKeys, setSelectedRowKeys] = useState(ids);
  const [pagination, setPagination] = useState({ current: 1, pageSize: 10 });
  const [loading, setLoading] = useState(false);
  const [init, setInit] = useState(false);
  const [prevConfig, setPrevConfig] = useState({
    visible: false,
    item: {},
  });
  const [screenSetConfig, setScreenSetConfig] = useState({
    visible: false,
    item: {},
    content_type: "",
  });
  const [contentConfig, setContentConfig] = useState({
    visible: false,
    item: {},
    content_type: "",
  });
  useEffect(() => {
    if (initGrid) {
      fetch(pagination);
    }
  }, [initGrid]);

  useEffect(() => {
    if (!init) {
      fetch(pagination);
    }
  }, []);

  if (acolumns.fileColumn && acolumns.fileColumn.show) {
    let file = {
      title: "File",
      key: "file",
      render: (row) => fileHandle(row),
    };
    columns.splice(0, 0, file);
  }

  if (acolumns.statusColumn && acolumns.statusColumn.show) {
    let index = columns.findIndex((item) => item.title === "created_at");
    let status = {
      title: "status",
      key: "status",
      width: 80,
      render: (row) => (
        <div className="status-wrapper">
          <Tooltip title={row.status === 1 ? "Active" : "Blocked"}>
            <Switch
              defaultChecked={row.status === 1}
              onChange={(e) => handleChangeStatus(e, row)}
              size="small"
            />
          </Tooltip>
        </div>
      ),
    };
    columns.splice(index, 0, status);
  }
  if (acolumns.pivotRelation && acolumns.pivotRelation.items.length > 0) {
    const { items } = acolumns.pivotRelation;
    for (let i = 0; i < items.length; i++) {
      if (items[i]["show"]) {
        let index = columns.findIndex((item) => item.title === "created_at");
        let status = {
          title: items[i]["column"],
          key: items[i]["column"],
          order: items[i]["order"],
          render: (row) => handelePivotRelation(row, items[i]),
        };
        columns.splice(index, 0, status);
      }
    }
  }
  const actions = (row) => {
    const { actions } = acolumns;
    const Menus = (
      <>
        {Object.values(actions).map((item, key) => {
          let menu;
          if (item.type === "popup") {
            menu = (
              <Tooltip title={item.name}>
                <a
                  href="#"
                  onClick={(e) =>
                    item.name === "Delete"
                      ? handleDelete(e, row)
                      : props[item.callFunction](e, row)
                  }
                >
                  {item.name}
                </a>
              </Tooltip>
            );
          } else {
            menu = (
              <Tooltip title={item.name}>
                <Link to={`/${props.acolumns.apiurl}/${row.id}`}>
                  {item.name}
                </Link>
              </Tooltip>
            );
          }
          return item.show && <Menu.Item key={key}>{menu}</Menu.Item>;
        })}
      </>
    );
    return <Menu>{Menus}</Menu>;
  };

  if (acolumns.actions.show) {
    let action = {
      title: "Actions",
      key: "operation",
      order: 10000, //by default should in last row
      width: 90,
      render: (row) => (
        <div className="action-wrapper">
          <Dropdown overlay={actions(row)} placement="bottomRight">
            <Button shape="circle" icon={<MoreOutlined />} />
          </Dropdown>
        </div>
      ),
    };
    columns.push(action);
  }
  //prevent duplicate cols
  let obj = {};
  let cols = [];
  for (let i = 0; i < columns.length; i++) {
    obj[columns[i].title] = columns[i];
  }
  for (let prop in obj) {
    cols.push(obj[prop]);
  }
  cols.sort(function (a, b) {
    if (a.order < b.order) {
      return -1;
    }
    if (a.order > b.order) {
      return 1;
    }
    return 0;
  });

  const fileHandle = (row) => {
    let item = row[acolumns["fileColumn"]["relationName"]]
      ? row[acolumns["fileColumn"]["relationName"]]
      : row;
    const imageType = ["jpeg", "png", "jpg", "gif", "PNG"];
    let save_path = item[acolumns["fileColumn"]["imageName"]];
    if (save_path) {
      item.save_path = save_path;
      item.file_type = item.file_type ? item.file_type : "png";
      item.user = row.user;
    }
    const t = (
      <div className="status-wrapper">
        {imageType.indexOf(item.file_type) > -1 ? (
          <a href="#" onClick={(e) => handlePreview(e, item)}>
            <img
              src={save_path}
              style={{ width: "60px", height: "50px" }}
              alt={item.original_name}
            />
          </a>
        ) : item.file_type === "mp4" ? (
          <div className="file-type">
            <a href="#" onClick={(e) => handlePreview(e, item)}>
              <VideoCameraOutlined />
            </a>
          </div>
        ) : (
          <div className="file-type">
            <a href="#" onClick={(e) => handlePreview(e, item)}>
              <FilePdfOutlined />
            </a>
          </div>
        )}
      </div>
    );

    return t;
  };

  const handelePivotRelation = (row, item) => {
    const { relationName, dType } = item;
    let relationColName = item.relationColName,
      d;
    let rel = relationName.split(".");
    let arel = row;
    for (let p in rel) {
      arel = arel[rel[p]];
    }
    const r = arel;
    if (dType === "obj" && r) {
      d = (
        <div
          className="pivot-relation"
          style={{ width: "90%", overflow: "auto" }}
        >
          <div className="table-col">
            {r[relationColName]
              ? r[relationColName].length > 20
                ? `${r[relationColName].substring(0, 20)} ...`
                : r[relationColName]
              : ""}
          </div>
        </div>
      );
    } else if (dType === "array" && r) {
      d = (
        <div className="pivot-relation">
          {r.map((item, index) => {
            return (
              <Tag color="cyan" key={index}>
                {item[relationColName]}
              </Tag>
            );
          })}
        </div>
      );
    }
    return d;
  };

  const handlePreview = (e, item) => {
    e.preventDefault();
    setPrevConfig({
      visible: true,
      item: item,
    });
  };

  const handleTableChange = (pag, filters, sorter) => {
    let params = {
      ...pagination,
      sortField: sorter.field,
      sortOrder: sorter.order,
      current: pag.current,
      pageSize: pag.pageSize,
      total: pag.total,
    };
    //params = Object.assign({},pagination,params);
    setPagination(params);
    fetch(params);
  };

  const fetch = async (params) => {
    try {
      setLoading(true);
      setInit(true);
      let url = `/${acolumns.apiurl}?page=${params.current}`;
      let d = await http(url, { params: params });
      let dT = d.data.data.data;
      dT.forEach((item) => {
        item.created_at = nepali_date.formatFull(item.created_at);
        item.updated_at = nepali_date.formatFull(item.updated_at);

        // item.created_at = dateFormat(item.created_at);
        // item.updated_at = dateFormat(item.updated_at);
      });
      setData(dT);
      if (acolumns.reduxKey)
        props.setGridData({ [acolumns["reduxKey"]]: d.data.data.data });

      setLoading(false);
      setInit(false);
      setPagination({
        ...pagination,
        current: d.data.data.current_page,
        pageSize: d.data.data.per_page,
        total: d.data.data.total,
      });
      props.setInitGrid({ init: false });
    } catch (err) {
      setLoading(false);
    }
  };
  const dateFormat = (date) => {
    return new Intl.DateTimeFormat("en-GB", {
      year: "numeric",
      month: "long",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      hour12: true,
    }).format(new Date(date));
  };

  const handleMultipleCheck = (selectedRowKeys) => {
    setSelectedRowKeys(selectedRowKeys);
    props.setSelectedGridIds(selectedRowKeys);
  };

  const handleChangeStatus = async (e, row) => {
    try {
      await http.post(`/${acolumns.apiurl}/status`, { status: e, id: row.id });
      fetch(pagination);
    } catch (err) {}
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: handleMultipleCheck,
  };

  const type =
    acolumns.selection && acolumns.selection.type
      ? acolumns.selection.type
      : "checkbox";
  let selectionRow = {
    type: type,
    ...rowSelection,
  };
  const isHideRowSelection = acolumns.selection && !acolumns.selection.show;
  if (isHideRowSelection) {
    selectionRow = null;
  }

  const handleDelete = (e, item) => {
    let items = item === "null" ? selectedRowKeys : [item.id];
    if (items.length <= 0) {
      Modal.error({
        title: `Items are not selected yet.`,
        content: `You need to confirm, atleast one item is selected.`,
        okText: "Cancel",
        okType: "info",
      });
      return 0;
    }
    Modal.confirm({
      title: `Want to delete ${items.length} ${
        items.length === 1 ? "item" : "items"
      }?`,
      content: `No longer selected items will be store in database.`,
      cancelText: "Cancel",
      okText: "Delete",
      okType: "danger",
      onOk: () => {
        confirmDelete(items);
      },
    });
  };
  const confirmDelete = async (items) => {
    try {
      setLoading(true);
      let id = items;
      await http.delete(`/${props.acolumns.apiurl}/delete`, {
        data: {
          id: id,
        },
      });
      fetch(pagination);
      setSelectedRowKeys([]);
    } catch (err) {
      setLoading(false);
      // console.log(err.response);
      if (err.response.status === 422) {
        Modal.error({
          title: `Item couldn't be deleted!`,
          content: `${err.response.data.message}`,
        });
      }
    }
  };

  const handleFilter = (e) => {
    setPagination({ ...pagination, search: e.value });
    fetch({ ...pagination, search: e.value });
  };

  const handleSave = async (row) => {
    const newData = [...data];
    const index = newData.findIndex((item) => row.id === item.id);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    setData(newData);
    try {
      let r = await http.put(`/${acolumns.apiurl}/${row.id}`, row);
    } catch (err) {}
  };

  const menu = (
    <Menu>
      {props.acolumns.settings.createLink === "" && (
        <Menu.Item key="1">
          <a href="#" onClick={(e) => props.handleCreate(e)}>
            {props.acolumns.settings.createTitle}
          </a>
        </Menu.Item>
      )}
      {props.acolumns.settings.createLink !== "" && (
        <Menu.Item key="1">
          <Link to={props.acolumns.settings.createLink}>
            {props.acolumns.settings.createTitle}
          </Link>
        </Menu.Item>
      )}

      <Menu.Item key="2">
        <a href="#" onClick={(e) => handleDelete(e, "null")}>
          Delete
        </a>
      </Menu.Item>
    </Menu>
  );

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };
  cols = cols.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        editable: col.editable.toString(),
        dataindex: col.dataIndex,
        title: col.title,
        handleSave: handleSave,
      }),
    };
  });
  return (
    <>
      <CCardHeader className="card-header-wrapper">
        <div className="card-title">
          {props.acolumns.title} | {data.length} of total: {pagination.total}{" "}
          items are
          <small className="text-muted"> List </small>
          {selectedRowKeys.length > 0 && (
            <span
              style={{
                background: "#1890ff",
                padding: "5px 10px",
                color: "#fff",
              }}
            >{`${selectedRowKeys.length} items are checked`}</span>
          )}
        </div>
        <div className="card-search-input">
          <DebounceInput
            minLength={2}
            debounceTimeout={300}
            onChange={(e) =>
              handleFilter({ name: e.target.name, value: e.target.value })
            }
            placeholder="filters"
            className="debounce-input"
          />
        </div>
        <div className="card-header-actions">
          {acolumns.settings.show && (
            <Dropdown overlay={menu} placement="bottomRight">
              <Button type="primary" icon={<SettingOutlined />}>
                Settings <DownOutlined />
              </Button>
            </Dropdown>
          )}
        </div>
      </CCardHeader>
      <CCardBody>
        <Table
          components={components}
          rowClassName={() => "editable-row"}
          className="table-wrapper"
          columns={cols}
          rowSelection={selectionRow}
          rowKey={(record) => record.id}
          dataSource={data}
          pagination={pagination}
          loading={loading}
          onChange={handleTableChange}
          size="small"
          fixed={false}
          scroll={{ y: 460 }}
          style={{ overflowY: "auto" }}
        />
      </CCardBody>
      {prevConfig.visible && (
        <Preview
          config={prevConfig}
          handleClose={() => setPrevConfig({ visible: false })}
        />
      )}
      {screenSetConfig.visible && (
        <SetScreen
          config={screenSetConfig}
          handleClose={() => setScreenSetConfig({ visible: false })}
        />
      )}
      {contentConfig.visible && (
        <SetContent
          config={contentConfig}
          handleClose={() => setContentConfig({ visible: false })}
        />
      )}
    </>
  );
};
const mapStateToProps = (state) => ({
  initGrid: state.initGrid,
  selectedGridIds: state.selectedGridIds,
});

export default connect(mapStateToProps, {
  setInitGrid,
  setSelectedGridIds,
  setGridData,
})(Grid);
