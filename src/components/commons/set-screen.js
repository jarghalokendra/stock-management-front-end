import React from 'react'
import { Modal, Button, message, Alert} from 'antd';
import { connect } from "react-redux"

import GridTable from '../grid/index'
import http from '../../config'

class setScreen extends React.Component{
	state={
		loading: false,
		errors: []
	}
	handleConfirm = async() =>{
		const {selectedGridIds} = this.props;
		const {content_type} = this.props.config;
		const {id} = this.props.config.item;
		const params = {
			setToScreens: selectedGridIds,
			setId: id,
			content_type: content_type
		}
		try{
			this.setState({loading: true})
			await http.post('/screens/set-to-screens',params)
			this.setState({loading: false})
			this.props.handleClose();
		}
		catch(err){
			let error;
			if(typeof err.response.data.message === 'string'){
				error = err.response.data.message;
			}
			let ers = []
			if(err.response.data.errors){
				let errs = err.response.data.errors
				for(let p in errs){
					let obj = {};
					obj.error = errs[p][0];
					ers.push(obj)
				}
				this.setState({errors: ers});
			}
			this.setState({loading: false})
			message.error({ content: error, duration: 2});
		}
	}
	render(){
		const {config,selectedGridIds} = this.props;
		const columns = [
		{
		  title: 'Name',
		  dataIndex: 'name',
		  sorter: true
		},
		{
		  title: 'created_at',
		  dataIndex: 'created_at',
		  sorter: true,
		}]
		const acolumns = {
		  apiurl: 'screens',
		  title: 'Screens',
		  reduxKey: 'screens',
		  settings: {
		  	show: false,
		    createTitle: 'Add Screen',
		    createLink: '',
		  },
		  pivotRelation: {
		    items: [{
		      column: 'Active Screen',
		      show: true,
		      relationName: 'content',
		      relationColName: 'original_name',
		      imageExist: true,
		      imageShow: true,
		      dType: 'obj'//we can pass dType like array or object
		    }]
		  },
		  statusColumn: {
		    column: 'status',
		    show: false
		  },
		  actions: {
		  	show: false,
		    edit: {
		      show: true,
		      name: 'Edit',
		      type: 'popup' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
		    },
		    delete:{
		      show: true,
		      name: 'Delete',
		    },
		    screen: {
		      show: true,
		      name: 'Set Content',
		      type: 'set-content' //it should be only two types set-to-screen or set-content
		    }
		  } 
		}
		const {errors} = this.state
		return (
			<>
				<Modal
          title={`Set to Screen ${config.item.original_name ? config.item.original_name : config.item.name}`}
          visible={config.visible}
          onCancel={this.props.handleClose}
          width={1050}
          style={{ top: 20 }}
          onOk={this.handleConfirm}
          footer={[
            <Button key="back" onClick={this.props.handleClose}>
              Cancel
            </Button>,
            <Button 
            	key="submit" 
            	type="primary" 
            	onClick={this.handleConfirm} 
            	loading={this.state.loading}
            	disabled={selectedGridIds.length<=0}
            >
              Confirm
            </Button>,
          ]}
        >
          <div className="screens-wrapper">
          	{
          		errors.length > 0 && 
          		 errors.map((item, index)=>{
          		 	return(
          		 		<Alert message={item.error} type="error" showIcon key={index} closable/>
          		 	)
          		 })
          	}
          	{
          		config.visible &&
          			<GridTable 
		          	  columns={columns} 
		          	  acolumns={acolumns} 
          			/> 
          	}
          	
          </div>
        </Modal>
			</>
		)
	}
}
const mapStateToProps = state => ({
  selectedGridIds: state.selectedGridIds
});
export default connect(mapStateToProps, {}) (setScreen);