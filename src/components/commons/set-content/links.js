import React from 'react'

import GridTable from '../../grid/index'

class Link extends React.Component{
	render(){
		const {config,selectedGridIds} = this.props;
		const columns = [
		{
		  title: 'Name',
		  dataIndex: 'name',
		  sorter: true
		},
		{
		  title: 'Link URL',
		  dataIndex: 'link_url',
		  sorter: true
		},
		{
		  title: 'created_at',
		  dataIndex: 'created_at',
		  sorter: true,
		}]
		const acolumns = {
		  apiurl: 'links',
		  title: 'Links',
		  reduxKey: 'links',
		  settings: {
		  	show: false,
		    createTitle: 'Add Screen',
		    createLink: '',
		  },
		  statusColumn: {
		    column: 'status',
		    show: false
		  },
		  selection: {
		  	type: 'radio',
		  	show: true
		  },
		  fileColumn: {
		  	column: 'file',
		  	show: true,
		  	imageName: 'preview'
		  	// relationName: 'preview'
		  },
		  actions: {
		  	show: false,
		    edit: {
		      show: true,
		      name: 'Edit',
		      type: 'popup' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
		    },
		    delete:{
		      show: true,
		      name: 'Delete',
		    },
		    screen: {
		      show: true,
		      name: 'Set Content',
		      type: 'set-content' //it should be only two types set-to-screen or set-content
		    }
		  } 
		}
		return (
			<>
  			<GridTable 
      	  columns={columns} 
      	  acolumns={acolumns} 
  			/> 
			</>
		)
	}
}
export default Link;