import React from 'react'

import GridTable from '../../grid/index'

class Medias extends React.Component{
	render(){
		const {config,selectedGridIds} = this.props;
		const columns = [
		{
		  title: 'Name',
		  dataIndex: 'original_name',
		  sorter: true
		},
		{
		  title: 'created_at',
		  dataIndex: 'created_at',
		  sorter: true,
		}]
		const acolumns = {
		  apiurl: 'medias',
		  title: 'Medias',
		  reduxKey: 'medias',
		  settings: {
		  	show: false,
		    createTitle: 'Add Screen',
		    createLink: '',
		  },
		  fileColumn: {
		    column: 'file',
		    show: true,
		    imageName: 'save_path'
		  },
		  statusColumn: {
		    column: 'status',
		    show: false
		  },
		  selection: {
		  	type: 'radio',
		  	show: true
		  },
		  actions: {
		  	show: false,
		    edit: {
		      show: true,
		      name: 'Edit',
		      type: 'popup' //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit} 
		    },
		    delete:{
		      show: true,
		      name: 'Delete',
		    },
		    screen: {
		      show: true,
		      name: 'Set Content',
		      type: 'set-content' //it should be only two types set-to-screen or set-content
		    }
		  } 
		}
		return (
			<>
  			<GridTable 
      	  columns={columns} 
      	  acolumns={acolumns} 
  			/> 
			</>
		)
	}
}
export default Medias;