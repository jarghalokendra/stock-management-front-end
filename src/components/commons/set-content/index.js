import React from 'react'
import { Modal, Button, message, Alert, Menu, Switch, Divider,Row,Col, Spin} from 'antd';
import { connect } from "react-redux"
import {
  UnorderedListOutlined,
  LinkOutlined,
  FileAddOutlined
} from '@ant-design/icons';
import {
  FaPaintBrush,FaLink
} from 'react-icons/fa';
import {
  VscFileMedia
} from 'react-icons/vsc';
import {
  ImList2
} from 'react-icons/im';
import {
  AiOutlineAppstore
} from 'react-icons/ai';

import {setInitGrid, setSelectedGridIds} from '../../../redux/actions/index'
import http from '../../../config'
import Medias from './medias'
import PlayList from './playlist'
import Link from './links'
import Canvas from './canvas'
import Apps from './apps'
import styles from './set-content.module.css';

class setContent extends React.Component{
	state={
		loading: false,
		errors: [],
		menuKey: '',
		content_type: 'playlists'
	}
	componentDidMount(){
		const {item} = this.props.config;
		let menuKey;
		if(item.content){
			if(item.content_type === 'medias'){
				menuKey = '3'
				this.props.setSelectedGridIds({medias: [item.content.id]})
			}
			else if(item.content_type === 'links'){
				this.props.setSelectedGridIds({links: [item.content.id]})
				menuKey = '2'
			}
			else if(item.content_type === 'canvas'){
				this.props.setSelectedGridIds({canvas: [item.content.id]})
				menuKey='4'
			}
			else if(item.content_type === 'apps'){
				this.props.setSelectedGridIds({apps: [item.content.id]})
				menuKey='5'
			}
			else{
				menuKey = '1'
				this.props.setSelectedGridIds({playlists: [item.content.id]})
			}
			this.setState({menuKey: menuKey,content_type:item.content_type})
		}
		
	}
	handleConfirm = async() =>{
		const {selectedGridIds} = this.props;
		const {content_type} = this.props.config;
		const {id} = this.props.config.item;
		const params = {
			setToScreens: [id],
			setId: selectedGridIds[0],
			content_type: this.state.content_type
		}
		try{
			this.setState({loading: true})
			await http.post('/screens/set-to-screens',params)
			this.setState({loading: false})
			this.props.setInitGrid({init: true})
			this.props.handleClose();
		}
		catch(err){
			let error;
			if(typeof err.response.data.message === 'string'){
				error = err.response.data.message;
			}
			let ers = []
			if(err.response.data.errors){
				let errs = err.response.data.errors
				for(let p in errs){
					let obj = {};
					obj.error = errs[p][0];
					ers.push(obj)
				}
				this.setState({errors: ers});
			}
			this.setState({loading: false})
			this.props.setInitGrid({initGrid: true})
			message.error({ content: error, duration: 2});
			this.props.setInitGrid({init: true})
		}
	}
	handleMenu(e,item){
		this.setState({menuKey: e.key})
		let content_type;
		if(e.key === '1')
			content_type = 'playlists';
		else if(e.key==='2')
			content_type = 'links';
		else if(e.key === '3')
			content_type = 'medias'
		else if(e.key === '4')
			content_type = 'canvas'
		else if(e.key === '5')
			content_type = 'apps'
		this.setState({content_type: content_type})
	}
	render(){
		const {config,selectedGridIds} = this.props;
		const {errors} = this.state
		const {item} = this.props.config;
		return (
			<>
				<Modal
          title={`Set Content to ${item.name}`}
          visible={config.visible}
          onCancel={this.props.handleClose}
          width={1200}
          bodyStyle={{padding:'0'}}
          style={{ top: 20 }}
          onOk={this.handleConfirm}
          footer={[
            <Button key="back" onClick={this.props.handleClose}>
              Cancel
            </Button>,
            <Button 
            	key="submit" 
            	type="primary" 
            	onClick={this.handleConfirm} 
            	loading={this.state.loading}
            	disabled={selectedGridIds.length<=0}
            >
              Confirm
            </Button>,
          ]}
        >
          <div className="screens-wrapper">
          	{
          		errors.length > 0 && 
          		 errors.map((item, index)=>{
          		 	return(
          		 		<Alert message={item.error} type="error" showIcon key={index} closable/>
          		 	)
          		 })
          	}
          </div>
          <Spin spinning={this.state.loading} tip="Loading...">
	          <Row>
	          	<Col span={6}>
			          <Menu
			            defaultSelectedKeys={[this.state.menuKey]}
			            selectedKeys={[this.state.menuKey]}
			            mode={`vertical`}
			            className={styles['set-content-menu']}
			            style={{height: '460px'}}
			          >
			            <Menu.Item key="1" onClick={(key, item)=>this.handleMenu(key,item)} icon={<ImList2 />}>
			              PlayList
			            </Menu.Item>
			            <Menu.Item key="2" onClick={(key,item)=>this.handleMenu(key,item)} icon={<FaLink />}>
			              Links
			            </Menu.Item>
			            <Menu.Item key="3" onClick={(key,item)=>this.handleMenu(key,item)} icon={<VscFileMedia />}>
			              Medias
			            </Menu.Item>
			            <Menu.Item key="4" onClick={(key,item)=>this.handleMenu(key,item)} icon={<FaPaintBrush />}>
			              Canvas
			            </Menu.Item>
			             <Menu.Item key="5" onClick={(key,item)=>this.handleMenu(key,item)} icon={<AiOutlineAppstore />}>
			              Apps
			            </Menu.Item>
			          </Menu>
			        </Col>
			        <Col span={18}>
			          {
			          	this.state.menuKey === '1' &&
			          		<PlayList/>
			          }
			          {
			          	this.state.menuKey === '2' &&
			          		<Link/>
			          }
			          {
			          	this.state.menuKey === '3' &&
			          		<Medias/>
			          }
			          {
			          	this.state.menuKey === '4' &&
			          		<Canvas/>
			          }
			          {
			          	this.state.menuKey === '5' &&
			          		<Apps/>
			          }
			        </Col>
		        </Row>
		      </Spin>
        </Modal>
			</>
		)
	}
}
const mapStateToProps = state => ({
  selectedGridIds: state.selectedGridIds
});
export default connect(mapStateToProps, {setInitGrid, setSelectedGridIds}) (setContent);