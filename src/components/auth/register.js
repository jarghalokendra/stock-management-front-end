import React, { useState } from "react";
import { Form, Input, Button, Alert, Spin, message } from "antd";
import {
  UserOutlined,
  LockOutlined,
  UserAddOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { useHistory, Link } from "react-router-dom";

import http from "../../config";

const Register = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [validationError, setValidationError] = useState("closed");
  const [errors, setErrors] = useState([]);

  const onFinish = async (values) => {
    try {
      setLoading(true);
      let result = await http.post("/auth/register", values);
      setLoading(false);
      history.push("/login");
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
      }
      setValidationError(error);
      setLoading(false);
      message.error({ content: error, duration: 2 });
    }
  };

  return (
    <>
      <div className="title-wrapper">
        <h1>Register</h1>
        <p className="text-muted">Setup your new account</p>
      </div>
      {validationError !== "closed" && (
        <Alert
          message="Validation errors"
          description={validationError}
          type="error"
          showIcon
          closable
          style={{ marginBottom: "10px" }}
        />
      )}
      {errors.length > 0 &&
        errors.map((item, index) => {
          return (
            <Alert
              message={item.error}
              type="error"
              showIcon
              key={index}
              closable
            />
          );
        })}
      <Spin spinning={loading}>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item style={{ marginBottom: 0 }}>
            <Form.Item
              name="first_name"
              rules={[
                { required: true, message: "Please input your First Name!" },
              ]}
              style={{ display: "inline-block", width: "calc(50% - 8px)" }}
            >
              <Input
                prefix={<UserAddOutlined className="site-form-item-icon" />}
                placeholder="First Name"
              />
            </Form.Item>
            <Form.Item
              name="last_name"
              style={{
                display: "inline-block",
                width: "calc(50% - 8px)",
                margin: "0 8px",
              }}
            >
              <Input
                prefix={<UserAddOutlined className="site-form-item-icon" />}
                placeholder="Last Name"
              />
            </Form.Item>
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Please input your email!" },
              { type: "email", message: "The input is not valid E-mail!" },
            ]}
            style={{ marginRight: "8px" }}
          >
            <Input prefix={<UserOutlined />} placeholder="Email" />
          </Form.Item>
          <Form.Item style={{ marginBottom: 0 }}>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
                { min: 8, message: "Password must be minimum 8 characters." },
              ]}
              style={{ display: "inline-block", width: "calc(50% - 8px)" }}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item
              name="password_confirmation"
              dependencies={["password"]}
              hasFeedback
              style={{
                display: "inline-block",
                width: "calc(50% - 8px)",
                margin: "0 8px",
              }}
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      "The password that you entered do not match!"
                    );
                  },
                }),
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
                type="password"
                placeholder="Confirm Password"
              />
            </Form.Item>
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              ghost
            >
              Register
            </Button>
          </Form.Item>
          Already registered ?
          <Link to="/login" className="login-form-forgot">
            &nbsp;Login
          </Link>
        </Form>
      </Spin>
    </>
  );
};

export default Register;
