import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Input, Button, Alert, Spin, message } from "antd";
import {
  LockOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { useHistory, useLocation, Link } from "react-router-dom";

import http from "../../config";
import { setAuth } from "../../redux/actions/index";

const ResetPass = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [validationError, setValidationError] = useState("closed");
  const [errors, setErrors] = useState([]);
  const parameter = useLocation()
    .search.replace("?", "")
    .split("&")
    .reduce(
      (r, e) => ((r[e.split("=")[0]] = decodeURIComponent(e.split("=")[1])), r),
      {}
    );
  const email = parameter.email;
  const token = parameter.token;
  useEffect(() => {
    if (!token || !email) history.push("/login");
    else validateResetToken();
  }, []);
  const validateResetToken = async () => {
    try {
      let validate = await http.post("/auth/validate-reset-token", parameter);
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
      }
      setValidationError(error);
      setLoading(false);
      message.error({ content: error, duration: 2 });
      history.push("/login");
    }
  };

  const onFinish = async (values) => {
    try {
      values.token = token;
      values.email = email;
      setLoading(true);
      await http.post("/auth/reset-password", values);
      setLoading(false);
      history.push("/login");
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
      }
      setValidationError(error);
      setLoading(false);
      message.error({ content: error, duration: 2 });
    }
  };

  return (
    <>
      <div className="title-wrapper">
        <h1>Password Reset</h1>
        <p className="text-muted">Enter New Password</p>
      </div>
      {validationError !== "closed" && (
        <Alert
          message="Validation errors"
          description={validationError}
          type="error"
          showIcon
          closable
          style={{ marginBottom: "10px" }}
        />
      )}
      {errors.length > 0 &&
        errors.map((item, index) => {
          return (
            <Alert
              message={item.error}
              type="error"
              showIcon
              key={index}
              closable
            />
          );
        })}
      <Spin spinning={loading}>
        <Form name="normal_login" className="login-form" onFinish={onFinish}>
          <Form.Item style={{ marginBottom: 0 }}>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
                { min: 8, message: "Password must be minimum 8 characters." },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item
              name="password_confirmation"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      "The password that you entered do not match!"
                    );
                  },
                }),
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
                type="password"
                placeholder="Confirm Password"
              />
            </Form.Item>
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              ghost
            >
              Reset Password
            </Button>
            &nbsp; &nbsp;<Link to="/login">Cancel</Link>
          </Form.Item>
        </Form>
      </Spin>
    </>
  );
};

export default ResetPass;
