import React, { useState } from 'react';
import Google_Login from 'react-google-login';
import http from '../../config'
import { setAuth } from "../../redux/actions/index";
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { Form, Input, Button, Alert, Spin, message } from 'antd';

const GoogleLogin = (props) => {
  const history = useHistory();
  const [errors, setErrors] = useState([])
  const dispatch = useDispatch();


  const responseGoogle = async (response) => {
    try {
      props.startLoading();
      let result = await http.post('/auth/sociallogin', response.profileObj);
      props.stopLoading();
      let auth = result.data;
      localStorage.setItem('access_token', auth.access_token)
      dispatch(setAuth({ auth: auth }))
      history.push('/dashboard')
    }
    catch (err) {
      let error;
      if (typeof err.response.data.message === 'string') {
        error = err.response.data.message;
      }
      let ers = []
      if (err.response.data.errors) {
        let errs = err.response.data.errors
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj)
        }
        setErrors(ers);
      }
      message.error({ content: error, duration: 2 });
      history.push('/login')
    }
  }

  return (
    <Google_Login
      clientId="66686623504-hi84i6t70k83er2228sa1cm4s2bb2jbp.apps.googleusercontent.com"
      render={renderProps => (
        <button onClick={renderProps.onClick} disabled={renderProps.disabled} className="ant-btn ant-btn-primary ant-btn-background-ghost"><i className="fa fa-google"></i> &nbsp;Login with Google</button>
      )}
      buttonText="Login"
      onSuccess={responseGoogle}
      // onFailure={responseGoogle}
      cookiePolicy={'single_host_origin'}
    />
  )
}
export default GoogleLogin;