import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Form, Input, Button, Checkbox, Alert, Spin, message } from "antd";
import {
  UserOutlined,
  LockOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { useHistory, Link } from "react-router-dom";
import http from "../../config";
import { setAuth } from "../../redux/actions/index";

const Login = (props) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [validationError, setValidationError] = useState("closed");
  const [errors, setErrors] = useState([]);

  useEffect(() => {
    if (
      props.isAuthenticate ||
      localStorage.getItem("isAuthenticate") === "true"
    ) {
      history.push("/");
    }
  }, []);

  const onFinish = async (values) => {
    try {
      setLoading(true);
      let result = await http.post("/auth/login", values);
      setLoading(false);
      let auth = result.data;
      localStorage.setItem("access_token", auth.access_token);
      props.setAuth({ auth: auth });
      history.push("/dashboard");
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
      }
      setValidationError(error);
      setLoading(false);
      message.error({ content: error, duration: 2 });
    }
  };

  return (
    <>
      <div className="title-wrapper">
        <h1>Login</h1>
        <p className="text-muted">Sign In to your account</p>
      </div>
      {validationError !== "closed" && (
        <Alert
          message="Validation errors"
          description={validationError}
          type="error"
          showIcon
          closable
          style={{ marginBottom: "10px" }}
        />
      )}
      {errors.length > 0 &&
        errors.map((item, index) => {
          return (
            <Alert
              message={item.error}
              type="error"
              showIcon
              key={index}
              closable
            />
          );
        })}
      <Spin spinning={loading}>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Please input your E-mail!" },
              { type: "email", message: "The input is not valid E-mail!" },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              iconRender={(visible) =>
                visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
              }
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Link
              to="/forgot-password"
              className="login-form-forgot"
              style={{ marginLeft: 73 }}
            >
              Forgot Password
            </Link>
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              ghost
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
      </Spin>
    </>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticate: state.isAuthenticate,
});

export default connect(mapStateToProps, { setAuth })(Login);
