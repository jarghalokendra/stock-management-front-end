import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Input, Button, Alert, Spin, message } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { useHistory, useLocation, Link } from "react-router-dom";

import http from "../../config";

const ForgotPass = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [validationError, setValidationError] = useState("closed");
  const [errors, setErrors] = useState([]);

  const onFinish = async (values) => {
    try {
      setLoading(true);
      await http.post("/auth/forgot-password", values);
      setLoading(false);
      setErrors([]);
      setValidationError("closed");
      history.push("/login");
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
      }
      setValidationError(error);
      setLoading(false);
      message.error({ content: error, duration: 2 });
    }
  };

  return (
    <>
      <div className="title-wrapper">
        <h1>Password recovery</h1>
        <p className="text-muted">We'll send you instructions in email</p>
      </div>
      {validationError !== "closed" && (
        <Alert
          message="Validation errors"
          description={validationError}
          type="error"
          showIcon
          closable
          style={{ marginBottom: "10px" }}
        />
      )}
      {errors.length > 0 &&
        errors.map((item, index) => {
          return (
            <Alert
              message={item.error}
              type="error"
              showIcon
              key={index}
              closable
            />
          );
        })}
      <Spin spinning={loading}>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            rules={[
              { required: true, message: "Please input your email!" },
              { type: "email", message: "The input is not valid E-mail!" },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              ghost
            >
              Reset Password
            </Button>
            &nbsp; &nbsp;<Link to="/login">Go back!</Link>
          </Form.Item>
        </Form>
      </Spin>
    </>
  );
};

export default ForgotPass;
