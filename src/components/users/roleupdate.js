import React from "react";
import { Card, Button, Divider } from "antd";
import Roles from "../account/roles";
import http from "../../config";

class RolesUpdate extends React.Component {
  state = {
    data: [{}],
    userId: [this.props.userId],
  };
  rolesCallBack = (rolesData) => {
    this.setState({ data: rolesData });
  };

  changeRoles = async () => {
    const data = this.state.data;
    data.users = this.state.userId[0];
    try {
      await http.post("permissions/store", data);
      this.props.closeRole();
    } catch (err) {
      console.log(err);
    }
  };
  render() {
    return (
      <>
        <Card
          title="Assign Roles & Permissions"
          style={{ marginLeft: "auto", marginRight: "auto" }}
        >
          <Roles
            userId={this.state.userId}
            rolesCallBack={this.rolesCallBack}
          />
          <Button
            type="primary"
            style={{ float: "right", marginTop: "15px" }}
            onClick={this.changeRoles}
          >
            Save
          </Button>
        </Card>
      </>
    );
  }
}
export default RolesUpdate;
