import React from 'react'
import { Card } from 'antd';
import MyAccount from "../../components/account/myaccount"
import Credentials from "../../components/account/credentials"
import UpdateRoles from "./roleupdate"
import Roles from "../account/roles"

class UpdateAccount extends React.Component {
    state = {
        noTitleKey: 'account',
        updateUser: true,
        updateId: "",
    };
    onTabChange = (key, type) => {
        //   console.log(key, type);
        this.setState({ [type]: key });
    };
    render() {
        const ids = this.props.match.params.id;
        const id = parseInt(ids, 10);
        const tabListNoTitle = [
            {
                key: 'account',
                tab: 'Update Account',
            },
            {
                key: 'credentials',
                tab: 'Update Credentials',
            },
            {
                key: 'permission',
                tab: 'Roles & Permissions',
            },
        ];

        const contentListNoTitle = {
            account: <MyAccount updateUser={this.state.updateUser} updateId={id} />,
            credentials: <Credentials updateUser={this.state.updateUser} updateId={id} />,
            permission: <UpdateRoles userId={id} />

        };


        return (
            <>
                <Card
                    style={{ width: '100%' }}
                    tabList={tabListNoTitle}
                    activeTabKey={this.state.noTitleKey}
                    onTabChange={key => {
                        this.onTabChange(key, 'noTitleKey');
                    }}
                >
                    {contentListNoTitle[this.state.noTitleKey]}

                </Card>
            </>
        )
    }
}

export default UpdateAccount