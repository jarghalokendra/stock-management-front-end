import React from "react";
import {
  Avatar,
  Upload,
  Row,
  Card,
  Form,
  Input,
  Button,
  Alert,
  Spin,
  message,
} from "antd";
import {
  UploadOutlined,
  UserOutlined,
  UserAddOutlined,
  PhoneOutlined,
  HomeOutlined,
  LockOutlined,
  EyeTwoTone,
  EyeInvisibleOutlined,
} from "@ant-design/icons";

import http from "../../config";

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
}

class User extends React.Component {
  formRef = React.createRef();
  state = {
    loading: false,
    imageUrl: "",
    preview: "",
    imagefile: "",
    validationerror: "closed",
    errors: [],
  };

  handleUpload = async (file) => {
    let previewUrl = URL.createObjectURL(file);
    this.setState({ preview: previewUrl, imagefile: file });
  };

  onFinish = async (values) => {
    try {
      this.setState({ loading: true, validationerror: "closed", errors: [] });
      let formData = new FormData();
      formData.append("file", this.state.imagefile);
      formData.append("first_name", values.first_name);
      formData.append("last_name", values.last_name);
      formData.append("email", values.email);
      formData.append("password", values.password);
      formData.append("phone", values.phone);
      formData.append("address", values.address);
      await http.post("/users", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      this.setState({
        loading: false,
        imageUrl: "",
        preview: "",
        imagefile: "",
      });
      this.formRef.current.resetFields();
      this.props.history.push("/users");
    } catch (err) {
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        this.setState({
          errors: ers,
        });
      }
      this.setState({
        validationerror: error,
      });
      message.error({ content: error, duration: 2 });
      this.setState({ loading: false });
    }
  };
  render() {
    return (
      <>
        <Card title="Create User">
          <div className="title-wrapper"></div>
          {this.state.validationerror !== "closed" && (
            <Alert
              message="Validation errors"
              description={this.state.validationerror}
              type="error"
              showIcon
              closable
              style={{ marginBottom: "10px" }}
            />
          )}
          {this.state.errors.length > 0 &&
            this.state.errors.map((item, index) => {
              return (
                <Alert
                  message={item.error}
                  type="error"
                  showIcon
                  key={index}
                  closable
                />
              );
            })}
          <Spin spinning={this.state.loading}>
            <Card style={{ marginLeft: "auto", marginRight: "auto" }}>
              <Form
                ref={this.formRef}
                name="normal_login"
                className="login-form"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
              >
                {/* {this.state.preview ? (
                  <Avatar
                    size={100}
                    src={this.state.preview}
                    alt={`file unable to preview.`}
                  ></Avatar>
                ) : (
                  <div>No Avatar Selected</div>
                )}
                <Row style={{ marginBottom: 15 }}>
                  <Upload
                    action={this.handleUpload}
                    beforeUpload={beforeUpload}
                    showUploadList={false}
                  >
                    <Button
                      icon={<UploadOutlined />}
                      style={{ marginTop: "5px" }}
                    >
                      Upload Avatar
                    </Button>
                  </Upload>
                </Row> */}

                <Form.Item style={{ marginBottom: 0 }}>
                  <Form.Item
                    name="first_name"
                    rules={[
                      {
                        required: true,
                        message: "Please input your First Name!",
                      },
                    ]}
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                    }}
                  >
                    <Input
                      prefix={
                        <UserAddOutlined className="site-form-item-icon" />
                      }
                      placeholder="First Name"
                    />
                  </Form.Item>
                  <Form.Item
                    name="last_name"
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                      margin: "0 8px",
                    }}
                  >
                    <Input
                      prefix={
                        <UserAddOutlined className="site-form-item-icon" />
                      }
                      placeholder="Last Name"
                    />
                  </Form.Item>
                </Form.Item>

                <Form.Item style={{ marginBottom: 0 }}>
                  <Form.Item
                    name="email"
                    rules={[
                      { required: true, message: "Please input your email!" },
                      {
                        type: "email",
                        message: "The input is not valid E-mail!",
                      },
                    ]}
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                    }}
                  >
                    <Input prefix={<UserOutlined />} placeholder="Email" />
                  </Form.Item>

                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Please input password!",
                      },
                      {
                        min: 8,
                        message: "Password must be minimum 6 characters.",
                      },
                    ]}
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                      margin: "0 8px",
                    }}
                  >
                    <Input.Password
                      prefix={<LockOutlined />}
                      iconRender={(visible) =>
                        visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                      }
                      type="password"
                      placeholder="Password"
                    />
                  </Form.Item>
                </Form.Item>

                <Form.Item style={{ marginBottom: 0 }}>
                  <Form.Item
                    name="phone"
                    rules={[
                      {
                        required: true,
                        message: "Please input your phone number!",
                      },
                      () => ({
                        validator(rule, value) {
                          const regex =
                            /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
                          if (regex.test(value)) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            "Please enter 10 digit number value"
                          );
                        },
                      }),
                    ]}
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                    }}
                  >
                    <Input
                      prefix={<PhoneOutlined />}
                      placeholder="Phone Number"
                    />
                  </Form.Item>
                  <Form.Item
                    name="address"
                    style={{
                      display: "inline-block",
                      width: "calc(50% - 8px)",
                      margin: "0 8px",
                    }}
                  >
                    <Input prefix={<HomeOutlined />} placeholder="Address" />
                  </Form.Item>
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    htmlType="submit"
                    className="login-form-button"
                    ghost
                  >
                    Create
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </Spin>
        </Card>
      </>
    );
  }
}

export default User;
