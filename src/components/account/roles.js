import React, { useEffect, useState } from "react";
import { Spin, Modal, Checkbox, Select, Divider } from "antd";
import http from "../../config";

const Roles = (props) => {
  const [roles, setRoles] = useState([]);
  const [state, setState] = useState({ loading: false });
  const [data, setData] = useState({});
  const [modulesdata, setModulesData] = useState({});

  const onChange = (e, index, type) => {
    if (type == "read") {
      if (e.target.checked) {
        console.log("this is read true");
      } else {
        data.items[index].permissions["write"] = false;
        data.items[index].permissions["delete"] = false;
        setData(data);
        props.rolesCallBack(data);
      }
    }
    data.items[index].permissions[type] = e.target.checked;
    setData(data);
    props.rolesCallBack(data);
  };

  const getRoles = async () => {
    try {
      let roleslist = await http.get("permissions/rolesindex");

      roles.items = roleslist.data.roles;
      setRoles(roleslist.data.roles);
      // setRoles(roles);
      console.log(roles.items);
      // setModulesData(rolepermission);
    } catch (err) {
      console.log(err);
    }
  };

  const getPermissions = async () => {
    setData({});
    props.rolesCallBack();
    setState({ loading: true });
    let userid = { id: props.userId };
    try {
      let test = await http.post("permissions/list", userid);
      let d = test.data.data;
      if (d.items) {
        setData(d);
        props.rolesCallBack(d);
      } else {
        console.log("no modules found");
      }
      roles.items = test.data.roles;
      setRoles(roles);
      console.log(data);
      setState({ loading: false });
    } catch (err) {
      console.log(err);
    }
  };
  const { Option } = Select;

  const roleSelect = async (value) => {
    data.role = value;
    setData(data);
    // setData({...data,role:value});
    props.rolesCallBack(data);
    const rolemodules = { modules: value, userId: props.userId };
    try {
      let modules = await http.post("permissions/byroles", rolemodules);

      let d = modules.data.data;
      if (d.items) {
        setData(d);
        props.rolesCallBack(d);
      } else {
        console.log("no modules found");
      }
      roles.items = modules.data.roles;
      setRoles(roles);

      setState({ loading: false });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const { childRef } = props;
    if (props.userId) {
      getPermissions();
      getRoles();
    }
  }, [props.userId]);
  return (
    <>
      <Spin spinning={state.loading}>
        <div style={{ marginBottom: 20 }}>
          <b>Select Role</b>

          <Select
            value={data.role}
            style={{ width: 120, marginButtom: 20, marginLeft: 20 }}
            onChange={(value) => roleSelect(value)}
          >
            {roles.map((item) => (
              <Option value={item.id} key={item.id}>
                {item.name}
              </Option>
            ))}
          </Select>
        </div>
        <table className="roles">
          <thead>
            <tr>
              <th className="roles__cell roles__cell--bold roles__cell--shaded">
                Name
              </th>
              <th className="roles__cell roles__cell--bold roles__cell--shaded">
                Read
              </th>
              <th className="roles__cell roles__cell--bold roles__cell--shaded">
                Write
              </th>
              <th className="roles__cell roles__cell--bold roles__cell--shaded">
                Delete
              </th>
            </tr>
          </thead>
          <tbody>
            {data.items &&
              data.items.map((item, index) => {
                return (
                  <tr key={index}>
                    <td className="roles__cell roles__cell--bold">
                      <div className="module-name">{item.module}</div>
                    </td>
                    <td className="roles__cell ">
                      <Checkbox
                        name="read"
                        checked={item.permissions.read}
                        onChange={(e) => onChange(e, index, "read")}
                      />
                    </td>
                    <td className="roles__cell">
                      <Checkbox
                        name="write"
                        checked={item.permissions.write}
                        onChange={(e) => onChange(e, index, "write")}
                        disabled={item.permissions.read ? "" : true}
                      />
                    </td>
                    <td className="roles__cell">
                      <Checkbox
                        name="delete"
                        checked={item.permissions.delete}
                        onChange={(e) => onChange(e, index, "delete")}
                        disabled={item.permissions.read ? "" : true}
                      />
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </Spin>
    </>
  );
};
export default Roles;
