import React from "react";
import {
  AntDesignOutlined,
  HomeOutlined,
  PhoneOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import {
  Upload,
  Avatar,
  Button,
  Card,
  Form,
  Input,
  Alert,
  message,
  Row,
  Divider,
} from "antd";
import http from "../../config";
import ImgCrop from "antd-img-crop";

// function beforeUpload(file) {
//   const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
//   if (!isJpgOrPng) {
//     message.error("You can only upload JPG/PNG file!");
//   }
//   const isLt2M = file.size / 1024 / 1024 < 2;
//   if (!isLt2M) {
//     message.error("Image must smaller than 2MB!");
//   }
//   return isJpgOrPng && isLt2M;
// }

class MyAccount extends React.Component {
  formRef = React.createRef();
  state = {
    avatar: "",
    loading: true,
    userdata: "",
    imageUrl: "",
    preview: "",
    imagefile: "",
    validationerror: "closed",
    errors: [],
    updateUser: false,
  };

  async getUser() {
    try {
      const { updateUser } = this.props;
      if (updateUser) {
        this.setState({ updateUser: true });
      }
      let user = updateUser
        ? await http.get(`users/${this.props.updateId}`)
        : await http.get("auth/user");
      this.setState({ loading: false });
      this.formRef.current.setFieldsValue({
        first_name: user.data.userdetails.first_name,
        last_name: user.data.userdetails.last_name,
        email: user.data.userdetails.email,
        phone: user.data.userdetails.phone,
        address: user.data.userdetails.address,
      });
      // if (user.data.userdetails.profile_photo_path) {
      //   this.setState({ preview: user.data.userdetails.profile_photo_path });
      // } else {
      //   this.setState({ avatar: user.data.userdetails.first_name.charAt(0) });
      // }
      this.setState({ userdata: user.data.userdetails });
    } catch (err) {
      console.error(err);
    }
  }

  componentDidMount() {
    this.getUser();
  }

  onFinish = async (values) => {
    try {
      this.setState({ loading: true, validationerror: "closed", errors: [] });
      let formData = new FormData();
      //  formData.append("file", this.state.imagefile);
      formData.append("first_name", values.first_name);
      formData.append("last_name", values.last_name);
      formData.append("phone", values.phone);
      formData.append("address", values.address);
      let url = this.state.updateUser
        ? `/users/${this.props.updateId}`
        : "/auth/user-update";
      let result = await http.post(url, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      // this.setState({ imagefile: "", imageUrl: "" });
      this.getUser();
    } catch (err) {
      let error;
      console.log(err.response);
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        this.setState({
          errors: ers,
        });
      }
      this.setState({
        validationerror: error,
      });
      message.error({ content: error, duration: 2 });
      this.setState({ loading: false });
      this.getUser();
    }
  };

  // handleUpload = async (file) => {
  //   let previewUrl = URL.createObjectURL(file);
  //   this.setState({ preview: previewUrl, imagefile: file, avatar: "" });
  // };

  render() {
    // const { loading, imageUrl } = this.state;
    return (
      <>
        <Card
          title={this.state.updateUser ? "Update User" : "My Account"}
          style={{ width: 960, marginLeft: "auto", marginRight: "auto" }}
          loading={this.state.loading}
        >
          {this.state.validationerror !== "closed" && (
            <Alert
              message="Validation errors"
              description={this.state.validationerror}
              type="error"
              showIcon
              closable
              style={{ marginBottom: "10px" }}
            />
          )}
          {this.state.errors.length > 0 &&
            this.state.errors.map((item, index) => {
              return (
                <Alert
                  message={item.error}
                  type="error"
                  showIcon
                  key={index}
                  closable
                />
              );
            })}
          {this.state.updateUser ? (
            <p>Manage users personal information and preferences</p>
          ) : (
            <p>Manage your personal information and preferences</p>
          )}
          {/* {this.state.avatar ? (
            <Avatar
              style={{ backgroundColor: "#f56a00", margin: "auto" }}
              size={100}
            >
              <span style={{ fontSize: 75, textTransform: "capitalize" }}>
                {this.state.avatar}
              </span>
            </Avatar>
          ) : (
            <Avatar
              size={100}
              src={this.state.preview}
              alt={`file unable to preview.`}
            ></Avatar>
          )} */}
          {/* <Row>
          <ImgCrop rotate>
          <Upload
              action={this.handleUpload}
              beforeUpload={beforeUpload}
              showUploadList={false}
            >
              <Button icon={<UploadOutlined />} style={{ marginTop: "5px" }}>
                Upload Avatar
              </Button>
            </Upload>
          </ImgCrop>
          </Row> */}
          <Form
            ref={this.formRef}
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 14 }}
            onFinish={this.onFinish}
          >
            <Form.Item
              label="First Name"
              name="first_name"
              icon={<AntDesignOutlined />}
              rules={[{ required: true, message: "Please enter first name!" }]}
              style={{
                display: "inline-block",
                width: "calc(50% - 8px)",
                marginTop: "20px",
              }}
            >
              <Input placeholder="First Name" />
            </Form.Item>
            <Form.Item
              label="Last Name"
              name="last_name"
              style={{
                display: "inline-block",
                width: "calc(50% - 8px)",
                marginTop: "20px",
              }}
            >
              <Input placeholder="Last Name" />
            </Form.Item>
            <Form.Item
              label="Email"
              name="email"
              style={{ display: "inline-block", width: "calc(50% - 8px)" }}
            >
              <Input placeholder="Email" disabled />
            </Form.Item>
            <Form.Item
              label="Phone"
              name="phone"
              rules={[
                {
                  required: true,
                  message: "Please input your phone number!",
                },
              ]}
              style={{
                display: "inline-block",
                width: "calc(50% - 8px)",
              }}
            >
              <Input prefix={<PhoneOutlined />} placeholder="Phone Number" />
            </Form.Item>
            <Form.Item
              label="Address"
              name="address"
              style={{
                display: "inline-block",
                width: "calc(50% - 8px)",
              }}
            >
              <Input prefix={<HomeOutlined />} placeholder="Address" />
            </Form.Item>

            <Form.Item label=" " colon={false} style={{ display: "block" }}>
              <Button type="primary" htmlType="submit">
                Save
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </>
    );
  }
}
export default MyAccount;
