import React, { useState, useEffect } from "react";
import {
  ExclamationCircleOutlined,
  LockOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from "@ant-design/icons";
import { Modal, Button, Card, Form, Input, Alert, message } from "antd";
import http from "../../config";
import { setAuth } from "../../redux/actions/index";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";

const Credentials = (props) => {
  const [form] = Form.useForm();
  const history = useHistory();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [validationError, setValidationError] = useState("closed");
  const [errors, setErrors] = useState([]);
  const [UpdateUser, setUpdateUser] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);

  let isMount = false;
  useEffect(() => {
    return () => {
      isMount = true;
    };
  }, []);

  const handleLogout = async () => {
    try {
      setLoading(true);
      await http.post("/auth/logout");
      dispatch(setAuth({ auth: {} }));
      history.push("/login");
      if (!isMount) setLoading(false);
    } catch (err) {
      if (!isMount) setLoading(false);
    }
  };

  const confirm = () => {
    Modal.confirm({
      title: "Do you want to stay logged in?",
      icon: <ExclamationCircleOutlined />,
      content: "Do you want to end this session and continue?",
      okText: "Stay logged in",
      cancelText: "Logout & Continue",
      onCancel() {
        handleLogout();
      },
    });
  };

  const onFinish = async (values) => {
    setLoading(true);
    try {
      const { updateUser } = props;
      let url = updateUser
        ? `/users/password-update/${props.updateId}`
        : "/auth/password-update";
      await http.post(url, values);
      setValidationError("closed");
      setErrors([]);
      setLoading(false);
      form.resetFields();
      if (updateUser) {
        setUpdateUser(true);
      } else {
        confirm();
      }
    } catch (err) {
      console.log(err);
      let error;
      if (typeof err.response.data.message === "string") {
        error = err.response.data.message;
      }
      let ers = [];
      if (err.response.data.errors) {
        let errs = err.response.data.errors;
        for (let p in errs) {
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj);
        }
        setErrors(ers);
      }
      setValidationError(error);
      setLoading(false);
      message.error({ content: error, duration: 2 });
    }
  };

  return (
    <>
      <Card
        title="Credentials"
        style={{ width: 960, marginLeft: "auto", marginRight: "auto" }}
        loading={loading}
      >
        {validationError !== "closed" && (
          <Alert
            message="Validation errors"
            description={validationError}
            type="error"
            showIcon
            closable
            style={{ marginBottom: "10px" }}
          />
        )}
        {errors.length > 0 &&
          errors.map((item, index) => {
            return (
              <Alert
                message={item.error}
                type="error"
                showIcon
                key={index}
                closable
              />
            );
          })}
        <Form
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 14 }}
          onFinish={onFinish}
        >
          <Form.Item style={{ marginBottom: 0 }}>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your new Password!" },
                { min: 8, message: "Password must be minimum 8 characters." },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
                type="password"
                placeholder="New Password"
              />
            </Form.Item>
            <Form.Item
              name="password_confirmation"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      "The password that you entered do not match!"
                    );
                  },
                }),
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
                type="password"
                placeholder="Confirm Password"
              />
            </Form.Item>
          </Form.Item>
          <Form.Item label=" " colon={false} style={{ display: "block" }}>
            <Button type="primary" htmlType="submit">
              Save
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </>
  );
};
export default Credentials;
