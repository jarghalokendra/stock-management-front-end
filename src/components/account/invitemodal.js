import React from "react";
import {  UserAddOutlined} from '@ant-design/icons';
import {  List, Modal, Button, Form, Input, Divider } from 'antd';

class InviteModal extends React.Component {
    state = {
        emails: []
    };
    onFinish = async (values) => {
        const exists = this.state.emails.find(p => p.email === values.email);
        if (exists) return;
        let emails = [...this.state.emails, values];
        this.setState({
          emails: emails
        })
      }
      removeEmail(index) {
        const email = this.state.emails;
        email.splice(index, 1);
        this.setState({ email });
      }
      onInvite = async () => {
        try {
    
        }
        catch {
    
        }
      }
      closeInvite = () =>
      {
          this.props.closeInvite();
      }
    render(){
    return (
        <>
              <Modal
          title="Invite People"
          centered
          visible={this.props.modal}
          onCancel={() => this.closeInvite()}
        //   onOk={this.onInvite}
          okText="Invite"
        >
          Invite people with email
        	<Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
            onFinish={this.onFinish}
          >
            <Form.Item style={{ marginBottom: 0 }}>
              <Form.Item
                name="email"
                rules={[{ required: true, message: 'Please input invitation email!' },
                { type: 'email', message: 'The input is not valid E-mail!' }
                ]}
                style={{ display: 'inline-block', width: 'calc(50% - 8px)' }}
              >
                <Input placeholder="Invitation Email" />
              </Form.Item>
              <Button type="primary" style={{ float: "right" }} icon={<UserAddOutlined />} htmlType="submit">Add</Button>
            </Form.Item>
          </Form>
          <Divider />
          <List
            size="large"
            header={<div> Invitation Emails</div>}
            bordered
            dataSource={this.state.emails}
            renderItem={(item, i) => (
              <List.Item extra={<Button size="small" onClick={() => this.removeEmail(i)}>Delete</Button>}>
                {item.email}
              </List.Item>
            )}
          />
        </Modal>
        </>
    );
    }
};
export default InviteModal

