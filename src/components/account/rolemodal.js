import React from "react";
import { Modal } from "antd";
import Roles from "./roles";
import http from "../../config";

class RoleModal extends React.Component {
  state = {
    data: [{}],
  };
  rolesCallBack = (rolesData) => {
    this.setState({ data: rolesData });
  };
  closeRoles = () => {
    this.props.closeRole();
  };
  changeRoles = async () => {
    const data = this.state.data;
    data.users = this.props.userId[0];
    console.log(data);
    try {
      await http.post("permissions/store", data);
      this.props.closeRole();
    } catch (err) {
      console.log(err);
    }
  };
  render() {
    return (
      <>
        <Modal
          title="Assign Roles & Permissions"
          centered
          visible={this.props.modal}
          onOk={() => this.changeRoles()}
          onCancel={() => this.closeRoles()}
          okText="Set Roles"
          width={1000}
        >
          <Roles
            userId={this.props.userId}
            rolesCallBack={this.rolesCallBack}
          />
        </Modal>
      </>
    );
  }
}
export default RoleModal;
