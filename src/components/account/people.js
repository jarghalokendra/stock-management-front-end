import React from "react";
import { connect } from "react-redux";
import { CCard, CCol, CRow } from "@coreui/react";
import { Button, Card } from "antd";
import GridTable from "../../components/grid/index";
import { setSelectedGridIds } from "../../redux/actions/index";
import RoleModal from "./rolemodal";
import InviteModal from "./invitemodal";
class People extends React.Component {
  formRef = React.createRef();
  state = {
    modalInviteVisible: false,
    modalRoleVisible: false,

    userId: "",
    // data: [],
  };
  handePermissionAction(e, row) {
    e.preventDefault();
    this.setState({ modalRoleVisible: true, userId: [row.id] });
  }
  setModalInviteVisible(modalInviteVisible) {
    this.setState({ modalInviteVisible });
  }

  setModalRoleVisible(modalRoleVisible) {
    this.setState({ modalRoleVisible });
  }
  componentDidUpdate() {
    if (this.props.selectedGridIds.length > 0) {
      this.setState({
        modalRoleVisible: true,
        userId: this.props.selectedGridIds,
      });
      this.props.setSelectedGridIds([]);
    }
  }
  render() {
    const columns = [
      {
        title: "First Name",
        dataIndex: "first_name",
        sorter: true,
      },
      {
        title: "Last Name",
        dataIndex: "last_name",
        sorter: true,
      },
      {
        title: "Email",
        dataIndex: "email",
        sorter: true,
      },
      {
        title: "Phone",
        dataIndex: "phone",
        sorter: true,
      },
      {
        title: "created_at",
        dataIndex: "created_at",
        sorter: true,
      },
    ];
    const acolumns = {
      apiurl: "users",
      title: "Users",
      content_type: "users",
      settings: {
        show: false,
        createTitle: "Create",
        createLink: "",
      },
      statusColumn: {
        column: "status",
        show: false,
      },
      selection: {
        type: "radio",
        show: false,
      },
      fileColumn: {
        column: "file",
        show: false,
        relationName: "preview",
      },
      actions: {
        show: true,
        permission: {
          show: true,
          name: "Permissions",
          type: "popup", //it should only two type: popup/url if type is url its redirect to path like {modules/id/edit}
          callFunction: "handlePermission",
        },
        delete: {
          show: false,
          name: "Delete",
          type: "popup",
        },
        screen: {
          show: false,
          name: "Set to Screen",
          type: "set-to-screen", //it should be only two types set-to-screen or set-content
        },
      },
    };
    return (
      <>
        <Card
          title="People"
          style={{ marginLeft: "auto", marginRight: "auto" }}
        >
          <p>
            Invite and manage members, and edit which groups they have access
            to.
            <Button
              type="primary"
              style={{ float: "right" }}
              onClick={() => this.setModalInviteVisible(true)}
            >
              Invite
            </Button>
          </p>
          <CRow>
            <CCol xl={12} md={12}>
              <CCard>
                <GridTable
                  columns={columns}
                  acolumns={acolumns}
                  handleCreate={() => this.handleCreate()}
                  handlePermission={(e, row) =>
                    this.handePermissionAction(e, row)
                  }
                />
              </CCard>
            </CCol>
          </CRow>
        </Card>

        <InviteModal
          modal={this.state.modalInviteVisible}
          openInvite={() => this.setModalInviteVisible(true)}
          closeInvite={() => this.setModalInviteVisible(false)}
        />
        <RoleModal
          userId={this.state.userId}
          modal={this.state.modalRoleVisible}
          openRole={() => this.setModalRoleVisible(true)}
          closeRole={() => this.setModalRoleVisible(false)}
        />
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  selectedGridIds: state.selectedGridIds,
});
export default connect(mapStateToProps, { setSelectedGridIds })(People);
