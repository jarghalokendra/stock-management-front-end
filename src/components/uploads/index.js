import React from 'react'
import { connect } from "react-redux"
import { Upload, Modal, Drawer, Button, Row, Spin,Col,Alert,Progress, message} from 'antd';
import Dropzone from 'react-dropzone';
import { DeleteOutlined, CloseOutlined, FilePdfOutlined, VideoCameraOutlined} from '@ant-design/icons';
import 'tui-image-editor/dist/tui-image-editor.css'
import ImageEditor from '@toast-ui/react-image-editor'

import http from '../../config'
import { setSelectedImages,setInitGrid,removeSelectedImage, resetSelectedImages,updateEditImage} from "../../redux/actions/index";

class UploadHandle extends React.Component {
  _isMounted = false
  editorRef = React.createRef();
  state = {
    files: [],
    progress: 0,
    loading: false,
    imageEditorVisible: false,
    errors: [],
    selectedEdit: {},
    fIndex: null,
    fileName: ''
  };
  componentDidMount(){
    this._isMounted = true;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  onDrop = (files) => {
    files.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file)
    }));
    for(let i=0; i < files.length ; i++){
      if(files[i].type === 'video/mp4')
        this.getDuration(files,i)
      else 
        files[i].duration = 10
      this.props.setSelectedImages(files[i]);
    }

  }
  handleClose = () => {
    this.props.handleClose();
    this.props.resetSelectedImages();
    this.setState({errors: []})
  }
  getDuration(files,index){
    window.URL = window.URL || window.webkitURL;
    let video = document.createElement('video');
    video.preload = 'metadata';
    video.onloadedmetadata = function() {
      window.URL.revokeObjectURL(video.src);
      let duration = video.duration;
      files[index].duration = duration
    }
    video.src = URL.createObjectURL(files[index])
  }
  handleUpload = async() =>{
    let formData = new FormData();
    let url = this.props.url ? this.props.url : '/medias/uploads'
    const {selectedImages} = this.props;
    if(selectedImages.length === 0){
      message.error({ content: 'Files are empty! Trying to upload empty file', duration: 10});
      return ;
    }
    for(let prop in selectedImages){
      formData.append('files[]', selectedImages[prop]);
      formData.append('duration[]', selectedImages[prop].duration)
    }
    try{
      this.setState({loading: true});
      let result = await http.post(url, formData,{
        headers: {
          "Content-Type": "multipart/form-data",
        },
        onUploadProgress: ProgressEvent => {
          const {loaded, total} = ProgressEvent;
          let percent = Math.round((loaded * 100) / total)
          if(percent < 100){
            this.setState({
              progress: percent
            })
          }
        }
      })
      this.setState({loading: false, progress: 100}, ()=>{
        setTimeout(()=>{
          this.setState({progress: 0})
        },1000)
      });
      this.props.resetSelectedImages();
      this.props.setInitGrid({init: true})
      this.setState({errors: []});
      this.props.handleClose();
    }
    catch(err){
      
      let error;
      if(typeof err.response.data.message === 'string'){
        error = err.response.data.message;
      }
      let ers = []
      if(err.response.data.errors){
        let errs = err.response.data.errors
        for(let p in errs){
          let obj = {};
          obj.error = errs[p][0];
          ers.push(obj)
        }
        this.setState({errors: ers})
      }
      this.setState({
        loading: false,
      });
      message.error({ content: error, duration: 2});
      //this.props.resetSelectedImages();
    }
  }
  handlePreviewDelete(e, index){
    e.preventDefault();
    this.props.removeSelectedImage(index)
    this.setState({errors: []})
  }
  handleOk = () => {
    const editorInstance = this.editorRef.current.getInstance();
    //editorInstance.flipX();
    let dataurl = editorInstance.toDataURL();
    let size = this.dataURItoBlob(dataurl);
    
    let file = new File([size], this.state.fileName, {
      type: "image/jpeg",
      lastModified: new Date(),
      size: size
    });
    let preview = URL.createObjectURL(file);
    file.preview = preview
    console.log(file);
    this.props.updateEditImage({index: this.state.fIndex,file:file})
    this.setState({imageEditorVisible: false})
  };
  dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    // write the ArrayBuffer to a blob, and you're done
    var bb = new Blob([ab]);
    return bb;
  }
  handleEdit(e,file,index){
    e.preventDefault();
    let selected = this.props.selectedImages[index];
    this.setState({ 
      fIndex: index,
      imageEditorVisible: true,
      selectedEdit: selected,
      fileName: file.name
    })

  }
  render(){
    const selectedImages = this.props.selectedImages ? this.props.selectedImages : []
    const files = selectedImages.map((file,index) => (
      <li key={index}>
        <div className="img-desc-wrapper">
          {
            file.type === 'image/jpeg' || file.type === 'image/gif' || file.type === 'image/png' || file.type === 'image/jpg' ? <img src={file.preview} alt={`file unable to preview.`}/> :
            file.type === 'video/mp4' ? <div className="file-type"><VideoCameraOutlined /></div>
            :<div className="file-type"><FilePdfOutlined/></div>
          }
          
          <p>{file.name.substring(0, 40)} {file.name.length > 40 ? '...' : ''} - {Math.ceil(file.size/1028)} kib</p>
        </div>
        <div className="action-wrapper">
          {
            file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg' ? 
            <a href="#" className="action-edit" onClick={(e)=>this.handleEdit(e,file, index)}>Edit</a> : ''
          }
          <a href="#" onClick={(e)=>this.handlePreviewDelete(e, index)}><CloseOutlined/></a>
        </div>
      </li>
    ));
  	return (
  		<Row gutter={{xs: 8, sm: 16, md: 24, lg: 32}}>
  			<Drawer
        	title="Media Upload"
        	className="drawer-wrapper"
        	visible={this.props.visible}
        	closable={true}
        	onClose={this.handleClose}
          maskClosable={false}
        	bodyStyle={{ paddingBottom: 80 }}
          footer={
            <div
              style={{
                textAlign: 'right',
              }}
            >
              <Button type="primary" style={{marginRight: '10px'}} onClick={this.handleUpload}>
                Upload {selectedImages.length > 0 ? `(${selectedImages.length})` : ''}
              </Button>
              <Button onClick={this.handleClose}>
                Cancel
              </Button>
            </div>
          }
        >
          <Row>
            <Col span={22} offset={1}>
              {
                this.state.errors.length > 0 && 
                this.state.errors.map((item, index)=>{
                  return(
                    <Alert message={item} type="error" showIcon key={index} closable={false}/>
                  )
                })
              }
            </Col>
          </Row>
          <div className="image-wrapper">
            {
              this.state.loading &&
                <div className="upload-progress">
                  <Progress percent={this.state.progress} />
                </div>
            }
            <Spin spinning={this.state.loading}>
              <Dropzone onDrop={this.onDrop} ref='dropzoneRef' >
               {({getRootProps, getInputProps}) => (
                 <section className="container">
                   <div {...getRootProps({className: 'dropzone'})}>
                     <input {...getInputProps()} accept="image/png,image/jpeg,image/jpg,video/mp4,application/pdf"/>
                     <div className="dropzone-select-label">
                      <h5>Select Files to Upload </h5>
                      <p> Drop your first video, photo or document here</p>
                      <Button type="primary">Browse</Button>
                    </div>
                   </div>
                   <aside>
                     <ul className="upload-preview">{files}</ul>
                   </aside>
                 </section>
               )}
              </Dropzone>
            </Spin>
            <Modal
              title="Media upload"
              style={{ top: 20 }}
              maskClosable={false}
              visible={this.state.imageEditorVisible}
              onOk={this.handleOk}
              onCancel={()=>this.setState({imageEditorVisible: false})}
              width={1040}
              footer={[
                <Button key="back" onClick={()=>this.setState({imageEditorVisible: false})}>
                  Cancel
                </Button>,
                <Button key="submit" type="primary" loading={this.state.loading} onClick={this.handleOk}>
                  Done
                </Button>,
              ]}
            >
              {
                this.state.imageEditorVisible &&
                <ImageEditor
                  ref={this.editorRef}
                  includeUI={{
                    loadImage: {
                      path: this.state.selectedEdit.preview,
                      name: this.state.selectedEdit.name
                    },
                    menu: ['shape','crop','rotate'],
                    initMenu: 'crop',
                    uiSize: {
                      width: '1000px',
                      height: '650px'
                    },
                    menuBarPosition: 'left'
                  }}
                  cssMaxHeight={500}
                  cssMaxWidth={700}
                  selectionStyle={{
                    cornerSize: 20,
                    rotatingPointOffset: 70
                  }}
                  usageStatistics={true}
                />
              }
              
            </Modal>
          </div>
        </Drawer>
  		</Row>
	  )
  }
}
const mapStateToProps = state => ({
  selectedImages: state.selectedImages
});
export default connect(mapStateToProps, {setSelectedImages, removeSelectedImage,setInitGrid,resetSelectedImages, updateEditImage}) (UploadHandle);