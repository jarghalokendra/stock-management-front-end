import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { CCard, CCardBody,CCol,CRow} from '@coreui/react'
import { Row, Select, Col, Form, Input, Button, Alert, Spin, message, Avatar} from 'antd';
import { useSelector, useDispatch } from 'react-redux'

import http from '../../config'
import Images from '../uploads/index'
import CardHeader from '../commons/card-header'

const Company = () => {
  const history = useHistory();
  const [form] = Form.useForm();

  const [loading, setLoading] = useState(false);
  const [validationError, setValidationError] = useState('closed');
  const [visible, setVisible] = useState(false)
  const [cats, setCats] = useState([]);
  const [init, setInit] = useState(false);
  const [logo, setLogo] = useState(null);
  const [logo_id, setLogoId] = useState(null);
  const [isEdit, setIsEdit] = useState(false);
  const [updateId, setUpdateId] = useState(null);

  const { Option } = Select;
  const {TextArea} = Input;

  const insertImages = useSelector(state => state.imageHandler.single)

  useEffect(() => {
    if (!init) {
      getParentCat();
      let path = history.location.pathname;
      path = path.split('/');
      if(path.length > 0 && path[2] != 'create'){
        initEdit(path[2])
      }
    }
  }, []);

  useEffect(() => {
    if(insertImages.category){
      setLogo(insertImages.category.path);
      setLogoId(insertImages.category.id)
    }
  }, [insertImages]); 

  const getParentCat = async () => {
    setInit(true)
    let cats = await http.post('/device/info');
    setCats(cats.data.items);
    setInit(false)
  }

  //getParentCat();
 
  const onFinish = async (values) => {
    try {
      values.logo_id = logo_id;
      if(!values.parent_id)
        values.parent_id = 0;
      setLoading(true);
      let url = isEdit ? `/categories/update/${updateId}` : 'categories/create'
      await http.post(url,values);
      setLoading(false)
      history.push('/categories')
    }
    catch(err){
      let field = err.response.data.errors['name'] ? 'name' : 'invalid';
      setValidationError(err.response.data.errors[field][0])
      setLoading(false)
      message.error({ content: err.response.data.errors[field][0], duration: 2});
    }
  };
  const initEdit = async(id) => {
    try{
      setLoading(true)
      setUpdateId(id);
      let values = {};
      setInit(true)
      let result = await http.get(`/categories/${id}/edit`);
      const edit = result.data.edit;

      values.name = edit.name;
      values.description = edit.description;
      values.parent_id = edit.parent_id === 0 ? '' : edit.parent_id;
      form.setFieldsValue(values);
      setIsEdit(true)
      if(edit.logos){
        setLogo(edit.logos.path)
        setLogoId(edit.logos.id)
      }
      setInit(false)
      setLoading(false)
    }
    catch(err){

    }
  }

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 5 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 10 },
    },
  };

  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <CardHeader actions={{linkTitle: 'Categories', listLink: '/categories'}}/>
          <CCardBody>
            <Row gutter={16}>
              <Col className="gutter-row" offset={4} span={8}>
                {
                  validationError !=='closed' && 
                  <Alert message={validationError} type="error" showIcon closable style={{marginBottom: '10px'}}/>
                }
              </Col>
            </Row>
            <Spin spinning={loading}>
              <Form 
                form={form}
                {...formItemLayout}
                name="complex-form" 
                onFinish={onFinish} 
              >
                <Form.Item
                  label="Name"
                  name="name"
                  rules={[{ required: true, message: 'Category Name is required!' }]}
                >
                  <Input placeholder="Category Name"/>
                </Form.Item>
                <Form.Item
                  label="Parent category"
                  name="parent_id"
                >
                  <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Select parent category"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {
                      cats.map((item, index)=>{
                        return (
                          <Option value={item.id} key={index}>{item.name}</Option>
                        )
                      })
                    }
                  </Select>
                </Form.Item>

                <Form.Item label=" " colon={false}>
                  <div className="avatar-wrapper">
                    {
                      logo &&
                      <Avatar shape="square" size={100} src={logo}/>
                    }
                    
                    <Button type="primary" ghost onClick={()=>setVisible(true)}>
                      Upload Logo
                    </Button>
                  </div>
                </Form.Item>

                <Form.Item 
                  label="Description"
                  name="description"
                >
                  <TextArea rows={5}/>
                </Form.Item>
                <Form.Item label=" " colon={false}>
                  <Button type="primary" htmlType="submit">
                    Submit
                  </Button>
                </Form.Item>
              </Form>
            </Spin>
            {
              visible &&
              <Images visible={visible} storeKey={`category`} insertType={`radio`} handleClose={()=>setVisible(false)}/>
            }
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Company
