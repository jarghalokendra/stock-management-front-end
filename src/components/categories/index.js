import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { CCard,CCol,CRow } from '@coreui/react'

import GridTable from '../grid/index'

const Users = () => {
  const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    sorter: true,
    fixed: 'left'
  },
  {
    title: 'created_at',
    dataIndex: 'created_at',
    sorter: true,
  }]
  const acolumns = {
    apiurl: 'categories',
    actions: {
      tabletitle: 'Categories',
      createLink: '/category/create'
    },
    statusColumn: {
      column: 'status',
      show: true
    }
  }  
  return (
    <CRow>
      <CCol xl={12} md={12}>
        <CCard>
          <GridTable columns={columns} acolumns={acolumns}/>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Users
